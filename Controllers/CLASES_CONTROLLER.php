<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

include '../Models/CLASES_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/CLASES_SHOWALL.php'; //incluye la vista mensaje
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/CLASES_ADD.php';
include '../Views/CLASES_DELETE.php';

//Esta función crea un objeto tipo USUARIO_MODEL con los valores que se le pasan con $_REQUEST
function get_data_form() {

    if (isset($_REQUEST['id_clase'])) {
    	$id_clase=$_REQUEST['id_clase'];
    }else{
    	$id_clase=null;
    }
    
    $fecha_clase = $_REQUEST['fecha_clase'];
    $hora_clase = $_REQUEST['hora_clase'];
    $nombre_escuela = $_REQUEST['nombre_escuela'];
   
    if (isset($_REQUEST['id_pista'])) {
    	 $id_pista = $_REQUEST['id_pista'];
    }else{
    	$id_pista=null;
    }
   
    
    $action = $_REQUEST[ 'action' ]; //Variable que almacena el valor de action
	$ESCUELA = new CLASES_MODEL(
		$id_clase,
		$fecha_clase,
        $hora_clase,
        $nombre_escuela,
        $id_pista
      
        
	);//Creamos un objeto de usuario con las variables que se han recibido del formulario
	//Devuelve el valor del objecto model creado
	return $ESCUELA;
}
//Si la variable action no tiene contenido le asignamos ''
if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
	case 'ADD'://Caso añadir
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario ADD

				new CLASES_ADD($_REQUEST['nombre_escuela']);
			} 
        else {
			$CLASES = get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
			$respuesta = $CLASES->ADD();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/ESCUELA_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
	case 'DELETE'://Caso borrar
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario DELETE
			$CLASES = new CLASES_MODEL( $_REQUEST[ 'id_clase' ], '', '','','');
			//Variable que almacena el relleno de los datos utilizando el login
			$valores = $CLASES->RellenaDatos();
                
            //Crea una vista delete para ver la tupla
			new CLASES_DELETE( $valores,$_REQUEST['id_clase']);
			}
			 else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL borra los datos
			//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos de los atributos
			$CLASES = get_data_form();
			//Variable que almacena la respuesta de realizar el borrado
			$respuesta = $CLASES->DELETE();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/ESCUELA_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
	
	default: //Caso que se ejecuta por defecto
		
		
        if ( !$_POST ) {//Si no se han recibido datos 
        	
            $CLASES = new CLASES_MODEL('', '', '',$_REQUEST['nombre_escuela'],'');//Variable que almacena la intancia de un objeto del modelo USUARIO
		//Si se reciben datos
		} else {
			$CLASES = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
		}
		//Variable que almacena un recordset con el resultado de los datos de la busqueda
		$datos = $CLASES->SEARCH();
    	
		//Variable que almacena array con el nombre de los atributos
		$lista = array( 'ID_CLASE','FECHA_CLASE','HORA_CLASE','NOMBRE_ESCUELA','ID_PISTA');
		new CLASES_SHOWALL( $lista, $datos,$_REQUEST['nombre_escuela']);

        
	}


?>