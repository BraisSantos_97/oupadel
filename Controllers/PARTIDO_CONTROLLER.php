<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

include '../Models/PARTIDO_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/PARTIDO_SHOWALL.php'; //incluye la vista mensaje
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/PARTIDO_ADD.php';
include  '../Views/PARTIDO_EDIT.php';
include '../Views/PARTIDO_DELETE.php';


//Esta función crea un objeto tipo USUARIO_MODEL con los valores que se le pasan con $_REQUEST
function get_data_form() {

    
    $id_partido=$_REQUEST['id_partido'];
    $fecha_partido = $_REQUEST['fecha_partido'];
    $hora_partido = $_REQUEST['hora_partido'];
    $id_pista = $_REQUEST['id_pista'];
    
    
    $action = $_REQUEST[ 'action' ]; //Variable que almacena el valor de action
	$PARTIDO = new PARTIDO_MODEL(
		$id_partido,
		$fecha_partido,
        $hora_partido,
        $id_pista
        
        
	);//Creamos un objeto de usuario con las variables que se han recibido del formulario
    
 
	//Devuelve el valor del objecto model creado
	return $PARTIDO;
}
//Si la variable action no tiene contenido le asignamos ''
if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
	case 'ADD'://Caso añadir
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario ADD
				new PARTIDO_ADD();
			} 
        else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL inserta los datos
			$PARTIDO = get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
			$respuesta = $PARTIDO->ADD();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/PARTIDO_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
	case 'DELETE'://Caso borrar
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario DELETE
			$PARTIDO= new PARTIDO_MODEL( $_REQUEST[ 'id_partido' ], '', '', '');
			//Variable que almacena el relleno de los datos utilizando el login
			$valores = $PARTIDO->RellenaDatos( $_REQUEST[ 'id_partido' ] );
                
            //Crea una vista delete para ver la tupla
			new PARTIDO_DELETE( $valores,$_REQUEST['id_partido']);
			}
			 else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL borra los datos
			//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos de los atributos
			$PARTIDO = get_data_form();
			//Variable que almacena la respuesta de realizar el borrado
			$respuesta = $PARTIDO->DELETE();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/PARTIDO_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
	
	case 'EDIT'://Caso editar	
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario EDIT
            
			$PARTIDO = new PARTIDO_MODEL( $_REQUEST[ 'id_partido' ], '', '', '');
			//Variable que almacena un objecto USUARIO(modelo) con los datos de los atibutos rellenados a traves de login
			$valores = $PARTIDO->RellenaDatos( $_REQUEST[ 'id_partido' ] );

			//Muestra la vista del formulario editar
			new PARTIDO_EDIT( $valores);
			}else {
			//Variable que almacena un objecto Usuario model de los datos recogidos
			$PARTIDO = get_data_form();
			//Variable que almacena la respuesta de la edición de los datos
			$respuesta = $PARTIDO->EDIT();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/PARTIDO_CONTROLLER.php' );
		}
		//Fin del bloque
		break;
	
	default: //Caso que se ejecuta por defecto
		
		
        if ( !$_POST ) {//Si no se han recibido datos 
            $PARTIDO = new PARTIDO_MODEL('', '', '', '','');//Variable que almacena la intancia de un objeto del modelo USUARIO
		//Si se reciben datos
		} else {
			$PARTIDO = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
		}
		//Variable que almacena un recordset con el resultado de los datos de la busqueda
		$datos = $PARTIDO->SEARCH();
    
		//Variable que almacena array con el nombre de los atributos
		$lista = array( 'ID_PARTIDO','FECHA_PARTIDO','HORA_PARTIDO','ID_PISTA');
		new PARTIDO_SHOWALL( $lista, $datos);

        
	}


?>