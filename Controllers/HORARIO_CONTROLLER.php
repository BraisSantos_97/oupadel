<?php
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
include '../Models/HORARIO_MODEL.php'; //incluye el contendio del modelo pista
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/HORARIO_ADD.php';
include '../Views/HORARIO_DELETE.php';
include '../Views/HORARIO_SHOWALL.php';
session_start();

//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

function get_data_form(){
    $fecha=$_REQUEST['fecha'];
    $hora=$_REQUEST['hora'];
    $dia=$_REQUEST['dia'];

    $action=$_REQUEST['action'];

    $HORARIO=new HORARIO($fecha,$hora,$dia);

    return $HORARIO;  
}

if(!isset($_REQUEST['action'])){
   $_REQUEST['action'] = ''; 
}

switch($_REQUEST['action']){
    case 'ADD':
        if(!$_POST){
            new HORARIO_ADD;
        } else {
            $HORARIO = get_data_form();//Variable que almacena un objecto PISTA(modelo) con los datos recogidos
			$respuesta = $HORARIO->add();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/HORARIO_CONTROLLER.php' );
        }

        break;
    
    case 'DELETE':
        if(!$_POST){
            $HORARIO = new HORARIO ($_REQUEST['fecha'],$_REQUEST['hora'],'');
            $HORARIO = $HORARIO->findByFechaHora($_REQUEST['fecha'],$_REQUEST['hora']);
            new HORARIO_DELETE ($HORARIO, $_REQUEST['fecha'], $_REQUEST['hora']);
            //new HORARIO_DELETE ($HORARIO, $HORARIO->getFecha(), $HORARIO->getHora());
        } else {
            $HORARIO = get_data_form();
            $respuesta = $HORARIO->delete();
            new MESSAGE($respuesta, '../Controllers/HORARIO_CONTROLLER.php');
        }

        break;

    default:
        if (!$_POST) {//Si no se han recibido datos 
            $HORARIO = new HORARIO('', '', '');//Variable que almacena la intancia de un objeto del modelo USUARIO
        //Si se reciben datos
        } else {
            $HORARIO = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
        }
        //Variable que almacena un recordset con el resultado de los datos de la busqueda
        $datos = $HORARIO->findAll();

        //Variable que almacena array con el nombre de los atributos
        $lista = array('FECHA','HORA','DIA');
        new HORARIO_SHOWALL( $lista, $datos);
}

?>