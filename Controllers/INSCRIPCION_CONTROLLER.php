<?php

session_start(); 
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

include '../Models/PARTIDO_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/PARTIDO_SHOWALL.php'; //incluye la vista mensaje
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/PARTIDO_ADD.php';
include  '../Views/PARTIDO_EDIT.php';
include '../Views/PARTIDO_DELETE.php';
include '../Models/INSCRIPCION_MODEL.php';
include '../Views/INSCRIPCION_ADD.php';
include '../Views/INSCRIPCION_SHOWALL.php';


//Esta función crea un objeto tipo USUARIO_MODEL con los valores que se le pasan con $_REQUEST
function get_data_form() {

//$login,$id_partido,$fecha_partido,$hora_partido,$id_pista
    
    $login=$_REQUEST['login'];
    $id_partido = $_REQUEST['id_partido'];
    //$fecha_partido = $_REQUEST['fecha_partido'];
   // $hora_partido = $_REQUEST['hora_partido'];
    
    
    
    $action = $_REQUEST[ 'action' ]; //Variable que almacena el valor de action
	$INSCRIPCION = new INSCRIPCION_MODEL(
		$login,
		$id_partido,
        '',
        '',
        ''
        
	);//Creamos un objeto de usuario con las variables que se han recibido del formulario
	//Devuelve el valor del objecto model creado
	return $INSCRIPCION;
}
//Si la variable action no tiene contenido le asignamos ''
if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
	case 'ADD'://Caso añadir
        
        $INSCRIPCION = new INSCRIPCION_MODEL($_SESSION['login'],$_REQUEST['id_partido'],$_REQUEST['fecha_partido'],$_REQUEST['hora_partido'],'');
        
	/*	if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario ADD
				new INSCRIPCION_ADD();
			} */
       // else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL inserta los datos
			//$INSCRIPCION = get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
			$respuesta = $INSCRIPCION->ADD();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/INSCRIPCION_CONTROLLER.php' );
	//	}
		//Finaliza el bloque
		break;
	case 'DELETE'://Caso borrar
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario DELETE
			$INSCRIPCION= new INSCRIPCION_MODEL( $_REQUEST[ 'login' ], '', '', '','');
			//Variable que almacena el relleno de los datos utilizando el login
			$valores = $INSCRIPCION->RellenaDatos( $_REQUEST[ 'login' ] );
                
            //Crea una vista delete para ver la tupla
			new INSCRIPCION_DELETE( $valores,$_REQUEST['login']);
			}
			 else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL borra los datos
			//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos de los atributos
			$INSCRIPCION = get_data_form();
			//Variable que almacena la respuesta de realizar el borrado
			$respuesta = $INSCRIPCION->DELETE();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/INSCRIPCION_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
	/*
	case 'EDIT'://Caso editar	
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario EDIT
            
			$INSCRIPCION = new INSCRIPCION_MODEL( $_REQUEST[ 'login' ], '', '', '');
			//Variable que almacena un objecto USUARIO(modelo) con los datos de los atibutos rellenados a traves de login
			$valores = $INSCRIPCION->RellenaDatos( $_REQUEST[ 'login' ] );

			//Muestra la vista del formulario editar
			new PARTIDO_EDIT( $valores);
			}else {
			//Variable que almacena un objecto Usuario model de los datos recogidos
			$PARTIDO = get_data_form();
			//Variable que almacena la respuesta de la edición de los datos
			$respuesta = $PARTIDO->EDIT();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/PARTIDO_CONTROLLER.php' );
		}
		//Fin del bloque
		break;
	*/
		 case 'INSCRIBIRSE'://caso para añadir una entrega cuando un usuario entra a gestión de entregas
		if($_SESSION['login']=='admin'){	//miramos si el usuario tiene dicho permiso	
            
           $INSCRIPCION = new INSCRIPCION_MODEL($_SESSION['login'],'','','','');
           $datos=$INSCRIPCION->SEARCH2();
                  $lista = array( 'Login','ID_Partido', 'Fecha_Partido', 'Hora_Partido', 'ID_Pista');//Variable que almacena un array almacenamos los campos a mostrar
          new INSCRIPCION_SHOWALL( $lista, $datos);//mostramos la vista showall 
          }else{   
            
        
            
         $USERINSCRIPCION = new INSCRIPCION_MODEL($_SESSION['login'],"","","","");//Variable que almacena un objeto de tipo ENTREGA_MODEL
            
      
         $datos=$USERINSCRIPCION->SEARCH();//Variable que almacena todas las entregas
          $lista = array( 'Login','ID_Partido', 'Fecha_Partido', 'Hora_Partido', 'ID_Pista');//Variable que almacena un array almacenamos los campos a mostrar
          new INSCRIPCION_SHOWALL( $lista, $datos);//mostramos la vista showall 
		}


        break;//Finaliza el bloque

	default: //Caso que se ejecuta por defecto
		
		
        if ( !$_POST ) {//Si no se han recibido datos 
            
          
            $INSCRIPCION = new INSCRIPCION_MODEL($_SESSION['login'],"","","","");//Variable que almacena la intancia de un objeto del modelo USUARIO
		//Si se reciben datos
		} else {
			$INSCRIPCION = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
		}
		//Variable que almacena un recordset con el resultado de los datos de la busqueda
		$datos = $INSCRIPCION->SEARCH();
    
        
		//Variable que almacena array con el nombre de los atributos
		$lista = array( 'Login','ID_Partido', 'Fecha_Partido', 'Hora_Partido', 'ID_Pista');
		new INSCRIPCION_SHOWALL( $lista, $datos);

        
	}


?>