<?php

//session
session_start();
//incluir funcion autenticacion
include '../Functions/Authentication.php';
//si no esta autenticado
if (!IsAuthenticated()){
	header('Location: ../index.php');//redirige a la página de inicio
}
//esta autenticado
else{
	include '../Views/users_index_View.php';//incluimos esa vista
	new Index();// se crea un objeto de tipo Index
}

?>