<?php
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
include '../Models/PISTA_MODEL.php'; //incluye el contendio del modelo pista
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/PISTA_ADD.php';
include '../Views/PISTA_DELETE.php';
include '../Views/PISTA_SHOWALL.php';
session_start();

//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

function get_data_form_add(){
    if(isset($_REQUEST['id_pista'])){
        $id_pista=$_REQUEST['id_pista'];
    } else {
        $id_pista='';
    }
    $tipo_pista=$_REQUEST['tipo_pista'];
    $descripcion_pista=$_REQUEST['descripcion_pista'];

    $action=$_REQUEST['action'];

    $pista=new PISTA($id_pista,$tipo_pista,$descripcion_pista);

    return $pista;  
}

function get_data_form(){
    $id_pista=$_REQUEST['id_pista'];
    $tipo_pista=$_REQUEST['tipo_pista'];
    $descripcion_pista=$_REQUEST['descripcion_pista'];

    $action=$_REQUEST['action'];

    $pista=new PISTA($id_pista,$tipo_pista,$descripcion_pista);

    return $pista;  
}

if(!isset($_REQUEST['action'])){
   $_REQUEST['action'] = ''; 
}

switch($_REQUEST['action']){
    case 'ADD':
        if(!$_POST){
            new PISTA_ADD;
        } else {
            $pista = get_data_form_add();//Variable que almacena un objecto PISTA(modelo) con los datos recogidos
			$respuesta = $pista->add();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/PISTA_CONTROLLER.php' );
        }

        break;
    
    case 'DELETE':
        if(!$_POST){
            $pista = new PISTA($_REQUEST['id_pista'],'','');
            $pista = $pista->findById($_REQUEST['id_pista']);
            new PISTA_DELETE ($pista, $_REQUEST['id_pista']);
        } else {
            $pista = get_data_form();
            $respuesta = $pista->delete();
            new MESSAGE($respuesta, '../Controllers/PISTA_CONTROLLER.php');
        }

        break;

    case 'EDIT':
        if(!$_POST){
            $pista = new PISTA( $_REQUEST[ 'id_pista' ], '', '', '');
			//Variable que almacena un objecto USUARIO(modelo) con los datos de los atibutos rellenados a traves de login
			$valores = $pista->RellenaDatos( $_REQUEST[ 'id_pista' ] );

			//Muestra la vista del formulario editar
			new PISTA_EDIT( $valores);
        } else {
            $pista = get_data_form();
			//Variable que almacena la respuesta de la edición de los datos
			$respuesta = $PISTA->update();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/PISTA_CONTROLLER.php' );
        }

        break;

    default:
        if ( !$_POST ) {//Si no se han recibido datos 
            $pista = new PISTA('', '', '');//Variable que almacena la intancia de un objeto del modelo USUARIO
        //Si se reciben datos
        } else {
            $pista = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
        }
        //Variable que almacena un recordset con el resultado de los datos de la busqueda
        $datos = $pista->findAll();

        //Variable que almacena array con el nombre de los atributos
        $lista = array( 'ID_PISTA','TIPO_PISTA','DESCRIPCION_PISTA');
        new PISTA_SHOWALL( $lista, $datos);
}

?>