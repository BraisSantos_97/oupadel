<?php

session_start(); 
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

include '../Models/INSCRIPCIONCLASE_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/INSCRIPCIONCLASE_SHOWALL.php'; //incluye la vista mensaje

include '../Views/INSCRIPCIONCLASE_ADD.php';
include '../Models/CLASES_MODEL.php'; 
include '../Views/CLASES_ADD.php';
include '../Views/CLASES_DELETE.php';
include '../Views/CLASES_SHOWALL.php';
include '../Views/MESSAGE.php'; //incluye la vista mensaje



//Esta función crea un objeto tipo USUARIO_MODEL con los valores que se le pasan con $_REQUEST
function get_data_form() {

//$login,$id_partido,$fecha_partido,$hora_partido,$id_pista
    
    $login=$_REQUEST['login'];
    $id_clase = $_REQUEST['id_clase'];
    //$fecha_partido = $_REQUEST['fecha_partido'];
   // $hora_partido = $_REQUEST['hora_partido'];
    
    
    
    $action = $_REQUEST[ 'action' ]; //Variable que almacena el valor de action
	$INSCRIPCIONCLASE = new INSCRIPCIONCLASE_MODEL(
		$login,
		$id_clase,
        '',
        '',
        '',
        ''
        
	);//Creamos un objeto de usuario con las variables que se han recibido del formulario
	//Devuelve el valor del objecto model creado
	return $INSCRIPCIONCLASE;
}
//Si la variable action no tiene contenido le asignamos ''
if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
	case 'ADD'://Caso añadir
        
        $INSCRIPCIONCLASE = new INSCRIPCIONCLASE_MODEL($_SESSION['login'],$_REQUEST['id_clase'],$_REQUEST['fecha_clase'],$_REQUEST['hora_clase'],$_REQUEST['nombre_escuela'],'');
        
	/*	if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario ADD
				new INSCRIPCION_ADD();
			} */
       // else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL inserta los datos
			//$INSCRIPCION = get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
			$respuesta = $INSCRIPCIONCLASE->ADD();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/INSCRIPCIONCLASE_CONTROLLER.php' );
	//	}
		//Finaliza el bloque
		break;
	case 'DELETE'://Caso borrar
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario DELETE
			$INSCRIPCIONCLASE= new INSCRIPCIONCLASE_MODEL( $_REQUEST[ 'login' ], '', '', '','');
			//Variable que almacena el relleno de los datos utilizando el login
			$valores = $INSCRIPCIONCLASE->RellenaDatos( $_REQUEST[ 'login' ] );
                
            //Crea una vista delete para ver la tupla
			new INSCRIPCIONCLASE_DELETE( $valores,$_REQUEST['login']);
			}
			 else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL borra los datos
			//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos de los atributos
			$INSCRIPCIONCLASE = get_data_form();
			//Variable que almacena la respuesta de realizar el borrado
			$respuesta = $INSCRIPCIONCLASE->DELETE();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/INSCRIPCIONCLASE_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;

		 case 'INSCRIBIRSE'://caso para añadir una entrega cuando un usuario entra a gestión de entregas
		if($_SESSION['login']=='admin'){	//miramos si el usuario tiene dicho permiso	
            
           $INSCRIPCIONCLASE = new INSCRIPCIONCLASE_MODEL($_SESSION['login'],'','','','','');
           $datos=$INSCRIPCIONCLASE->SEARCH2();
                  $lista = array( 'LOGIN','ID_CLASE','FECHA_CLASE', 'HORA_CLASE', 'NOMBRE_ESCUELA', 'ID_PISTA');//Variable que almacena un array almacenamos los campos a mostrar
          new INSCRIPCIONCLASE_SHOWALL( $lista, $datos);//mostramos la vista showall 
          }else{   
            
        
            
         $USERINSCRIPCIONCLASE = new INSCRIPCIONCLASE_MODEL($_SESSION['login'],"","","","","");//Variable que almacena un objeto de tipo ENTREGA_MODEL
            
      
         $datos=$USERINSCRIPCIONCLASE->SEARCH();//Variable que almacena todas las entregas
          $lista = array( 'LOGIN','ID_CLASE','FECHA_CLASE', 'HORA_CLASE', 'NOMBRE_ESCUELA', 'ID_PISTA');//Variable que almacena un array almacenamos los campos a mostrar
          new INSCRIPCIONCLASE_SHOWALL( $lista, $datos);//mostramos la vista showall 
		}


        break;//Finaliza el bloque

	default: //Caso que se ejecuta por defecto
		
		
        if ( !$_POST ) {//Si no se han recibido datos 
            
          
            $INSCRIPCIONCLASE = new INSCRIPCIONCLASE_MODEL($_SESSION['login'],"","","","","");//Variable que almacena la intancia de un objeto del modelo USUARIO
		//Si se reciben datos
		} else {
			$INSCRIPCIONCLASE = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
		}
		//Variable que almacena un recordset con el resultado de los datos de la busqueda
		$datos = $INSCRIPCIONCLASE->SEARCH();
    
        
		//Variable que almacena array con el nombre de los atributos
		$lista = array( 'LOGIN','ID_CLASE','FECHA_CLASE', 'HORA_CLASE', 'NOMBRE_ESCUELA', 'ID_PISTA');
		new INSCRIPCIONCLASE_SHOWALL( $lista, $datos);

        }
	


?>