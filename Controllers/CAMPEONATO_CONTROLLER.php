<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

include '../Models/CAMPEONATO_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/CAMPEONATO_SHOWALL.php'; //incluye la vista mensaje
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/CAMPEONATO_ADD.php';
include '../Views/CAMPEONATO_DELETE.php';

//Esta función crea un objeto tipo USUARIO_MODEL con los valores que se le pasan con $_REQUEST
function get_data_form() {

    
    $nombre_campeonato=$_REQUEST['nombre_campeonato'];
    $fecha_inicial = $_REQUEST['fecha_inicial'];
    $fecha_final = $_REQUEST['fecha_final'];
    $descripcion_campeonato = $_REQUEST['descripcion_campeonato'];
    $fecha_limite = $_REQUEST['fecha_limite_inscripcion'];
    
    $action = $_REQUEST[ 'action' ]; //Variable que almacena el valor de action
	$CAMPEONATO = new CAMPEONATO_MODEL(
		$nombre_campeonato,
		$fecha_inicial,
        $fecha_final,
        $descripcion_campeonato,
        $fecha_limite
        
	);//Creamos un objeto de usuario con las variables que se han recibido del formulario
	//Devuelve el valor del objecto model creado
	return $CAMPEONATO;
}
//Si la variable action no tiene contenido le asignamos ''
if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
	case 'ADD'://Caso añadir
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario ADD
				new CAMPEONATO_ADD();
			} 
        else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL inserta los datos
			$CAMPEONATO = get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
			$respuesta = $CAMPEONATO->ADD();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/CAMPEONATO_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
	case 'DELETE'://Caso borrar
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario DELETE
			$CAMPEONATO = new CAMPEONATO_MODEL( $_REQUEST[ 'nombre_campeonato' ], '', '', '','');
			//Variable que almacena el relleno de los datos utilizando el login
			$valores = $CAMPEONATO->RellenaDatos( $_REQUEST[ 'nombre_campeonato' ] );
                
            //Crea una vista delete para ver la tupla
			new CAMPEONATO_DELETE( $valores,$_REQUEST['nombre_campeonato']);
			}
			 else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL borra los datos
			//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos de los atributos
			$CAMPEONATO = get_data_form();
			//Variable que almacena la respuesta de realizar el borrado
			$respuesta = $CAMPEONATO->DELETE();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/CAMPEONATO_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
	/*case 'EDIT'://Caso editar	
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario EDIT
            
			$CAMPEONATO = new CAMPEONATO_MODEL( $_REQUEST[ 'nombre_campeonato' ], '', '', '');
			//Variable que almacena un objecto USUARIO(modelo) con los datos de los atibutos rellenados a traves de login
			$valores = $CAMPEONATO->RellenaDatos( $_REQUEST[ 'nombre_campeonato' ] );

			//Muestra la vista del formulario editar
			new CAMPEONATO_EDIT( $valores);
			}else {
			//Variable que almacena un objecto Usuario model de los datos recogidos
			$CAMPEONATO = get_data_form();
			//Variable que almacena la respuesta de la edición de los datos
			$respuesta = $CAMPEONATO->EDIT();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/CAMPEONATO_CONTROLLER.php' );
		}
		//Fin del bloque
		break;*/
	
	default: //Caso que se ejecuta por defecto
		
		
        if ( !$_POST ) {//Si no se han recibido datos 
            $CAMPEONATO = new CAMPEONATO_MODEL('', '', '', '','');//Variable que almacena la intancia de un objeto del modelo USUARIO
		//Si se reciben datos
		} else {
			$CAMPEONATO = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
		}
		//Variable que almacena un recordset con el resultado de los datos de la busqueda
		$datos = $CAMPEONATO->SEARCH();
    
		//Variable que almacena array con el nombre de los atributos
		$lista = array( 'NOMBRE_CAMPEONATO','FECHA_INICIO','FECHA_FINAL','DESCRIPCION_CAMPEONATO','FECHA_LIMITE_INSCRIPCION');
		new CAMPEONATO_SHOWALL( $lista, $datos);

        
	}


?>