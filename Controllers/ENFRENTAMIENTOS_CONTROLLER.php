<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

$errores=null; 

include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/ENFRENTAMIENTOS_SHOWALL.php'; //incluye la vista mensaje

include '../Models/ENFRENTAMIENTOS_MODEL.php'; //incluye el contendio del modelo usuarios

include '../Views/CLASIFICACION_SHOWALL.php';
include '../Views/MIS_ENFRENTAMIENTOS_SHOWALL.php';
include '../Views/TODOS_ENFRENTAMIENTOS.php';



function get_data_form2(){
    
    $id_enfrentamiento=$_REQUEST['id_enfrentamiento'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nombre_campeonato = $_REQUEST['nombre_campeonato'];
    $nivel = $_REQUEST['nivel'];
    $id_grupo =$_REQUEST['id_grupo'];
    
  
    
      $resultado=$_REQUEST['resultado'];
  
    
    //$action = $_REQUEST[ 'action' ];
	$ENFRENTAMIENTO = new ENFRENTAMIENTOS_MODEL(
         $id_enfrentamiento,
         $id_grupo,
		 $nombre_categoria,
         $nombre_campeonato,
         $nivel,
         $resultado,
         '',
         '',
         ''
        
	);
    
	return $ENFRENTAMIENTO;
}


if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {

        
        
    case 'GENERAR_ENFRENTAMIENTOS':
        $ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL('',$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],'','','','');
        $mensaje = $ENFRENTAMIENTOS->generarEnfrentamientos();
        
        new MESSAGE ($mensaje, "../Controllers/CAMPEONATO_CONTROLLER.php");
        
        break;

    case 'GESTIONAR_RESULTADOS':
        $ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL('',$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],'','','','');
        $datos = $ENFRENTAMIENTOS->SEARCH1();
        
       
        $lista = array( 'ID_ENFRENTAMIENTO','FECHA_ENFRENTAMIENTO','HORA_ENFRENTAMIENTO','ID_PAREJA','CAPITAN','LOGIN_PAREJA','ID_PAREJA','CAPITAN','LOGIN_PAREJA');
        
        new ENFRENTAMIENTOS_SHOWALL( $lista, $datos, $_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        
        break;

    case 'EDIT':
      
        $ENFRENTAMIENTO = get_data_form2();
        $respuesta =$ENFRENTAMIENTO->EDIT($_REQUEST['pareja1'],$_REQUEST['pareja2']);
            
        new MESSAGE ($respuesta, "../Controllers/CAMPEONATO_CONTROLLER.php");

        break;

        
    case 'DELETE':
        
            $ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL($_REQUEST['id_enfrentamiento'],'','','','','','','','');
            $respuesta =  $ENFRENTAMIENTOS->DELETE();
            new MESSAGE ($respuesta, "../Controllers/CAMPEONATO_CONTROLLER.php");
        
        break;
        
        
       
    case 'DELETE_ACORDADO':
        
        $ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL($_REQUEST['id_enfrentamiento'],$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],'',$_REQUEST['fecha_enfrentamiento'],$_REQUEST['hora_enfrentamiento'],'');
      
        $m=$ENFRENTAMIENTOS->DELETE_ACORDADO();
        
        new MESSAGE ($m, "../Controllers/CAMPEONATO_CONTROLLER.php");
        
        break;

    case 'VER_CLASIFICACION':
        $ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL('',$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],'','','','');
        $datos = $ENFRENTAMIENTOS->VER_CLASIFICACION();
        
       
        $lista = array( 'ID_PAREJA','CAPITAN','LOGIN_PAREJA','PUNTOS');
        
        new CLASIFICACION_SHOWALL( $lista, $datos, $_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);

        break;


    case 'FECHA_PROPUESTA':
        $ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL('',$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],'','','','');
        $datos=$ENFRENTAMIENTOS->ver_enfrentamientos();

        $lista=array('ID_ENFRENTAMIENTO','ID_GRUPO','NOMBRE_CATEGORIA','NOMBRE_CAMPEONATO');

        new MIS_ENFRENTAMIENTOS_SHOWALL( $lista, $datos,$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato']);
        break;
        
    case 'TODOS_ENFRENTAMIENTOS':
        $ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL('',$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],'','','','');
        $datos = $ENFRENTAMIENTOS->todosEnfrentamientos();
        
        $lista=array('ID_ENFRENTAMIENTO','FECHA_ENFRENTAMIENTO','HORA_ENFRENTAMIENTO','ID_PISTA','RESULTADO','CAPITAN','LOGIN_PAREJA','CAPITAN','LOGIN_PAREJA');
        
        new TODOS_ENFRENTAMIENTOS($lista, $datos, $_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        
        
  
        break;
}




   

   
   

?>