<?php
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
include '../Models/CRUCES_MODEL.php'; //incluye el contendio del modelo cruce
include '../Models/GRUPOS_MODEL.php'; //incluye el contendio del modelo grupo
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/TABLA_CRUCES_SHOWALL.php';
include '../Views/ENFRENTAMIENTOS_CRUCES_SHOWALL.php';
include '../Views/CRUCES_FECHA_HORA.php';

session_start();

//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

function get_data_form(){
    $nombre_campeonato=$_REQUEST['nombre_campeonato'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nivel=$_REQUEST['nivel'];
    $id_grupo=$_REQUEST['id_grupo'];
    $id_partido=$_REQUEST['id_partido'];
    $id_pareja_1=$_REQUEST['id_pareja_1'];
    $id_pareja_2=$_REQUEST['id_pareja_2'];
    $resultado=$_REQUEST['resultado'];
    $ganador=$_REQUEST['ganador'];
    $fase=$_REQUEST['fase'];
    $fecha=$_REQUEST['fecha'];
    $hora=$_REQUEST['hora'];

    $cruce=new CRUCE($id_grupo,$nombre_campeonato,$nombre_categoria,$nivel,$id_partido,$id_pareja_1,$id_pareja_2,$resultado,$ganador,$fase,$fecha,$hora);

    return $cruce;
}

function get_data_form_edit(){
    $nombre_campeonato=$_REQUEST['nombre_campeonato'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nivel=$_REQUEST['nivel'];
    $id_grupo=$_REQUEST['id_grupo'];
    $id_partido=$_REQUEST['id_partido'];
    $id_pareja_1=$_REQUEST['id_pareja_1'];
    $id_pareja_2=$_REQUEST['id_pareja_2'];
    $resultado=$_REQUEST['resultado'];
    $fase=$_REQUEST['fase'];

    $cruce=new CRUCE($id_grupo,$nombre_campeonato,$nombre_categoria,$nivel,$id_partido,$id_pareja_1,$id_pareja_2,$resultado,"",$fase,"","");

    return $cruce;
}


function get_data_form_edit_2(){
    $nombre_campeonato=$_REQUEST['nombre_campeonato'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nivel=$_REQUEST['nivel'];
    $id_grupo=$_REQUEST['id_grupo'];
    $id_partido=$_REQUEST['id_partido'];
    $id_pareja_1=$_REQUEST['id_pareja_1'];
    $id_pareja_2=$_REQUEST['id_pareja_2'];
    $resultado="";
    $fase=$_REQUEST['fase'];
    $fecha=$_REQUEST['fecha'];
    $hora=$_REQUEST['hora'];

    $cruce=new CRUCE($id_grupo,$nombre_campeonato,$nombre_categoria,$nivel,$id_partido,$id_pareja_1,$id_pareja_2,$resultado,"",$fase,$fecha,$hora);

    return $cruce;
}

function get_data_form_gen(){
    $nombre_campeonato=$_REQUEST['nombre_campeonato'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nivel=$_REQUEST['nivel'];


    $cruce=new CRUCE('',$nombre_campeonato,$nombre_categoria,$nivel,'','','','','','','','');

    return $cruce;
}

if(!isset($_REQUEST['action'])){
   $_REQUEST['action'] = ''; 
}

switch($_REQUEST['action']){
    case 'ADD':
        if(!$_POST){
            new CRUCE_ADD;
        } else {
            $cruce = get_data_form();//Variable que almacena un objecto PISTA(modelo) con los datos recogidos
			$respuesta = $cruce->add();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/CAMPEONATO_CONTROLLER.php' );
        }

        break;

    case 'GENERAR_CRUCES':
        $cruce = get_data_form_gen();//Variable que almacena un objecto PISTA(modelo) con los datos recogidos
        $grupoaux = new GRUPOS_MODEL('',$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        $grupos = $grupoaux->findAllByCategoria();

        foreach($grupos as $grupo){
            $respuesta = $cruce->generarCruces($grupo['ID_GRUPO']);
        }
        
        //Crea la vista con la respuesta y la ruta para volver
        new MESSAGE( "Cruces generados", '../Controllers/CAMPEONATO_CONTROLLER.php' );

        break;

    case 'VER_ENFRENTAMIENTOS':
        $cruce = new CRUCE;
        $enfrentamientos = $cruce->findAllByGrupo($_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        //$enfrentamientos = $enfrentamientos->fetch_assoc();
        $lista = array('ID_PARTIDO','ID_PAREJA_1','ID_PAREJA_2','RESULTADO','GANADOR','FASE','FECHA','HORA');
        new ENFRENTAMIENTOS_CRUCES_SHOWALL($lista,$enfrentamientos);
        break;
        
        
    case 'VER_ENFRENTAMIENTOS_DEPORTISTA':
        
        $cruce = new CRUCE;
        $enfrentamientos = $cruce->findAllByGrupo($_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        //$enfrentamientos = $enfrentamientos->fetch_assoc();
        $lista = array('ID_PARTIDO','ID_PAREJA_1','ID_PAREJA_2','RESULTADO','GANADOR','FASE','FECHA','HORA');
        new ENFRENTAMIENTOS_CRUCES_SHOWALL($lista,$enfrentamientos);
        break;
        
        break;
        
    case 'EDIT':
        $cruce = get_data_form_edit();
        
        //Variable que almacena la respuesta de la edición de los datos
        $respuesta = $cruce->ganador();
        //crea una vista mensaje con la respuesta y la dirección de vuelta
        new MESSAGE( $respuesta, '../Controllers/CAMPEONATO_CONTROLLER.php' );

        break;
        
        
    case 'EDIT2':
        
        $cruce = get_data_form_edit_2();
        
        //Variable que almacena la respuesta de la edición de los datos
        $respuesta = $cruce->actualizarfecha();
        //crea una vista mensaje con la respuesta y la dirección de vuelta
        new MESSAGE( $respuesta, '../Controllers/CAMPEONATO_CONTROLLER.php' );

        break;
        
        
        break;
        
    case 'METER_FECHA_HORA':
        
        $cruce = new CRUCE;
        $enfrentamientos = $cruce->findAllByGrupo_2($_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        //$enfrentamientos = $enfrentamientos->fetch_assoc();
        $lista = array('ID_PARTIDO','ID_PAREJA_1','ID_PAREJA_2','FASE');
        new CRUCES_FECHA_HORA($lista,$enfrentamientos);
       
       
        break;

    default:
        if ( !$_POST ) {//Si no se han recibido datos 
            $cruce = new CRUCE('', '', '', '', '', '', '', '', '', '', '');//Variable que almacena la intancia de un objeto del modelo USUARIO
        //Si se reciben datos
        } else {
            $cruce = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
        }
        
        $datos = $cruce->findAllByGrupo($_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        $toRet = mysqli_fetch_all($datos);
        //Variable que almacena array con el nombre de los atributos
        $lista = array( 'ID_GRUPO','NOMBRE_CAMPEONATO','NOMBRE_CATEGORIA','NIVEL','ID_PARTIDO','ID_PAREJA_1','ID_PAREJA_2','RESULTADO','GANADOR','FASE','FECHA','HORA');
        new TABLA_CRUCES_SHOWALL( $lista, $toRet);
}

?>