<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}


include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/PAREJAS_SHOWALL.php'; //incluye la vista mensaje
include '../Views/PAREJA_ADD.php'; //incluye la vista mensaje
include '../Models/PAREJA_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/MOSTRAR_PAREJAS.php';
include '../Views/MOSTRAR_PAREJAS_GRUPO.php';
include '../Views/PAREJAS_EDIT.php';
include '../Views/VER_RIVAL.php';

function get_data_form() {

    $id_pareja=$_REQUEST['id_pareja'];
    $capitan= $_REQUEST['capitan'];
    $login_pareja = $_REQUEST['login_pareja'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nombre_campeonato = $_REQUEST['nombre_campeonato'];
    $nivel = $_REQUEST['nivel'];	
    $id_grupo =$_REQUEST['id_grupo'];
    $nombre_categoria_grupo =$_REQUEST['nombre_categoria_grupo'];
    $nombre_campeonato_grupo = $_REQUEST['nombre_campeonato_grupo'];
    $nivel_categoria_grupo = $_REQUEST['nombre_campeonato_grupo'];
    //$action = $_REQUEST[ 'action' ];
	$PAREJA = new PAREJA_MODEL(
         $id_pareja,
         $capitan,
         $login_pareja,
		 $nombre_categoria,
         $nombre_campeonato,
         $nivel,
         $id_grupo,
         $nombre_categoria_grupo,
         $nombre_campeonato_grupo,
         $nivel_categoria_grupo
	);
    
	return $PAREJA;
}

function get_data_form2(){
    
    $id_pareja=$_REQUEST['id_pareja'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nombre_campeonato = $_REQUEST['nombre_campeonato'];
    $id_grupo =$_REQUEST['id_grupo'];
    $nivel = $_REQUEST['nivel'];
    
    //$action = $_REQUEST[ 'action' ];
	$PAREJA = new PAREJA_MODEL(
         $id_pareja,
         '',
         '',
		 $nombre_categoria,
         $nombre_campeonato,
         $nivel,
         $id_grupo,
         '',
         '',
         ''
        
	);
    
	return $PAREJA;
}


if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
        
        
        
    case 'VER_RIVAL': 
            
        $PAREJA = new PAREJA_MODEL($_REQUEST['id_pareja'],'','',$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],$_REQUEST['id_grupo'],'','','');
        
      
        $rival=$PAREJA->verRival($_REQUEST['id_enfrentamiento']);
        
     
        $lista = array('ID_PAREJA','CAPITAN','LOGIN_PAREJA');
         
       new VER_RIVAL($rival,$lista,$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],$_REQUEST['id_grupo'],$_REQUEST['id_enfrentamiento']);
        
      
        
        break;
        
        
    case 'VER_PAREJAS':
        
        $PAREJAS = new PAREJA_MODEL('','','','','','','','','','');
        $parejas= $PAREJAS->buscarParejas();
        
        $lista = array('CAPITAN','LOGIN_PAREJA','NOMBRE_CATEGORIA','NOMBRE_CAMPEONATO','NIVEL');
        new PAREJAS_SHOWALL($lista,$parejas);
        
        break;
    
    case 'INSERTAR_PAREJAS':
        
        $id_grupo  = $_REQUEST['id_grupo'];
        
        $PAREJAS = new PAREJA_MODEL('','','',$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],$id_grupo,'','','');
        $numParejas = $PAREJAS->numParejas();
        $si8=$PAREJAS->si8(); 
       
       
        
        if($si8 >= 8){
            $mensaje ="ya insertaste parejas en este grupo";
        }
        
        else{
             if($numParejas<8){
                $mensaje="hay menos de 8 parejas, reparte las parejas que falten";
            }
            else{
                $mensaje=$PAREJAS->insertarParejasGrupo();
            }
        }
        

        new MESSAGE ($mensaje, "../Controllers/CAMPEONATO_CONTROLLER.php");
        break;
        
        
    case 'MOSTRAR_PAREJAS':    
        
        $id_grupo  = $_REQUEST['id_grupo'];
    
        $PAREJAS = new PAREJA_MODEL('','','',$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],'','','','');
        $datos = $PAREJAS->SEARCH();
        
       
        $lista = array( 'ID_PAREJA','CAPITAN','LOGIN_PAREJA','NOMBRE_CATEGORIA','NOMBRE_CAMPEONATO','NIVEL','ID_GRUPO','NOMBRE_CATEGORIA_GRUPO','NOMBRE_CAMPEONATO_GRUPO');
        
		new MOSTRAR_PAREJAS( $lista, $datos, $id_grupo,$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        
        break;
        
    case 'EDIT':
        if(!$_POST){
           
            
        new PAREJAS_EDIT($_REQUEST['id_grupo'],$_REQUEST['nombre_campeonato'],$_REQUEST['nombre_categoria'],$_REQUEST['id_pareja'],$_REQUEST['nivel']);    
            
        }
        else{
            $PAREJA = get_data_form2();
            $respuesta =$PAREJA->EDIT();
            
            new MESSAGE ($respuesta, "../Controllers/CAMPEONATO_CONTROLLER.php");
        }
        
        break;
        
     
        
    case 'VER_PAREJAS_GRUPO':
            
             $PAREJAS = new PAREJA_MODEL('','','',$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],$_REQUEST['id_grupo'],'','','');
             $datos=$PAREJAS->conseguirParejas();        
        
           
                
              $lista = array( 'CAPITAN','LOGIN_PAREJA');

             new MOSTRAR_PAREJAS_GRUPO( $lista, $datos,$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        
        
        break;
        
        
        
    default:
        
         if(!$_POST){
            
            $capitan = $_SESSION['login']; 
             
            new PAREJA_ADD($_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel'],$capitan);
    }
    else{
    
            $PAREJA= get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
			$respuesta = $PAREJA->ADD($_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);//Variable que almacena la respuesta de la inserción
			new MESSAGE( $respuesta,"../Controllers/CAMPEONATO_CONTROLLER.php" );
    }
}




   

   
   

?>