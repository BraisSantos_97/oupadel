<?php
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
include '../Models/CONTENIDO_MODEL.php'; //incluye el contendio del modelo pista
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/CONTENIDO_ADD.php';
include '../Views/CONTENIDO_DELETE.php';
include '../Views/CONTENIDO_SHOWALLDIV.php';
include '../Views/CONTENIDO_SHOWCURRENT.php';
session_start();

//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

function get_data_form_add(){
    if(isset($_REQUEST['id_contenido'])){
        $id_contenido=$_REQUEST['id_contenido'];
    } else {
        $id_contenido='';
    }
    $titulo=$_REQUEST['titulo'];
    $texto=$_REQUEST['texto'];
    $fecha_publicacion=$_REQUEST['fecha_publicacion'];

    $action=$_REQUEST['action'];

    $contenido=new CONTENIDO($id_contenido,$titulo,$texto,$fecha_publicacion);

    return $contenido;
}

function get_data_form(){
    $id_contenido=$_REQUEST['id_contenido'];
    $titulo=$_REQUEST['titulo'];
    $texto=$_REQUEST['texto'];
    $fecha_publicacion=$_REQUEST['fecha_publicacion'];

    $action=$_REQUEST['action'];

    $contenido=new CONTENIDO($id_contenido,$titulo,$texto,$fecha_publicacion);

    return $contenido;
}

if(!isset($_REQUEST['action'])){
   $_REQUEST['action'] = ''; 
}

switch($_REQUEST['action']){
    case 'ADD':
        if(!$_POST){
            new CONTENIDO_ADD;
        } else {
            $contenido = get_data_form_add();//Variable que almacena un objecto PISTA(modelo) con los datos recogidos
			$respuesta = $contenido->add();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
			new MESSAGE( $respuesta, '../Controllers/CONTENIDO_CONTROLLER.php' );
        }

        break;
    
    case 'DELETE':
        if(!$_POST){
            $contenido = new CONTENIDO($_REQUEST['id_contenido'],'','');
            $contenido = $contenido->findById($_REQUEST['id_contenido']);
            new CONTENIDO_DELETE ($contenido, $_REQUEST['id_contenido']);
        } else {
            $contenido = get_data_form();
            $respuesta = $contenido->delete();
            new MESSAGE($respuesta, '../Controllers/CONTENIDO_CONTROLLER.php');
        }

        break;

    case 'EDIT':
        if(!$_POST){
            $contenido = new CONTENIDO( $_REQUEST[ 'id_contenido' ], '', '', '');
			//Variable que almacena un objecto USUARIO(modelo) con los datos de los atibutos rellenados a traves de login
			$valores = $contenido->findById( $_REQUEST[ 'id_contenido' ] );

			//Muestra la vista del formulario editar
			new CONTENIDO_EDIT( $valores);
        } else {
            $contenido = get_data_form();
			//Variable que almacena la respuesta de la edición de los datos
			$respuesta = $contenido->update();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
			new MESSAGE( $respuesta, '../Controllers/CONTENIDO_CONTROLLER.php' );
        }

        break;

    case 'SHOWCURRENT':
        if ( !$_POST ) {//Si no se han recibido datos 
            $contenido = new CONTENIDO('', '', '');//Variable que almacena la intancia de un objeto del modelo USUARIO
        //Si se reciben datos
        } else {
            $contenido = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
        }
        //Variable que almacena un recordset con el resultado de los datos de la busqueda
        $datos = $contenido->findAll();

        //Variable que almacena array con el nombre de los atributos
        $lista = array( 'ID_CONTENIDO','TITULO','TEXTO','FECHA_PUBLICACION');
        new CONTENIDO_SHOWCURRENT( $lista, $datos);

        break;

    default:
        if ( !$_POST ) {//Si no se han recibido datos 
            $contenido = new CONTENIDO('', '', '');//Variable que almacena la intancia de un objeto del modelo USUARIO
        //Si se reciben datos
        } else {
            $contenido = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
        }
        //Variable que almacena un recordset con el resultado de los datos de la busqueda
        $datos = $contenido->findAll();

        //Variable que almacena array con el nombre de los atributos
        $lista = array( 'ID_CONTENIDO','TITULO','TEXTO','FECHA_PUBLICACION');
        new CONTENIDO_SHOWALL( $lista, $datos);
}

?>