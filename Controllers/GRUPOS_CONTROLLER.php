<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

include '../Views/MIS_ENFRENTAMIENTOS_SHOWALL.php';
include '../Models/GRUPOS_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/GRUPO_SHOWALL.php'; //incluye la vista mensaje
include '../Views/GRUPO_ADD.php'; //incluye la vista mensaje
include '../Views/GRUPOS_DELETE.php'; //incluye la vista mensaje
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/MESSAGE_GRUPO_ADD.php'; //incluye la vista mensaje
include '../Views/MESSAGE_GRUPO_DELETE.php'; //incluye la vista mensaje


//Esta función crea un objeto tipo USUARIO_MODEL con los valores que se le pasan con $_REQUEST
function get_data_form() {

    $id_grupo =$_REQUEST['id_grupo'];
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $nombre_campeonato = $_REQUEST['nombre_campeonato'];
    $nivel = $_REQUEST['nivel'];
    
    $action = $_REQUEST[ 'action' ]; //Variable que almacena el valor de action
	$GRUPOS = new GRUPOS_MODEL(
		 $id_grupo,
         $nombre_categoria,
         $nombre_campeonato,
         $nivel
        
	);//Creamos un objeto de usuario con las variables que se han recibido del formulario
	//Devuelve el valor del objecto model creado
    
	return $GRUPOS;
}
//Si la variable action no tiene contenido le asignamos ''
if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
	case 'ADD'://Caso añadir
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario ADD
            
            
            
				new GRUPO_ADD($_REQUEST['nombre_campeonato'],$_REQUEST['nombre_categoria'],$_REQUEST['nivel']);
			} 
        else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL inserta los datos
			$GRUPO = get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
           
			$respuesta = $GRUPO->ADD();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
            //$campeonato = $_REQUEST['nombre_campeonato'];
            //$categoria = $_REQUEST['nombre_categoria'];
			//new MESSAGE_GRUPO_ADD( $respuesta,$_REQUEST['id_grupo'],$campeonato,$categoria);
            new MESSAGE( $respuesta,'../Controllers/CAMPEONATO_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
        
	case 'DELETE'://Caso borrar
       
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario DELETE
            
			$GRUPO = new GRUPOS_MODEL($_REQUEST['id_grupo'],$_REQUEST[ 'nombre_categoria' ],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
			//Variable que almacena el relleno de los datos utilizando el login
			$valores = $GRUPO->RellenaDatos($_REQUEST['id_grupo'],$_REQUEST[ 'nombre_categoria' ], $_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
              
          
            //Crea una vista delete para ver la tupla
			new GRUPOS_DELETE( $valores,$_REQUEST['id_grupo']);
			}
			 else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL borra los datos
			//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos de los atributos
			$GRUPO = get_data_form();
			//Variable que almacena la respuesta de realizar el borrado
			$respuesta = $GRUPO->DELETE();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
           
          /*  $campeonato = $_REQUEST['nombre_campeonato'];
            $categoria = $_REQUEST['nombre_categoria'];
            $id_grupo = $_REQUEST['id_grupo'];*/
                 
			//new MESSAGE_GRUPO_DELETE( $respuesta,$id_grupo,$campeonato,$categoria);
                 new MESSAGE( $respuesta,'../Controllers/CAMPEONATO_CONTROLLER.php' );
                 
		}
		//Finaliza el bloque
		break;
        
        
        case 'FECHA_PROPUESTA':
        $GRUPOS = new GRUPOS_MODEL($_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        $datos=$GRUPOS->ver_grupos();
       

        $lista=array('ID_GRUPO','NOMBRE_CATEGORIA','NOMBRE_CAMPEONATO','NIVEL');
      
        new MIS_ENFRENTAMIENTOS_SHOWALL( $lista, $datos,$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
    
        
        break;
        
        
	default: //Caso que se ejecuta por defecto
		
		
        if ( !$_POST ) {//Si no se han recibido datos 
            $GRUPO = new GRUPOS_MODEL('',$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);//Variable que almacena la intancia de un objeto del modelo
		} else {
			$GRUPO = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
		}
		//Variable que almacena un recordset con el resultado de los datos de la busqueda
		$datos = $GRUPO->SEARCH();
        $fecha_limite = $GRUPO->devolverFechaLimite($_REQUEST['nombre_campeonato']);
       
		$lista = array('ID_GRUPO','NOMBRE_CAMPEONATO','NOMBRE_CATEGORIA','NIVEL');
        
        $capitan = $GRUPO->siCapitan();
        
		new GRUPO_SHOWALL($lista,$datos,$_REQUEST['nombre_campeonato'],$_REQUEST['nombre_categoria'],$_REQUEST['nivel'],$fecha_limite,$capitan);

        
	}


?>