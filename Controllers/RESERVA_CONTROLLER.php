<?php
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
include '../Models/RESERVA_MODEL.php'; //incluye el contendio del modelo reserva
include '../Models/HORARIO_MODEL.php'; //incluye el contendio del modelo horario
include '../Models/PISTA_MODEL.php'; //incluye el contendio del modelo pista
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/RESERVA_ADD.php';
include '../Views/RESERVA_ADD2.php';
include '../Views/RESERVA_DELETE.php';
include '../Views/RESERVA_SHOWALL.php';
session_start();

//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

function get_data_form(){
    if(!isset($_REQUEST['id_pista'])){
        $id_pista=NULL;
    } else {
        $id_pista=$_REQUEST['id_pista'];
    }
    if(!isset($_REQUEST['fecha'])){
        $fecha='';
    } else {
        $fecha=$_REQUEST['fecha'];
    }
    if(!isset($_REQUEST['hora'])){
        $hora='';
    } else {
        $hora=$_REQUEST['hora'];
    }
    if(!isset($_REQUEST['login'])){
        $login='';
    } else {
        $login=$_REQUEST['login'];
    }

    $action=$_REQUEST['action'];

    $reserva=new RESERVA($id_pista,$fecha,$hora,$login);

    return $reserva;  
}

if(!isset($_REQUEST['action'])){
   $_REQUEST['action'] = ''; 
}

switch($_REQUEST['action']){
    case 'ADD':
        if(!$_POST){
            new RESERVA_ADD;
        } elseif(isset($_POST['date']) && !isset($_POST['id_date'])) {
            $reserva = get_data_form();
            $fecha = $_POST['date'];
            $tabla = $reserva->findByFecha($fecha);
            $horario = new HORARIO('','','');
            $horas = $horario->findHorarioByFecha($fecha);
            $pista = new PISTA('','','');
            $pistas = $pista->getAllPistas();
            new RESERVA_ADD2($fecha,$tabla,$horas,$pistas);
        } else {
            $reserva = get_data_form();//Variable que almacena un objecto PISTA(modelo) con los datos recogidos
            $fechaAnterior = $reserva->esPasada($reserva->getFecha(),$reserva->getHora());
            if($fechaAnterior == false){
                $respuesta = $reserva->add();//Variable que almacena la respuesta de la inserción
                //Crea la vista con la respuesta y la ruta para volver
                new MESSAGE( $respuesta, '../Controllers/RESERVA_CONTROLLER.php' );
            } else {
                new MESSAGE( "No se puede reservar una pista en una fecha pasada", '../Controllers/RESERVA_CONTROLLER.php' );
            }
        }

        break;
    
    case 'DELETE':
        if(!$_POST){
            $reserva = get_data_form();
            $menosDeDoceHoras = $reserva->menosDeDoceHoras($_REQUEST['fecha'],$_REQUEST['hora']);
            $reserva = new RESERVA($_REQUEST['id_pista'],$_REQUEST['fecha'],$_REQUEST['hora'],$_REQUEST['login']);
            if($reserva->findReserva($_REQUEST['id_pista'],$_REQUEST['fecha'],$_REQUEST['hora'])){
                new RESERVA_DELETE ($reserva, $_REQUEST['id_pista'],$_REQUEST['fecha'],$_REQUEST['hora']);
            } else {
                new MESSAGE("No existe esa reserva", '../Controllers/RESERVA_CONTROLLER.php' );
            }
            
        } else {
            $reserva = get_data_form();
            $menosDeDoceHoras = $reserva->menosDeDoceHoras($reserva->fecha,$reserva->hora);
            if($menosDeDoceHoras){
                new MESSAGE("No puedes cancelar una reserva con menos de doce horas de antelación", '../Controllers/RESERVA_CONTROLLER.php');
            } else {
                $respuesta = $reserva->delete();
                new MESSAGE($respuesta, '../Controllers/RESERVA_CONTROLLER.php');
            } 
        }
        break;

    default:
        if (!$_POST) {//Si no se han recibido datos 
            $reserva = new Reserva('', '', '');//Variable que almacena la intancia de un objeto del modelo RESERVA
        //Si se reciben datos
        } else {
            $reserva = get_data_form();//Variable que almacena los valores de un objeto RESERVA_MODEL
        }

        //Variable que almacena array con el nombre de los atributos
        if($_SESSION['login'] == 'admin'){
            $datos = $reserva->findAll();
        } else {
            $datos = $reserva->findByLogin($_SESSION['login']);
        }
        $lista = array('ID_PISTA','FECHA','HORA','LOGIN');
        new PISTA_SHOWALL( $lista, $datos);
}

?>