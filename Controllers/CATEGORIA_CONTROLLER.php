<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}

include '../Models/CATEGORIA_MODEL.php'; //incluye el contendio del modelo usuarios
include '../Views/CATEGORIA_SHOWALL.php'; //incluye la vista mensaje
include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/MESSAGE_CATEGORIA_ADD.php'; //incluye la vista mensaje
include '../Views/CATEGORIA_ADD.php';
include '../Views/CATEGORIA_DELETE.php';


//Esta función crea un objeto tipo USUARIO_MODEL con los valores que se le pasan con $_REQUEST
function get_data_form() {

    
    $nombre_categoria=$_REQUEST['nombre_categoria'];
    $descripcion_categoria = $_REQUEST['descripcion_categoria'];
    $nombre_campeonato = $_REQUEST['nombre_campeonato'];
    $nivel = $_REQUEST['nivel'];
    
    $action = $_REQUEST[ 'action' ]; //Variable que almacena el valor de action
	$CATEGORIA = new CATEGORIA_MODEL(
		 $nombre_categoria,
		 $descripcion_categoria,
         $nombre_campeonato,
         $nivel
        
	);//Creamos un objeto de usuario con las variables que se han recibido del formulario
	//Devuelve el valor del objecto model creado
    
	return $CATEGORIA;
}
//Si la variable action no tiene contenido le asignamos ''
if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {
	case 'ADD'://Caso añadir
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario ADD
				new CATEGORIA_ADD($_REQUEST['nombre_campeonato']);
			} 
        else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL inserta los datos
			$CATEGORIA = get_data_form();//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos
           
			$respuesta = $CATEGORIA->ADD();//Variable que almacena la respuesta de la inserción
			//Crea la vista con la respuesta y la ruta para volver
            $campeonato = $_REQUEST['nombre_campeonato'];
			//new MESSAGE_CATEGORIA_ADD( $respuesta,$campeonato );
            new MESSAGE( $respuesta,'../Controllers/CAMPEONATO_CONTROLLER.php' );
		}
		//Finaliza el bloque
		break;
        
	case 'DELETE'://Caso borrar
		if ( !$_POST ) {//Si no se han recibido datos se envia a la vista del formulario DELETE
			$CATEGORIA = new CATEGORIA_MODEL( $_REQUEST[ 'nombre_categoria' ], '',$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
			//Variable que almacena el relleno de los datos utilizando el login
			$valores = $CATEGORIA->RellenaDatos($_REQUEST[ 'nombre_categoria' ], $_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
                
            //Crea una vista delete para ver la tupla
			new CATEGORIA_DELETE( $valores,$_REQUEST['nombre_categoria'],$_REQUEST['nivel']);
			}
			 else {//Si recibe datos los recoge y mediante las funcionalidad de USUARIO_MODEL borra los datos
			//Variable que almacena un objecto USUARIO(modelo) con los datos recogidos de los atributos
			$CATEGORIA = get_data_form();
			//Variable que almacena la respuesta de realizar el borrado
			$respuesta = $CATEGORIA->DELETE();
			//crea una vista mensaje con la respuesta y la dirección de vuelta
           
                 new MESSAGE( $respuesta,'../Controllers/CAMPEONATO_CONTROLLER.php' );
                 
		}
		//Finaliza el bloque
		break;
	
        
        
	default: //Caso que se ejecuta por defecto
	
        
        if ( !$_POST ) {//Si no se han recibido datos 
            $CATEGORIA = new CATEGORIA_MODEL('', '',$_REQUEST['nombre_campeonato'],'');//Variable que almacena la intancia de un objeto del modelo USUARIO
		//Si se reciben datos
		} else {
			$CATEGORIA = get_data_form();//Variable que almacena los valores de un objeto USUARIO_MODEL
		}
		//Variable que almacena un recordset con el resultado de los datos de la busqueda
		$datos = $CATEGORIA->SEARCH();
        
       // var_dump($datos);
       // exit;
        
        $fecha_limite = $CATEGORIA->devolverFechaLimite($_REQUEST['nombre_campeonato']);
        
        
		//Variable que almacena array con el nombre de los atributos
		$lista = array('NOMBRE_CATEGORIA','DESCRIPCION_CATEGORIA','NOMBRE_CAMPEONATO','NIVEL');
		new CATEGORIA_SHOWALL( $lista, $datos,$_REQUEST['nombre_campeonato'],$fecha_limite);

        
	}


?>