<?php

session_start(); //solicito trabajar con la session
include '../Functions/Authentication.php'; //incluye el contenido de la función de autentificación
//Si no esta autenticado se redirecciona al index
if (!IsAuthenticated()){
	//Redireción al index
 	header('Location:../index.php');
}


include '../Views/MESSAGE.php'; //incluye la vista mensaje
include '../Views/PROPONER_FECHA_SHOWALL.php'; //incluye la vista mensaje
include '../Views/FECHAS_ACORDADAS_SHOWALL.php'; //incluye la vista mensaje
include '../Views/VER_FECHAS_SHOWALL.php'; //incluye la vista mensaje
include '../Models/PROPONER_FECHA_MODEL.php'; //incluye el contendio del modelo usuarios




function get_data_form(){
    
    $id_fecha_propuesta = $_REQUEST['id_fecha_propuesta'];
    $fecha_propuesta = $_REQUEST['fecha_propuesta'];
    $hora_propuesta = $_REQUEST['hora_propuesta'];
    $id_enfrentamiento=$_REQUEST['id_enfrentamiento'];
    $id_pareja = $_REQUEST['id_pareja'];
    $nombre_campeonato = $_REQUEST['nombre_campeonato'];
    $nombre_categoria   = $_REQUEST['nombre_categoria'];  
    $id_grupo  = $_REQUEST['id_grupo'];
    $nivel = $_REQUEST['nivel'];
    
    //$action = $_REQUEST[ 'action' ];
	$PROPONER_FECHA = new PROPONER_FECHA_MODEL(
          $id_fecha_propuesta,
          $fecha_propuesta,
          $hora_propuesta,
          $id_enfrentamiento,
          $id_pareja,
          $nombre_campeonato,
          $nombre_categoria,
          $id_grupo,
          $nivel
          
     
	);
    
	return $PROPONER_FECHA;
}


if ( !isset( $_REQUEST[ 'action' ] ) ) {
	$_REQUEST[ 'action' ] = '';
	
}
//Estructura de control, que realiza un determinado caso dependiendo del valor action
switch ( $_REQUEST[ 'action' ] ) {


    case 'FECHAS_ACEPTADAS':
        
        $PROPONER_FECHA = new PROPONER_FECHA_MODEL('','','',$_REQUEST['id_enfrentamiento'],'',$_REQUEST['nombre_campeonato'],$_REQUEST['nombre_categoria'],$_REQUEST['id_grupo'],$_REQUEST['nivel']);
        
        $datos = $PROPONER_FECHA->enfrentamientosAcordados();
       $lista=array('ID_ENFRENTAMIENTO','NOMBRE_CAMPEONATO','NOMBRE_CATEGORIA','NIVEL','ID_GRUPO','FECHA_ENFRENTAMIENTO','HORA_ENFRENTAMIENTO');  
        
        new FECHAS_ACORDADAS_SHOWALL( $lista, $datos,$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        
        break;
        
    case 'PROPONER_FECHA':
        
        if(!$_POST){
        
        
        $PROPONER_FECHA = new PROPONER_FECHA_MODEL('','','','','',$_REQUEST['nombre_campeonato'],$_REQUEST['nombre_categoria'],$_REQUEST['id_grupo'],$_REQUEST['nivel']);
        $datos=$PROPONER_FECHA->tuplasEnfrentamientos($_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);

            $lista=array('ID_ENFRENTAMIENTO','ID_GRUPO','NOMBRE_CATEGORIA','NOMBRE_CAMPEONATO','NIVEL','ID_PAREJA','CAPITAN','LOGIN_PAREJA');    
      

        
        new PROPONER_FECHA_SHOWALL( $lista, $datos,$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
        
        }
        else{
            $PROPONER_FECHA = get_data_form();
            $respuesta =$PROPONER_FECHA->ADD($_REQUEST['nombre_campeonato']);
            
            new MESSAGE ($respuesta, "../Controllers/CAMPEONATO_CONTROLLER.php");
        }
        break;
        
        
        
    case 'VER_PROPUESTAS':
       
        
        if(!$_POST){
         
        
            
             $PROPONER_FECHA = new PROPONER_FECHA_MODEL('','','',$_REQUEST['id_enfrentamiento'],$_REQUEST['id_pareja'],$_REQUEST['nombre_campeonato'],$_REQUEST['nombre_categoria'],$_REQUEST['id_grupo'],$_REQUEST['nivel']);
            
           
            
            $datos=$PROPONER_FECHA->verPropuestas($_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
           
        
            $lista=array('ID_ENFRENTAMIENTO','NOMBRE_CAMPEONATO','NOMBRE_CATEGORIA','ID_GRUPO','NIVEL','FECHA_PROPUESTA','HORA_PROPUESTA','ID_PAREJA');
            
            
   
            new VER_FECHAS_SHOWALL($lista, $datos,$_REQUEST['id_grupo'],$_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
            
        }
        
        else{

            
            $PROPONER_FECHA = get_data_form();
            $rival = $PROPONER_FECHA->enfrentamientosRival($_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
            $misEnfrentamientos = $PROPONER_FECHA->misEnfrentamientos($_REQUEST['nombre_categoria'],$_REQUEST['nombre_campeonato'],$_REQUEST['nivel']);
            $dosPartidos= $PROPONER_FECHA->dosPartidos($rival,$misEnfrentamientos);
            
           
            
            if($dosPartidos >= 1){
                $respuesta = "Ya no puedes jugar más partidos con esta pareja";
            }
            else{
                $respuesta =$PROPONER_FECHA->EDIT($_REQUEST['nombre_campeonato'],$_REQUEST['nombre_categoria'],$_REQUEST['id_grupo'],$_REQUEST['nivel']);
            }
            
            
            new MESSAGE ($respuesta, "../Controllers/CAMPEONATO_CONTROLLER.php");
        }
        
        
        
      
        break;

       
}




   

?>