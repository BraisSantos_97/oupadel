<?php

//declaración de la clase
class CLASES_MODEL{ 

	var $id_clase; // declaración del atributo login
    var $fecha_clase;//declaración del atributo password
	var $hora_clase; // declaración del atributo Nombre
	var $nombre_escuela;
	var $id_pista;
	
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($id_clase,$fecha_clase,$hora_clase,$nombre_escuela,$id_pista) {
		
		$this->id_clase=$id_clase;
        $this->fecha_clase=$fecha_clase;
        $this->hora_clase=$hora_clase;
        $this->nombre_escuela=$nombre_escuela;
        $this->id_pista=$id_pista;
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
		$this->mysqli = ConectarBD();
		$this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor
    

    function RellenaDatos() { 

		$sql = "SELECT * FROM clase WHERE (ID_CLASE = '$this->id_clase')";// se construye la sentencia de busqueda de la tupla
		// Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; // 
		} else { // si existe se devuelve la tupla resultado
            //Aplicamos fetch_array sobre $resultado para crear un array y se guarda en $result
			$result = $resultado->fetch_array();
			return $result;
		}
        
	}


	function SEARCH() {
		// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
		$sql = "select  
                    ID_CLASE,
					FECHA_CLASE,
					HORA_CLASE,
					NOMBRE_ESCUELA,
					ID_PISTA
       			from clase
    			where 
    				(
					(BINARY ID_CLASE LIKE '%$this->id_clase%') &&
                    (BINARY FECHA_CLASE LIKE '%$this->fecha_clase%') &&
					(BINARY HORA_CLASE LIKE '%$this->hora_clase%') && 
					(BINARY NOMBRE_ESCUELA = '$this->nombre_escuela')

    				)";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
	}


	function DELETE() {    

			$sql = "DELETE FROM clase WHERE (ID_CLASE = '$this->id_clase')";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en el borrado sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return 'Borrado correctamente';
		}
	} 
function ADD() {
				
			$sql = "INSERT INTO clase (
                                    FECHA_CLASE,
                                    HORA_CLASE,
                                    NOMBRE_ESCUELA
                                       ) 
                                      VALUES(
                                      '$this->fecha_clase',
                                      '$this->hora_clase',
                                      '$this->nombre_escuela'
                                      )";
            
            
            
						
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						/*if($mensaje == 'Inserción realizada con éxito'){//miramos si la inserción en USU_GRUPO tuvo exito
							return 'Inserción realizada con éxito'; //operacion de insertado correcta
						}else{//si la insercion no tuvo exito
							return $mensaje;
						}*/
						return 'Insercion realizada con exito';
						
					}


    }

	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct

function EDIT2() {
		// se construye la sentencia de busqueda de la tupla en la bd
		$sql = "SELECT * FROM clase WHERE (ID_CLASE = '$this->id_clase')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE clase SET 
                    ID_PISTA = '$this->id_pista'
                   
				
				WHERE ( ID_CLASE = '$this->id_clase'
				)";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'No existe en la base de datos';
		}
	} 
	
	}
 ?>
