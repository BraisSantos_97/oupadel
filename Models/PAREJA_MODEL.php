<?php

//declaración de la clase
class PAREJA_MODEL{ 

	var $id_pareja; // declaración del atributo login
    var $capitan;//declaración del atributo password
	var $login_pareja; // declaración del atributo Nombre
	var $nombre_categoria; // declaración del atributo Apellidos
	var $nombre_campeonato; // declaración del atributo Telefono
    var $nivel;
    var $id_grupo;
    var $nombre_categoria_grupo;
    var $nombre_campeonato_grupo;
    var $nivel_categoria_grupo;
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($id_pareja,$capitan,$login_pareja,$nombre_categoria,$nombre_campeonato,$nivel,$id_grupo,$nombre_categoria_grupo,$nombre_campeonato_grupo,$nivel_categoria_grupo) {
		
		$this->id_pareja = $id_pareja;//declaracion de la variable que almacena login
        $this->capitan=$capitan;//declaracion de la variable que almacena password
		$this->login_pareja = $login_pareja;//declaracion de la variable que almacena nombre
		$this->nombre_categoria = $nombre_categoria;//declaracion de la variable que almacena apellidos
		$this->nombre_campeonato = $nombre_campeonato;//declaracion de la variable que almacena telefono
        $this->nivel=$nivel;
		$this->id_grupo =$id_grupo;
        $this->nombre_categoria_grupo=$nombre_categoria_grupo;
        $this->nombre_campeonato_grupo =$nombre_campeonato_grupo;
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor
    
    function conseguirParejas(){
        $sql="SELECT DISTINCT CAPITAN,LOGIN_PAREJA FROM grupo g, pareja p, deportista_pareja dp WHERE g.ID_GRUPO=p.ID_GRUPO AND p.ID_PAREJA=dp.ID_PAREJA AND g.ID_GRUPO='$this->id_grupo' AND p.NOMBRE_CATEGORIA='$this->nombre_categoria' AND p.NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND  p.NIVEL ='$this->nivel' ";
        
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
        
        
    }
    

        function SEARCH() {
		// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
		$sql = "select  
                    ID_PAREJA,
					CAPITAN,
					LOGIN_PAREJA,
					NOMBRE_CATEGORIA,
					NOMBRE_CAMPEONATO,
                    NIVEL,
                    ID_GRUPO,
                    NOMBRE_CATEGORIA_GRUPO,
                    NOMBRE_CAMPEONATO_GRUPO
                   
       			from pareja
    			where 
    				(
				    NOMBRE_CATEGORIA = '$this->nombre_categoria' &&
	 				NOMBRE_CAMPEONATO = '$this->nombre_campeonato' &&
                    NIVEL = '$this->nivel'
                  
    				)";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
	} // fin metodo SEARCH
    
    function EDIT(){
       
        $sql = "select * from pareja where ID_GRUPO='$this->id_grupo' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NIVEL='$this->nivel'";
        
        $result = $this->mysqli->query( $sql );
        
        $filas = $result->num_rows;
        
        if($filas == 12){
            
            return "Solo puede haber un máximo de 12 parejas por grupo";
            
        }
        
			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE pareja SET 
                    ID_GRUPO= '$this->id_grupo',
                    NOMBRE_CATEGORIA_GRUPO = '$this->nombre_categoria',
                    NOMBRE_CAMPEONATO_GRUPO = '$this->nombre_campeonato',
                    NIVEL_CATEGORIA_GRUPO ='$this->nivel'
				
				WHERE ( ID_PAREJA= '$this->id_pareja'
				)";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

    }
   
    
    function buscarParejas(){
        if($_SESSION['login'] == "admin"){
            $sql ="SELECT DISTINCT CAPITAN,LOGIN_PAREJA, NOMBRE_CATEGORIA, NOMBRE_CAMPEONATO,NIVEL FROM pareja p, deportista_pareja dp, deportista d
             WHERE d.LOGIN=dp.LOGIN AND dp.ID_PAREJA=p.ID_PAREJA";
            
            if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			     return 'Error en la consulta sobre la base de datos';
		       } else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		    }
        }
        else{
            $milogin=$_SESSION['login'];
            $sql ="SELECT d.LOGIN,CAPITAN,LOGIN_PAREJA, NOMBRE_CATEGORIA, NOMBRE_CAMPEONATO,NIVEL FROM pareja p, deportista_pareja dp, deportista d
             WHERE d.LOGIN=dp.LOGIN AND dp.ID_PAREJA=p.ID_PAREJA AND d.LOGIN='$milogin'";
            
            if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			     return 'Error en la consulta sobre la base de datos';
		       } else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		    }
        }
    }
    
    function numParejas(){
        
        
        $sql ="SELECT ID_GRUPO,NOMBRE_CATEGORIA,NOMBRE_CAMPEONATO,NIVEL FROM pareja where NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND NIVEL='$this->nivel' AND ID_GRUPO IS NULL AND NOMBRE_CATEGORIA_GRUPO IS NULL AND NOMBRE_CAMPEONATO_GRUPO IS NULL AND NIVEL_CATEGORIA_GRUPO IS NULL";
        
        if(!$result = $this->mysqli->query( $sql )){
            return 'No se ha podido conectar con la base de datos';
        }
        else{
            
            $filas = $result->num_rows;
            
        }
        
        return $filas;
    }
    
    function si8(){
        
          $sql ="SELECT * FROM pareja where NOMBRE_CAMPEONATO_GRUPO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA_GRUPO='$this->nombre_categoria' AND NIVEL_CATEGORIA_GRUPO='$this->nivel' AND ID_GRUPO='$this->id_grupo'";
        
        if(!$result = $this->mysqli->query( $sql )){
            return 'No se ha podido conectar con la base de datos';
        }
        else{
            
            $filas = $result->num_rows;
            
        }
        
        return $filas;
    }
    
    
    function insertarParejasGrupo(){
                
        $sql ="SELECT ID_GRUPO,NOMBRE_CATEGORIA,NOMBRE_CAMPEONATO,NIVEL  FROM pareja where NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND NIVEL='$this->nivel' AND ID_GRUPO IS NULL AND NOMBRE_CATEGORIA_GRUPO IS NULL AND NOMBRE_CAMPEONATO_GRUPO IS NULL AND NIVEL_CATEGORIA_GRUPO IS NULL";
        
         if(!$result = $this->mysqli->query( $sql )){
            return 'No se ha podido conectar con la base de datos';
        }
        
        else{
            
            $sql = "SELECT ID_PAREJA FROM pareja where NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND NIVEL='$this->nivel' AND ID_GRUPO IS NULL AND NOMBRE_CATEGORIA_GRUPO IS NULL AND NOMBRE_CAMPEONATO_GRUPO IS NULL AND NIVEL_CATEGORIA_GRUPO IS NULL";
            
            $resultado = $this->mysqli->query( $sql );
            
            $id=array();
            $i=0;
            
           while($fila = mysqli_fetch_array($resultado)){
            
               $id[$i]=$fila['ID_PAREJA'];
              
               $i++;
               
           }
            
            $i=0;
            $p=0;
          
        
            while($fila = mysqli_fetch_array($result)){
                
                
             for($i=0;$i<4;$i++){
               
                switch ($i) {
                    case 0:
                        $sql = "UPDATE pareja SET 
                                ID_GRUPO= '$this->id_grupo'
                        WHERE ( ID_PAREJA= '$id[$p]'
                        )";
                        $res = $this->mysqli->query( $sql );
                        break;
                    case 1:
                         $sql = "UPDATE pareja SET 
                                NOMBRE_CATEGORIA_GRUPO= '$fila[$i]'
                        WHERE ( ID_PAREJA= '$id[$p]'
                        )";
                       $res = $this->mysqli->query( $sql );
                    
                        break;
                    case 2:
                         $sql = "UPDATE pareja SET 
                                NOMBRE_CAMPEONATO_GRUPO= '$fila[$i]'
                        WHERE ( ID_PAREJA= '$id[$p]'
                        )";
                        $res = $this->mysqli->query( $sql );
                    
                        break;
                    case 3:
                         $sql = "UPDATE pareja SET 
                                NIVEL_CATEGORIA_GRUPO= '$fila[$i]'
                        WHERE ( ID_PAREJA= '$id[$p]'
                        )";
                        $res = $this->mysqli->query( $sql );
                        
                        break;    
                }
                
                
             }
                $i=0;
                $p++;

             if($p==8){
                break;
             }
               
                     
            }
            
             
               
           }    
        
     
        return "Insercion de parejas realizada con éxito";
    }


    function verRival($enfrentamiento){
        
        $milogin = $_SESSION['login'];
        
        $sql ="SELECT p.ID_PAREJA,CAPITAN,LOGIN_PAREJA FROM enfrentamiento e, pareja_enfrentamiento pe, pareja p where e.ID_ENFRENTAMIENTO=pe.ID_ENFRENTAMIENTO AND pe.ID_PAREJA=p.ID_PAREJA AND e.ID_ENFRENTAMIENTO= '$enfrentamiento' AND p.CAPITAN != '$milogin'";
        
         if(!$result = $this->mysqli->query( $sql )){
            return 'No se ha podido conectar con la base de datos';
        }
        else{
            return $result;
        }
    }


	//Metodo ADD()
	//Inserta en la tabla  de la bd  los valores
	// de los atributos del objeto. Comprueba si la clave/s esta vacia y si 
	//existe ya en la tabla
	function ADD($categoria,$campeonato,$nivel) {
        
        
        $milogin=$_SESSION['login'];
        
        $sql ="SELECT DISTINCT CAPITAN,LOGIN_PAREJA FROM pareja   WHERE ( NOMBRE_CAMPEONATO='$campeonato' AND NOMBRE_CATEGORIA='$categoria' AND (CAPITAN = '$milogin' OR  CAPITAN = '$this->login_pareja' OR
        LOGIN_PAREJA ='$milogin' OR  LOGIN_PAREJA = '$this->login_pareja'))";
        
        if(!$result = $this->mysqli->query( $sql )){
            return 'No se ha podido conectar con la base de datos';
        }
        else{
            
        
         if($result->num_rows != 0 ){
                return "ADVERTENCIA: 1.No puedes volver a participar en la misma categoria del campeonato/2.No puedes meter de capitan a un deportista que no seas tu";
            }
        else{
        
			// construimos el sql para buscar esa clave en la tabla
        $milogin=$_SESSION['login'];
        
        $sql ="SELECT DISTINCT CAPITAN,LOGIN_PAREJA FROM pareja p, campeonato c, categoria cc  WHERE ( c.NOMBRE_CAMPEONATO=cc.NOMBRE_CAMPEONATO  AND cc.NOMBRE_CAMPEONATO=p.NOMBRE_CAMPEONATO AND cc.NOMBRE_CATEGORIA = p.NOMBRE_CATEGORIA AND cc.NIVEL=P.NIVEL AND cc.NOMBRE_CAMPEONATO='$campeonato' AND cc.NOMBRE_CATEGORIA='$categoria' AND  (CAPITAN = '$milogin' OR  CAPITAN = '$this->login_pareja' OR
        LOGIN_PAREJA ='$milogin' OR  LOGIN_PAREJA = '$this->login_pareja'))";
        
        
        
        if(!$result = $this->mysqli->query( $sql )){
            return 'No se ha podido conectar con la base de datos';
        }
        else{
            if($result->num_rows != 0 ){
                return "ADVERTENCIA: 1.No pueden participar deportistas que ya estan inscritos/2.No puedes meter de capitan a un deportista que no seas tu";
            }
       
            else{
                if(($this->capitan == $this->login_pareja && $this->login_pareja==$_SESSION['login']) || ($this->login_pareja == $_SESSION['login']) || ($this->capitan == $this->login_pareja )){
                    return 'ADVERTENCIA: 1.Tienes que ser el capitan/2.No puedes participar tu solo';
                }
            
           
        else{
            
       
			$sql = "SELECT * FROM deportista  WHERE (  LOGIN = '$this->login_pareja')";
       

			if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows == 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						return 'No está ese login en la base de datos';// ya existe
                    }
                     else{
                            $sql = "SELECT * FROM deportista  WHERE (  LOGIN = '$this->capitan')";
                     if(!$result = $this->mysqli->query( $sql )){
                                    return 'No se ha podido conectar con la base de datos';
                                }
                    else{
                                if ( $result->num_rows == 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
                                    return 'No está ese login en la base de datos';// ya existe
                                }
                     else{
                         
                            $sql = "SELECT SEXO FROM deportista where LOGIN ='$this->capitan'";
                            $sql_pareja = "SELECT SEXO FROM deportista where LOGIN='$this->login_pareja'";
                         
                            $capi = $this->mysqli->query( $sql );
                            $row = $capi->fetch_array(MYSQLI_ASSOC);
                         
                            $couple = $this->mysqli->query( $sql_pareja );
                            $row_p = $couple->fetch_array(MYSQLI_ASSOC);
                        
                          if(($row['SEXO']== $categoria && $row_p['SEXO']==$categoria) || ($categoria=='Mixto' && ( ($row['SEXO']=='Masculino' && $row_p['SEXO']=='Femenino') || ($row['SEXO']=='Femenino' && $row_p['SEXO']=='Masculino')) )){
                          
                        
                                if($this->capitan == $_SESSION['login']){
                                    
                                    
                                    
                                                $sql = "INSERT INTO pareja (
                                                CAPITAN,
                                                LOGIN_PAREJA,
                                                NOMBRE_CATEGORIA,
                                                NOMBRE_CAMPEONATO,
                                                NIVEL,
                                                ID_GRUPO,
                                                NOMBRE_CATEGORIA_GRUPO,
                                                NOMBRE_CAMPEONATO_GRUPO,
                                                NIVEL_CATEGORIA_GRUPO
                                                 ) 
                                                VALUES(
                                                '$this->capitan',
                                                '$this->login_pareja',
                                                '$this->nombre_categoria',
                                                '$this->nombre_campeonato',
                                                '$this->nivel',
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL
                                                
                                                )";
                                    
                                   
                                
                                            if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
                                                return 'Error en la inserción';
                                            } else { //si no da error en la insercion devolvemos mensaje de exito

                                            
                                                $sql = "SELECT ID_PAREJA FROM pareja WHERE CAPITAN='$this->capitan' AND LOGIN_PAREJA='$this->login_pareja' AND NOMBRE_CATEGORIA ='$this->nombre_categoria' AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NIVEL='$this->nivel'";
                                                $resultado = $this->mysqli->query( $sql );//hacemos la consulta en la base de datos
                                                $row = $resultado->fetch_array(MYSQLI_ASSOC);
                                                
                                            if(!$row['ID_PAREJA']){
                                                return 'no hay id pareja';
                                            }
                                            else{
                                                include '../Models/DEPORTISTA_PAREJA_MODEL.php';
                                                $D_PAREJA = new DEPORTISTA_PAREJA_MODEL($_SESSION['login'],$row['ID_PAREJA']);//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
                                                
                                                $D_PAREJA2 = new DEPORTISTA_PAREJA_MODEL($this->login_pareja,$row['ID_PAREJA']);
                                                
                                                $mensaje = $D_PAREJA->ADD();//insertamos el login en el grupo alumnos*/
                                                $mensaje2 = $D_PAREJA2->ADD();
                                                
                                                return "Insercion realizada con éxito";
                                            }
                                                

                                        }

                                            
                                            }
                                        else{
                                            return 'El capitan no eres tú ni ese login porque no está registrado';
                                        }
                         
                             
                                            }
                         else{
                             return 'Alguno de los login introducidos no pueden participar en dicha categoria';
                         }
                                    }
                            }
                        }
             
			
		              }
                
                         }
                }
               } 
             }
                
        }       
                                           

    }
	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct



} //fin de clase

?>