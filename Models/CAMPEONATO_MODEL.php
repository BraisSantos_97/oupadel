<?php

//declaración de la clase
class CAMPEONATO_MODEL{ 

	var $nombre; // declaración del atributo login
    var $fecha_inicio;//declaración del atributo password
	var $fecha_final; // declaración del atributo Nombre
	var $descripcion; // declaración del atributo Apellidos
	var $fecha_limite; // declaración del atributo Telefono
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($nombre,$fecha_inicio,$fecha_final,$descripcion,$fecha_limite) {
		
		$this->nombre = $nombre;//declaracion de la variable que almacena login
        $this->fecha_inicio=$fecha_inicio;//declaracion de la variable que almacena password
		$this->fecha_final = $fecha_final;//declaracion de la variable que almacena nombre
		$this->descripcion = $descripcion;//declaracion de la variable que almacena apellidos
		$this->fecha_limite = $fecha_limite;//declaracion de la variable que almacena telefono
		
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
		$this->mysqli = ConectarBD();
		$this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor

	//funcion SEARCH: hace una búsqueda en la tabla con
	//los datos proporcionados. Si van vacios devuelve todos
	function SEARCH() {
		// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
		$sql = "select  
                    NOMBRE_CAMPEONATO,
					FECHA_INICIO,
					FECHA_FINAL,
					DESCRIPCION_CAMPEONATO,
					FECHA_LIMITE_INSCRIPCION
       			from campeonato
    			where 
    				(
					(BINARY NOMBRE_CAMPEONATO LIKE '%$this->nombre%') &&
                    (BINARY FECHA_INICIO LIKE '%$this->fecha_inicio%') &&
					(BINARY FECHA_FINAL LIKE '%$this->fecha_final%') &&
	 				(BINARY DESCRIPCION_CAMPEONATO LIKE '%$this->descripcion%') &&
	 				(BINARY FECHA_LIMITE_INSCRIPCION LIKE '%$this->fecha_limite%')
    				)";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
	} // fin metodo SEARCH


	//Metodo ADD()
	//Inserta en la tabla  de la bd  los valores
	// de los atributos del objeto. Comprueba si la clave/s esta vacia y si 
	//existe ya en la tabla
	function ADD() {
        
        
                    $fecha_actual=date("d/m/Y");
                    $porciones = explode("/", $fecha_actual);
                    $dia_actual= intval($porciones[0]);
                    $mes_actual= intval($porciones[1]);
                    $ano_actual= intval($porciones[2]);
                    
                    $inicio=explode("/", $this->fecha_inicio);
                    $dia_inicio= intval($inicio[0]);
                    $mes_inicio= intval($inicio[1]);
                    $ano_inicio= intval($inicio[2]);
        
                    $final=explode("/", $this->fecha_final);
                    $dia_final= intval($final[0]);
                    $mes_final= intval($final[1]);
                    $ano_final= intval($final[2]);
                    
        
                    $limite=explode("/", $this->fecha_limite);
                    $dia_limite= intval($limite[0]);
                    $mes_limite= intval($limite[1]);
                    $ano_limite= intval($limite[2]);
        
                    
        
        if(($dia_limite>=$dia_actual && $mes_limite>=$mes_actual && $ano_limite>=$ano_actual) || ($mes_limite>$mes_actual && $ano_limite >=$ano_actual) || ($ano_limite>$ano_actual)){
                
                if(($dia_inicio>=$dia_actual && $mes_inicio>=$mes_actual && $ano_inicio>=$ano_actual) || ($mes_inicio>$mes_actual && $ano_inicio >=$ano_actual) || ($ano_inicio>$ano_actual)){
                    
                    if(($dia_final>=$dia_actual && $mes_final>=$mes_actual && $ano_final>=$ano_actual) || ($mes_final>$mes_actual && $ano_final >=$ano_actual) || ($ano_final>$ano_actual)){
                        
                            if(($dia_inicio>=$dia_limite && $mes_inicio>=$mes_limite && $ano_inicio>=$ano_limite) || ($mes_inicio>$mes_limite && $ano_inicio >=$ano_limite) || ($ano_inicio>$ano_limite)){
                                
                                if(($dia_final>=$dia_limite && $mes_final>=$mes_limite && $ano_final>=$ano_limite) || ($mes_final>$mes_limite && $ano_final >=$ano_limite) || ($ano_final>$ano_limite)){
                                    
                                    if(($dia_final>=$dia_inicio && $mes_final>=$mes_inicio && $ano_final>=$ano_inicio) || ($mes_final>$mes_inicio && $ano_final >=$ano_inicio) || ($ano_final>$ano_inicio)){
                                        
                                        
                                            
		if ( ( $this->nombre <> '' ) ) { // si el atributo clave de la entidad no esta vacio
            
			// construimos el sql para buscar esa clave en la tabla
			$sql = "SELECT * FROM campeonato WHERE (  NOMBRE_CAMPEONATO = '$this->nombre')";

			if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows != 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						return 'Ya existe ese campeonato en la base de datos';// ya existe
						
					} else {

							$sql = "INSERT INTO campeonato (
                                 NOMBRE_CAMPEONATO,
                                FECHA_INICIO,
                                FECHA_FINAL,
                                DESCRIPCION_CAMPEONATO,
                                FECHA_LIMITE_INSCRIPCION
                                 ) 
								VALUES(
                                '$this->nombre',
                                '$this->fecha_inicio',
                                '$this->fecha_final',
                                '$this->descripcion',
                                '$this->fecha_limite'
								)";
						
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						
						return 'Insercion realizada con exito';
						
					}

				}
			
		} 
	} else { // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
			return 'Introduzca un valor'; // introduzca un valor para el usuario
		}
                                        
                                        
                                    }
                                    else{
                                          return "las fechas no se corresponden";
                                    }
                                  
                                }
                                else{
                                    return "las fechas no se corresponden";
                                }
                                
                                
                            }
                        else{
                            return "las fechas no se corresponden";
                        }
                        
                    }
                    else{
                        return "las fechas no se corresponden";
                    }
                }
            else{
                return "las fechas no se corresponden";
            }
            
            
        }
        else{
            return "las fechas no se corresponden";
        }
		
        
        
        
        
	/*	if ( ( $this->nombre <> '' ) ) { // si el atributo clave de la entidad no esta vacio
            
			// construimos el sql para buscar esa clave en la tabla
			$sql = "SELECT * FROM campeonato WHERE (  NOMBRE_CAMPEONATO = '$this->nombre')";

			if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows != 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						return 'Ya existe ese campeonato en la base de datos';// ya existe
						
					} else {

							$sql = "INSERT INTO campeonato (
                                 NOMBRE_CAMPEONATO,
                                FECHA_INICIO,
                                FECHA_FINAL,
                                DESCRIPCION_CAMPEONATO,
                                FECHA_LIMITE_INSCRIPCION
                                 ) 
								VALUES(
                                '$this->nombre',
                                '$this->fecha_inicio',
                                '$this->fecha_final',
                                '$this->descripcion',
                                '$this->fecha_limite'
								)";
						
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						
						return 'Insercion realizada con exito';
						
					}

				}
			
		} 
	} else { // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
			return 'Introduzca un valor'; // introduzca un valor para el usuario
		}*/

    }
	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct

	// funcion DELETE()
	// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
	// se manda un mensaje de que ese valor de clave no existe
	function DELETE() {
		// se construye la sentencia sql de busqueda con los atributos de la clase
		$sql = "SELECT * FROM campeonato WHERE (NOMBRE_CAMPEONATO = '$this->nombre')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		

		if ( $result->num_rows == 1 ) {// si existe una tupla con ese valor de clave
			// se construye la sentencia sql de borrado
			$sql = "DELETE FROM campeonato WHERE (NOMBRE_CAMPEONATO = '$this->nombre' )";
			// se ejecuta la query
			$this->mysqli->query( $sql );
			// se devuelve el mensaje de borrado correcto
			return "Borrado correctamente";
		} // si no existe el login a borrar se devuelve el mensaje de que no existe
		else
			return "No existe";
	} // fin metodo DELETE

	// funcion RellenaDatos()
	// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
	// en el atributo de la clase
	function RellenaDatos() { 

		$sql = "SELECT * FROM campeonato WHERE (NOMBRE_CAMPEONATO = '$this->nombre')";// se construye la sentencia de busqueda de la tupla
		// Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; // 
		} else { // si existe se devuelve la tupla resultado
            //Aplicamos fetch_array sobre $resultado para crear un array y se guarda en $result
			$result = $resultado->fetch_array();
			return $result;
		}
        
	} // fin del metodo RellenaDatos()
    
   

    
    

	// funcion EDIT()
	// Se comprueba que la tupla a modificar exista en base al valor de su clave primaria
	// si existe se modifica
	function EDIT() {
		// se construye la sentencia de busqueda de la tupla en la bd
		$sql = "SELECT * FROM campeonato WHERE (NOMBRE_CAMPEONATO = '$this->nombre')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE campeonato SET 
                    NOMBRE_CAMPEONATO= '$this->nombre',
                    FECHA_INICIO = '$this->fecha_inicio',
                    FECHA_FINAL='$fecha_final',
                    DESCRIPCION_CAMPEONATO = '$this->descripcion',
                    FECHA_LIMITE_INSCRIPCION = '$this->fecha_limite'
				
				WHERE ( NOMBRE_CAMPEONATO = '$this->nombre'
				)";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'No existe en la base de datos';
		}
	} // fin del metodo EDIT


} //fin de clase

?>