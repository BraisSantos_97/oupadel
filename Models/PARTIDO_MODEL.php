<?php

//declaración de la clase
class PARTIDO_MODEL{ 

	   var $id_partido;
		var $fecha_partido;
        var $hora_partido;
        var $id_pista;
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($id_partido,$fecha_partido,$hora_partido,$id_pista) {
		
		$this->id_partido = $id_partido;//declaracion de la variable que almacena login
        $this->fecha_partido=$fecha_partido;//declaracion de la variable que almacena password
		$this->hora_partido = $hora_partido;//declaracion de la variable que almacena nombre
		$this->id_pista = $id_pista;//declaracion de la variable que almacena apellidos
	

		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
		$this->mysqli = ConectarBD();
		$this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor

	//funcion SEARCH: hace una búsqueda en la tabla con
	//los datos proporcionados. Si van vacios devuelve todos
	function SEARCH() {
		// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
		$sql = "select  
                    ID_PARTIDO,
					FECHA_PARTIDO,
					HORA_PARTIDO,
					ID_PISTA
       			from partido
    			where 
    				(
					(BINARY ID_PARTIDO LIKE '%$this->id_partido%') &&
                    (BINARY FECHA_PARTIDO LIKE '%$this->fecha_partido%') &&
					(BINARY HORA_PARTIDO LIKE '%$this->hora_partido%')
	 				)";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
	} // fin metodo SEARCH


    function ADD() {
		if ( ( $this->id_partido <> '' ) ) { // si el atributo clave de la entidad no esta vacio
            
			// construimos el sql para buscar esa clave en la tabla
			$sql = "SELECT * FROM partido WHERE (  ID_PARTIDO = '$this->id_partido')";

			if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows != 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						return 'Ya existe ese partido en la base de datos';// ya existe
						
					} else {

							$sql = "INSERT INTO partido (
                                ID_PARTIDO,
								FECHA_PARTIDO,
								HORA_PARTIDO
                                 ) 
								VALUES(
                                '$this->id_partido',
                                '$this->fecha_partido',
                                '$this->hora_partido'
								)";
							/*include_once '../Models/USU_GRUPO_MODEL.php';//incluimos el modelo USU_GRUPO
							$USU_GRUPO = new USU_GRUPO($this->login,'00001A');//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
							$mensaje = $USU_GRUPO->ADD();//insertamos el login en el grupo alumnos*/

					//var_dump($sql);
                       // exit;
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						/*if($mensaje == 'Inserción realizada con éxito'){//miramos si la inserción en USU_GRUPO tuvo exito
							return 'Inserción realizada con éxito'; //operacion de insertado correcta
						}else{//si la insercion no tuvo exito
							return $mensaje;
						}*/
						return 'Insercion realizada con exito';
						
					}

				}
			
		} 
	} else { // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
			return 'Introduzca un valor'; // introduzca un valor para el usuario
		}

    }
    
 function EDIT2() {
		// se construye la sentencia de busqueda de la tupla en la bd
		$sql = "SELECT * FROM partido WHERE (ID_PARTIDO = '$this->id_partido')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE partido SET 
                    ID_PISTA = '$this->id_pista'
                   
				
				WHERE ( ID_PARTIDO = '$this->id_partido'
				)";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'No existe en la base de datos';
		}
	} // fin del metodo EDIT
    
    
    
	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct

	// funcion DELETE()
	// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
	// se manda un mensaje de que ese valor de clave no existe
	function DELETE() {
		// se construye la sentencia sql de busqueda con los atributos de la clase
		$sql = "SELECT * FROM partido WHERE (ID_PARTIDO = '$this->id_partido')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		

		if ( $result->num_rows == 1 ) {// si existe una tupla con ese valor de clave
			// se construye la sentencia sql de borrado
			$sql = "DELETE FROM partido WHERE (ID_PARTIDO = '$this->id_partido' )";
			// se ejecuta la query
			$this->mysqli->query( $sql );
            
            include_once '../Models/RESERVA_MODEL.php';
            
                    $par=explode("/",$this->fecha_partido);
                      $dia= intval($par[0]);
                      $mes= intval($par[1]);
                      $ano= intval($par[2]);

                      $date = $ano.'-'.$mes.'-'.$dia;
            
            
            
            $sq = "SELECT * FROM horario_pista WHERE  HORA='$this->hora_partido'  AND LOGIN  ='admin' AND FECHA='$date' AND ID_PISTA='$this->id_pista'";
            
           
            $resul = $this->mysqli->query( $sq );
            
            if($resul->num_rows == 1 ){
                $s = "DELETE FROM horario_pista WHERE (ID_PISTA = '$this->id_pista' )";
                // se ejecuta la query
                $this->mysqli->query( $s );
            }
            
            
            
			// se devuelve el mensaje de borrado correcto
			return "Borrado correctamente";
		} // si no existe el login a borrar se devuelve el mensaje de que no existe
		else
			return "No existe";
	} // fin metodo DELETE

	// funcion RellenaDatos()
	// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
	// en el atributo de la clase
	function RellenaDatos() { 

		$sql = "SELECT * FROM partido WHERE (ID_PARTIDO = '$this->id_partido')";// se construye la sentencia de busqueda de la tupla
		// Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; // 
		} else { // si existe se devuelve la tupla resultado
            //Aplicamos fetch_array sobre $resultado para crear un array y se guarda en $result
			$result = $resultado->fetch_array();
			return $result;
		}
        
	} // fin del metodo RellenaDatos()
    
   

    
    

	// funcion EDIT()
	// Se comprueba que la tupla a modificar exista en base al valor de su clave primaria
	// si existe se modifica
	function EDIT() {
		// se construye la sentencia de busqueda de la tupla en la bd
		$sql = "SELECT * FROM partido WHERE (ID_PARTIDO = '$this->id_partido')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE partido SET 
                    ID_PARTIDO= '$this->id_partido',
                    FECHA_PARTIDO = '$this->fecha_partido',
                    HORA_PARTIDO='$this->hora_partido',
                    ID_PISTA = '$this->id_pista'
                   
				
				WHERE ( ID_PARTIDO = '$this->id_partido'
				)";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'No existe en la base de datos';
		}
	} // fin del metodo EDIT


} //fin de clase

?>