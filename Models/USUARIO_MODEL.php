<?php

/*
 Función: modelo de datos definida en una clase que permite interactuar con la base de datos
 Fecha de creación:23/11/2017 Autor:Brais Santos
*/

//declaración de la clase
class USUARIO_MODEL{ 

	var $login; // declaración del atributo login
    var $password;//declaración del atributo password
	var $nombre; // declaración del atributo Nombre
	var $apellidos; // declaración del atributo Apellidos
	var $telefono; // declaración del atributo Telefono
    var $sexo;
    var $email;
	var $mysqli; // declaración del atributo manejador de la bd
	
	

    //Constructor de la clase
	function __construct($login,$password,$nombre,$apellidos,$telefono,$sexo,$email) {
		//asignación de valores de parámetro a los atributos de la clase
		$this->login = $login;//declaracion de la variable que almacena login
        $this->password=$password;//declaracion de la variable que almacena password
		$this->nombre = $nombre;//declaracion de la variable que almacena nombre
		$this->apellidos = $apellidos;//declaracion de la variable que almacena apellidos
		$this->telefono = $telefono;//declaracion de la variable que almacena telefono
		$this->sexo =$sexo;
        $this->email = $email;
        
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
		$this->mysqli = ConectarBD();
		$this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor

	//funcion SEARCH: hace una búsqueda en la tabla con
	//los datos proporcionados. Si van vacios devuelve todos
	function SEARCH() {
		// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
		$sql = "select  
                    NOMBRE,
					APELLIDO,
					TELEFONO,
					LOGIN,
					EMAIL,
					PASSWORD,
                    SEXO
       			from deportista 
    			where 
    				(
					(BINARY LOGIN LIKE '%$this->login%') &&
					(BINARY EMAIL LIKE '%$this->email%') &&
                    (BINARY PASSWORD LIKE '%$this->password%') &&
					(BINARY NOMBRE LIKE '%$this->nombre%') &&
	 				(BINARY APELLIDO LIKE '%$this->apellidos%') &&
	 				(BINARY TELEFONO LIKE '%$this->telefono%') &&
                    (BINARY TELEFONO LIKE '%$this->sexo%')
    				)";
        

        
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
	} // fin metodo SEARCH


	//Metodo ADD()
	//Inserta en la tabla  de la bd  los valores
	// de los atributos del objeto. Comprueba si la clave/s esta vacia y si 
	//existe ya en la tabla
    
	function ADD() {
		if ( ( $this->login <> '' ) ) { // si el atributo clave de la entidad no esta vacio
            
			// construimos el sql para buscar esa clave en la tabla
			$sql = "SELECT * FROM deportista WHERE (  LOGIN = '$this->login')";

			if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows != 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						return 'Ya existe un usuario con ese login en la base de datos';// ya existe
						
					} else {

							$sql = "INSERT INTO deportista (
                                 NOMBRE,
							     APELLIDO,
                                 TELEFONO,
                                 LOGIN,
								 EMAIL,
					             PASSWORD,
                                 SEXO
                                 ) 
								VALUES(
                                '$this->nombre',
                                '$this->apellidos',
                                '$this->telefono',
                                '$this->login',
								'$this->email',
                                '$this->password',
                                '$this->sexo'
								)";
							/*include_once '../Models/USU_GRUPO_MODEL.php';//incluimos el modelo USU_GRUPO
							$USU_GRUPO = new USU_GRUPO($this->login,'00001A');//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
							$mensaje = $USU_GRUPO->ADD();//insertamos el login en el grupo alumnos*/

					//var_dump($sql);
                       // exit;
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						/*if($mensaje == 'Inserción realizada con éxito'){//miramos si la inserción en USU_GRUPO tuvo exito
							return 'Inserción realizada con éxito'; //operacion de insertado correcta
						}else{//si la insercion no tuvo exito
							return $mensaje;
						}*/
						return 'Insercion realizada con exito';
						
					}

				}
			
		} 
	} else { // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
			return 'Introduzca un valor'; // introduzca un valor para el usuario
		}

    }
	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct

	// funcion DELETE()
	// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
	// se manda un mensaje de que ese valor de clave no existe
	function DELETE() {
		// se construye la sentencia sql de busqueda con los atributos de la clase
		$sql = "SELECT * FROM deportista WHERE (LOGIN = '$this->login')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		

		if ( $result->num_rows == 1 ) {// si existe una tupla con ese valor de clave
			// se construye la sentencia sql de borrado
			$sql = "DELETE FROM deportista WHERE (LOGIN = '$this->login' )";
			// se ejecuta la query
			$this->mysqli->query( $sql );
			// se devuelve el mensaje de borrado correcto
			return "Borrado correctamente";
		} // si no existe el login a borrar se devuelve el mensaje de que no existe
		else
			return "No existe";
	} // fin metodo DELETE

	// funcion RellenaDatos()
	// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
	// en el atributo de la clase
	function RellenaDatos() { 

		$sql = "SELECT * FROM deportista WHERE (LOGIN = '$this->login')";// se construye la sentencia de busqueda de la tupla
		// Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; // 
		} else { // si existe se devuelve la tupla resultado
            //Aplicamos fetch_array sobre $resultado para crear un array y se guarda en $result
			$result = $resultado->fetch_array();
			return $result;
		}
        
	} // fin del metodo RellenaDatos()
    
   

    
    

	// funcion EDIT()
	// Se comprueba que la tupla a modificar exista en base al valor de su clave primaria
	// si existe se modifica
	function EDIT() {
		// se construye la sentencia de busqueda de la tupla en la bd
		$sql = "SELECT * FROM deportista WHERE (LOGIN = '$this->login')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE deportista SET 
                    NOMBRE = '$this->nombre',
                    APELLIDO = '$this->apellidos',
                    PASSWORD='$this->password',
                    TELEFONO = '$this->telefono',
                    LOGIN = '$this->login',
					EMAIL = '$this->email',
                    SEXO ='$this->sexo'
				
				WHERE ( LOGIN= '$this->login'
				)";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'No existe en la base de datos';
		}
	} // fin del metodo EDIT



	//Con esta función vemos si ya está registrado el usuario, sino lo registramos
	function Register() {
        
		$sql = "select * from deportista where LOGIN = '" . $this->login . "'";//miramos los usuarios cuyo login es igual al que nos pasan

		$result = $this->mysqli->query( $sql ); //hacemos la consulta en la base de datos.
		if ( $result->num_rows == 1 ) { // existe el usuario
			return 'El usuario ya existe';
		} else {
			   return true; //no existe el usuario
            }
		}

	 //fin del método Register
        
        // funcion login: realiza la comprobación de si existe el usuario en la bd y despues si la pass
	   // es correcta para ese usuario. Si es asi devuelve true, en cualquier otro caso devuelve el 
	   // error correspondiente
	function login() {
        //hacemos la consulta para saber que usuario tiene dicho login
		$sql = "SELECT *
			FROM deportista
			WHERE (
				(LOGIN = '$this->login' AND PASSWORD='$this->password') 
			)";
        
		$resultado = $this->mysqli->query( $sql );//hacemos la consulta en la base de datos
		if ( $resultado->num_rows == 0 ) {//miramos si el numero de filas es 0
			return 'El usuario no existe';
		} else {//si no es 0, el usuario existe
			//$tupla = $resultado->fetch_array();//devolvemos la tupla
                return true;
			
		}
	} //fin metodo login
    
   
   

} //fin de clase

?>