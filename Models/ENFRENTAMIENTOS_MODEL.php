<?php

//declaración de la clase
class ENFRENTAMIENTOS_MODEL{ 

	var $id_enfrentamiento; // declaración del atributo login
    var $nombre_grupo;//declaración del atributo password
	var $nombre_categoria; // declaración del atributo Nombre
	var $nombre_campeonato; // declaración del atributo Apellidos
    var $nivel;
	var $resultado; // declaración del atributo Telefono
    var $fecha_e;
    var $hora_e;
    var $id_pista;
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($id_enfrentamiento,$nombre_grupo,$nombre_categoria,$nombre_campeonato,$nivel,$resultado,$fecha_e,$hora_e,$id_pista) {
		
		$this->id_enfrentamiento=$id_enfrentamiento;
        $this->nombre_grupo=$nombre_grupo;
        $this->nombre_categoria=$nombre_categoria;
        $this->nombre_campeonato=$nombre_campeonato;
        $this->nivel=$nivel;
        $this->resultado=$resultado;
        $this->fecha_e=$fecha_e;
        $this->hora_e=$hora_e;
        $this->id_pista=$id_pista;
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor
    
 function DELETE() {
		// se construye la sentencia sql de busqueda con los atributos de la clase
		$sql = "SELECT * FROM enfrentamiento WHERE (ID_ENFRENTAMIENTO = '$this->id_enfrentamiento' AND RESULTADO IS  NULL)";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		

		if ( $result->num_rows == 1 ) {// si existe una tupla con ese valor de clave
			// se construye la sentencia sql de borrad
            
            
			$sql = "DELETE FROM enfrentamiento WHERE (ID_ENFRENTAMIENTO = '$this->id_enfrentamiento')";
			// se ejecuta la query
			$this->mysqli->query( $sql );
			// se devuelve el mensaje de borrado correcto
			return "Borrado correctamente";
		} // si no existe el login a borrar se devuelve el mensaje de que no existe
		else
			return "No se puede borrar porque ya se jugó";
	} // fin metodo DELETE
    
    
    function  DELETE_ACORDADO(){
            
		// se ejecuta la query
        $sql = "SELECT * FROM enfrentamiento WHERE (ID_ENFRENTAMIENTO = '$this->id_enfrentamiento' AND ID_GRUPO = '$this->nombre_grupo' AND NOMBRE_CATEGORIA = '$this->nombre_categoria' AND NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND RESULTADO IS NULL)";
       
        
          if ( !( $result = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
          }
        
        else{
            
      
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

        if($this->esPasada($this->fecha_e,$this->hora_e)==true){
            
       
			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE enfrentamiento SET 
                    FECHA_ENFRENTAMIENTO = NULL,
                    HORA_ENFRENTAMIENTO= NULL,
                    ID_PISTA = NULL
            
				WHERE ( ID_ENFRENTAMIENTO = '$this->id_enfrentamiento' AND ID_GRUPO = '$this->nombre_grupo' AND NOMBRE_CATEGORIA = '$this->nombre_categoria' AND NOMBRE_CAMPEONATO = '$this->nombre_campeonato'
				)";
            
            
            
            $row = $result->fetch_array(MYSQLI_ASSOC);
            
            include_once '../Models/RESERVA_MODEL.php';//incluimos el modelo USU_GRUPO
                        
            $date = $this->convertirFecha($this->fecha_e);
            
				   $RESERVA = new Reserva('', '', '', '');//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
                   $RESERVA->setIdPista($row['ID_PISTA']);
                   $RESERVA->setHora($this->hora_e);
                   $RESERVA->setFecha($date);
                   $RESERVA->setLogin('admin');
                            
				$m=$RESERVA->delete();//insertamos el login en el grupo alumnos*   
            
            
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}
            
             }
            else{
                return 'Ya ha pasado la fecha del partido';
            }
         
		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'Ya ha pasado la fecha del partido';
		}
          }
    }
    private function convertirFecha($fecha){ 
        $date = str_replace('/', '-', $fecha); 
        return date('Y-m-d', strtotime($date)); 
    }
    
  public function esPasada($fecha,$hora){
        //$hoy=localtime();
        $hoyFecha=date("Y-m-d");
        $hoyHora=date("H:i");
    
     
        if($fecha < $hoyFecha){
            return true;
        } else {  
            if($fecha == $hoyFecha){
                if($hora > $hoyHora){            
                    return false;
                } else{
                    return true;
                }
            }
            
            if($fecha > $hoyFecha){
                return false;
            }

            return true;
        }
    }
    

   
    function SEARCH(){

        $sql = "select  
                    ID_ENFRENTAMIENTO,
                    ID_GRUPO,
                    NOMBRE_CATEGORIA,
                    NOMBRE_CAMPEONATO,
                    FECHA_ENFRENTAMIENTO,
                    HORA_ENFRENTAMIENTO,
                    ID_PISTA,
                    NIVEL
                from enfrentamiento
                where 
                    (
                    NOMBRE_CATEGORIA = '$this->nombre_categoria' &&
                    NOMBRE_CAMPEONATO = '$this->nombre_campeonato' &&
                    ID_GRUPO = '$this->nombre_grupo' &&
                    NIVEL = '$this->nivel'
                  
                    )";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }
    }
    
    function generarEnfrentamientos(){
        
        
        $sq = "SELECT COUNT(*) AS tam FROM enfrentamiento WHERE NOMBRE_CATEGORIA = '$this->nombre_categoria' && NOMBRE_CAMPEONATO = '$this->nombre_campeonato' &&  ID_GRUPO = '$this->nombre_grupo' && NIVEL = '$this->nivel'";
        
         $res = $this->mysqli->query( $sq );
         $row = $res->fetch_array(MYSQLI_ASSOC);
        
        if($row['tam']>=1){
            return "ya se generó los enfrentamientos";
        }
            
        
        $sql ="SELECT ID_PAREJA FROM pareja WHERE NOMBRE_CATEGORIA = '$this->nombre_categoria' && NOMBRE_CAMPEONATO = '$this->nombre_campeonato' &&  ID_GRUPO = '$this->nombre_grupo' && NIVEL = '$this->nivel'";
        
      if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            $id=array();
            $i=0;
            
          
           while($fila = mysqli_fetch_array($resultado)){
            
               $id[$i]=$fila['ID_PAREJA'];
               $i++;
               
           }
          
            $principal=0;
            $secundario=1;
            $bucle=0;
            $j=0;
          
          $total = $resultado->num_rows;
          
       for($j=$principal;$j<$total;$j++){
           
           for($bucle=$secundario;$bucle<$total;$bucle++){
                        $sql = "INSERT INTO enfrentamiento (
                                                  NOMBRE_CAMPEONATO,
                                                  NOMBRE_CATEGORIA,
                                                  ID_GRUPO,
                                                  NIVEL
                                                         ) 
                                                    VALUES(
                                                    '$this->nombre_campeonato',
                                                    '$this->nombre_categoria',
                                                    '$this->nombre_grupo',
                                                    '$this->nivel'
                                                        )";
                  
              
                                    $resultado = $this->mysqli->query( $sql );
                                        
                                    $sql = "SELECT MAX(ID_ENFRENTAMIENTO)  AS ID_ENFRENTAMIENTO FROM enfrentamiento";
                                    $resultado = $this->mysqli->query( $sql );//hacemos la consulta en la base de datos
                                    $row = $resultado->fetch_array(MYSQLI_ASSOC);
                                    $enfrentamiento= $row['ID_ENFRENTAMIENTO']; 
                        
                                                	
                                 include_once '../Models/PAREJA_ENFRENTAMIENTO_MODEL.php';//incluimos el modelo USU_GRUPO
                                   $PE_1 = new PAREJA_ENFRENTAMIENTO_MODEL($id[$j],$enfrentamiento,$this->nombre_grupo,$this->nombre_categoria,$this->nombre_campeonato,$this->nivel);//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
                                   $m= $PE_1->ADD();//insertamos el login en el grupo alumnos*   

               

                                   $PE_2 = new PAREJA_ENFRENTAMIENTO_MODEL($id[$bucle],$enfrentamiento,$this->nombre_grupo,$this->nombre_categoria,$this->nombre_campeonato,$this->nivel);//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
                                  $m= $PE_2->ADD();//insertamos el login en el grupo alumnos*  
            
                
               
                    }
                $principal++;
                $secundario++;
               
            }
           
          
            
        }
        return "Generación de enfrentamientos realizada con éxito";
    }

    function VER_CLASIFICACION(){
        $sql = "SELECT  
                    ID_PAREJA,
                    CAPITAN,
                    LOGIN_PAREJA,
                    PUNTOS
                FROM pareja
                WHERE 
                    (
                    NOMBRE_CATEGORIA = '$this->nombre_categoria' AND
                    NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND
                    ID_GRUPO = '$this->nombre_grupo' AND
                    NIVEL ='$this->nivel'
                  
                    )
                ORDER BY PUNTOS DESC";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }


    }

    function ver_enfrentamientos(){

        $milogin =$_SESSION['login'];

        
        $sql = "SELECT ID_GRUPO,NOMBRE_CATEGORIA,NOMBRE_CAMPEONATO FROM grupo g, pareja p where g.ID_GRUPO = p.ID_GRUPO 
        AND g.NOMBRE_CATEGORIA=p.NOMBRE_CATEGORIA_GRUPO AND g.NOMBRE_GRUPO=p.NOMBRE_CATEGORIA_GRUPO AND g.NIVEL=p.NIVEL_CATEGORIA_GRUPO AND CAPITAN='$milogin'";

         // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }

    }
 
    function todosEnfrentamientos(){
        
        $sql = "SELECT e.ID_ENFRENTAMIENTO,FECHA_ENFRENTAMIENTO,HORA_ENFRENTAMIENTO,RESULTADO,CAPITAN,LOGIN_PAREJA,ID_PISTA FROM enfrentamiento e, pareja p, pareja_enfrentamiento pe where e.ID_ENFRENTAMIENTO=pe.ID_ENFRENTAMIENTO AND pe.ID_PAREJA=p.ID_PAREJA
        AND  e.NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND e.NOMBRE_CATEGORIA = '$this->nombre_categoria' AND e.ID_GRUPO = '$this->nombre_grupo' AND e.NIVEL = '$this->nivel' ORDER BY e.ID_ENFRENTAMIENTO
        ";

         // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }
    }




    function SEARCH1(){
        $sql = "SELECT e.ID_ENFRENTAMIENTO,e.FECHA_ENFRENTAMIENTO,e.HORA_ENFRENTAMIENTO,p.ID_PAREJA,CAPITAN, LOGIN_PAREJA  
        FROM enfrentamiento e,pareja_enfrentamiento pe,pareja p 
        WHERE ( e.NOMBRE_CATEGORIA = '$this->nombre_categoria' AND 
                e.NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND 
                e.ID_GRUPO = '$this->nombre_grupo' AND 
                e.NIVEL ='$this->nivel' AND
                e.ID_ENFRENTAMIENTO=pe.ID_ENFRENTAMIENTO AND 
                pe.ID_PAREJA=p.ID_PAREJA AND
                e.RESULTADO IS NULL AND e.FECHA_ENFRENTAMIENTO IS NOT NULL)  ORDER BY e.ID_ENFRENTAMIENTO";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado
            
            
            return $resultado;
        }
    }
    
    
    function editar(){
 
   
		// se ejecuta la query
        $sql = "SELECT * FROM enfrentamiento WHERE (ID_ENFRENTAMIENTO = '$this->id_enfrentamiento' AND ID_GRUPO = '$this->nombre_grupo' AND NOMBRE_CATEGORIA = '$this->nombre_categoria' AND NOMBRE_CAMPEONATO = '$this->nombre_campeonato')";
       
        
          if ( !( $result = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
          }
        
        else{
            
      
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE enfrentamiento SET 
                    FECHA_ENFRENTAMIENTO = '$this->fecha_e',
                    HORA_ENFRENTAMIENTO='$this->hora_e',
                    ID_PISTA = '$this->id_pista'
            
				WHERE ( ID_ENFRENTAMIENTO = '$this->id_enfrentamiento' AND ID_GRUPO = '$this->nombre_grupo' AND NOMBRE_CATEGORIA = '$this->nombre_categoria' AND NOMBRE_CAMPEONATO = '$this->nombre_campeonato'
				)";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'No existe en la base de datos';
		}
          }    
    }
    

    function EDIT($pareja1,$pareja2){

         $ganador="";
        
        $ptsGanador=3;
        $ptsPerdidos=1;
        $puntos1=0;
        $puntos2=0;
        
        
        $sql = "UPDATE enfrentamiento SET 
                    RESULTADO= '$this->resultado'
                WHERE ( ID_ENFRENTAMIENTO= '$this->id_enfrentamiento' AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->nombre_grupo' AND NIVEL='$this->nivel'
                )";
            // si hay un problema con la query se envia un mensaje de error en la modificacion
            if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
                return 'Error en la modificación';
            } else { // si no hay problemas con la modificación se indica que se ha modificado
              
                if (strlen($this->resultado)<8) {
                     $porciones = explode("/", $this->resultado);
                     $set1 = explode("-",$porciones[0]);
                     $set2 = explode("-", $porciones[1]);


                     if((intval($set1[0]) > intval($set1[1])) && ( intval($set2[0]) >  intval($set2[1]))) {
                        $ganador=$pareja1;
                     }else{
                        if ((intval($set1[0]) < intval($set1[1])) && ( intval($set2[0]) < intval($set2[1]))) {
                            $ganador=$pareja2;
                        }
                     }
                }else{
                    if (strlen($this->resultado)>=8) {
                         $porciones = explode("/", $this->resultado);
                        $set1 = explode("-", $porciones[0]);
                        $set2 = explode("-", $porciones[1]);
                        $set3 = explode("-", $porciones[2]);
                        
                        
                     
                        
                        if ((intval($set1[0]) > intval($set1[1])) && ( intval($set2[0]) <  intval($set2[1])) && (intval($set3[0]) > intval($set3[1]))) {
                            $ganador=$pareja1;
                        }else{
                            if ((intval($set1[0]) < intval($set1[1])) && ( intval($set2[0]) >  intval($set2[1])) && (intval($set3[0]) < intval($set3[1]))) {
                                $ganador=$pareja2;
                            }
                            else{
                                if((intval($set1[0]) < intval($set1[1])) && ( intval($set2[0]) >  intval($set2[1])) && (intval($set3[0]) > intval($set3[1]))){
                                    $ganador=$pareja1;
                                }
                                else{
                                    if((intval($set1[0]) > intval($set1[1])) && ( intval($set2[0]) <  intval($set2[1])) && (intval($set3[0]) < intval($set3[1]))){
                                         $ganador=$pareja2;
                                    }
                                }
                            }
                        }


                    }
                }


                $sql = "SELECT PUNTOS FROM pareja WHERE ID_PAREJA='$pareja1'";
                $resultado1 = $this->mysqli->query( $sql );//hacemos la consulta en la base de datos
                $row1 = $resultado1->fetch_array(MYSQLI_ASSOC);

                if ($ganador==$pareja1) {

                    $ptos=$row1['PUNTOS'];
                    $puntos1= $ptos+$ptsGanador;
                }else{
                    $puntos=$row1['PUNTOS'];
                    $puntos1= $puntos+$ptsPerdidos;
                }


                $sql2 = "UPDATE pareja SET 
                    PUNTOS= '$puntos1'
                
                WHERE ( ID_PAREJA= '$pareja1'
                )";
                $resultado2 = $this->mysqli->query( $sql2 );//hacemos la consulta en la base de datos


                $sql3 = "SELECT PUNTOS FROM pareja WHERE ID_PAREJA='$pareja2'";
                $resultado3 = $this->mysqli->query( $sql3 );//hacemos la consulta en la base de datos
                $row3 = $resultado3->fetch_array(MYSQLI_ASSOC);

                if ($ganador==$pareja2) {
                    $puntos=$row3['PUNTOS'];
                    $puntos2= $puntos+$ptsGanador;
                }else{
                    $puntos=$row3['PUNTOS'];
                    $puntos2= $puntos+$ptsPerdidos;
                }

               

                $sql4 = "UPDATE pareja SET 
                    PUNTOS= '$puntos2'
                
                WHERE ( ID_PAREJA= '$pareja2'
                )";
                
                $resultado4 = $this->mysqli->query( $sql4 );//hacemos la consulta en la base de datos




                return 'Modificado correctamente';
            }
    }
    
    	function ADD() {
				
							$sql = "INSERT INTO enfrentamiento (
                                                      NOMBRE_CAMPEONATO,
                                                      NOMBRE_CATEGORIA,
                                                      ID_GRUPO,
                                                      FECHA_ENFRENTAMIENTO,
                                                      HORA_ENFRENTAMIENTO,
                                                      ID_PISTA,
                                                      NIVEL
                                                         ) 
                                                        VALUES(
                                                        '$this->nombre_campeonato',
                                                        '$this->nombre_categoria',
                                                        '$this->nombre_grupo',
                                                        '$this->fecha_e',
                                                        '$this->hora_e',
                                                        '$this->id_pista',
                                                        '$this->nivel'
                                                        )";
            
            
            
							/*include_once '../Models/USU_GRUPO_MODEL.php';//incluimos el modelo USU_GRUPO
							$USU_GRUPO = new USU_GRUPO($this->login,'00001A');//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
							$mensaje = $USU_GRUPO->ADD();//insertamos el login en el grupo alumnos*/

					//var_dump($sql);
                       // exit;
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						/*if($mensaje == 'Inserción realizada con éxito'){//miramos si la inserción en USU_GRUPO tuvo exito
							return 'Inserción realizada con éxito'; //operacion de insertado correcta
						}else{//si la insercion no tuvo exito
							return $mensaje;
						}*/
						return 'Insercion realizada con exito';
						
					}


    }
    

	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct



} //fin de clase

?>