<?php
//Acceso a BD
include_once '../Functions/BdAdmin.php';

class Horario{
    var $fecha;
    var $hora;
    var $dia;

    public function __construct($fecha=NULL,$hora=NULL,$dia=NULL){
		//asignación de valores de parámetro a los atributos de la clase
        $this->fecha = $fecha;
        $this->hora = $hora;
        $this->dia = $dia;

        // conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    public function getFecha(){
        return $this->fecha;
    }

    public function setFecha(){
        $this->fecha = $fecha;
    }

    public function getHora(){
        return $this->hora;
    }

    public function setHora($hora){
        $this->hora = $hora;
    }

    public function getDia(){
        return $this->dia;
    }

    public function setDia(){
        $this->dia = $dia;
    }



    public function findByFechaHora($fecha,$hora){
        //$sentencia = "SELECT * FROM HORARIO WHERE FECHA=$this->fecha AND HORA=$this->hora";
        $resultado = $this->mysqli->query("SELECT * FROM HORARIO WHERE FECHA='".$this->fecha."' AND HORA='".$this->hora."'");
        if($resultado != false){
            $horario = $resultado->fetch_assoc();
        }else{
            $horario = NULL;
        }

        if($horario != NULL){
            return new Horario($horario["FECHA"],$horario["HORA"],$horario["DIA"]);
        } else {
            return NULL;
        }
    }

    public function findHorarioByFecha($fecha){ 
        $toRet = NULL;
        $fecha = $this->convertirFecha($fecha);         
        $sentencia = "SELECT HORA FROM HORARIO WHERE FECHA='".$fecha."'"; 
        $resultado = $this->mysqli->query($sentencia); 
        $toRet = array(); 
        if($resultado != false){ 
            $matrix = $resultado->fetch_all(); 
            $toRet = $this->matrixToArray($matrix); 
        }else{ 
            $toRet = NULL; 
        } 
 
        return $toRet; 
    } 

    public function findAll(){
        $sentencia = "SELECT * FROM HORARIO";
        $resultado = $this->mysqli->query($sentencia);

        return $resultado;
    }

    public function add(){
        $this->fecha = $this->convertirFecha($this->fecha);
        $sentencia = "INSERT INTO HORARIO(FECHA, HORA, DIA) VALUES ('$this->fecha', '$this->hora', '$this->dia')";
        if($this->mysqli->query($sentencia) === TRUE){
            return "Horario añadido correctamente";
        } else {
            return "Este horario ya está añadido " ;
        }
    }

    public function delete(){
        $sentencia = "DELETE FROM HORARIO WHERE (DIA='$this->dia' AND HORA='$this->hora')";
        if($this->mysqli->query($sentencia) === TRUE){
            return "Horario eliminado correctamente";
        } else {
            return "Error: " . $sql . "<br>" . $this->mysqli->error;
        }
    }

    private function convertirFecha($fecha){ 
        $date = str_replace('/', '-', $fecha); 
        return date('Y-m-d', strtotime($date)); 
    }
 
    private function matrixToArray($matrix){ 
        $toRet = array(); 
        foreach ($matrix as $value) { 
            array_push($toRet,$value[0]); 
        } 
 
        return $toRet; 
    } 
}

?>