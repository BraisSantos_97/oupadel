<?php

//declaración de la clase
class PROPONER_FECHA_MODEL{ 
//ID_FECHA_PROPUESTA	FECHA_PROPUESTA	HORA_PROPUESTA	ID_ENFRENTAMIENTO	NOMBRE_CAMPEONATO	NOMBRE_CATEGORIA	ID_GRUPO	NIVEL	ID_PAREJA
    
	var $id_enfrentamiento; // declaración del atributo login
    var $fecha_propuesta;
    var $hora_propuesta;
    var $id_pareja;
    var $id_fecha_propuesta;
    var $nombre_campeonato;
    var $nombre_categoria;
    var $id_grupo;
    var $nivel;
    
	var $mysqli; // declaración del atributo manejador de la bd
	
//FALTAN LOS OTROS PARAMETROS DE ENFRENTAMIENTO
    //Constructor de la clase
	function __construct($id_fecha_propuesta,$fecha_propuesta,$hora_propuesta,$id_enfrentamiento,$id_pareja,$nombre_campeonato,$nombre_categoria,$id_grupo,$nivel) {
		
		$this->id_enfrentamiento=$id_enfrentamiento;
        $this->fecha_propuesta=$fecha_propuesta;
        $this->id_fecha_propuesta=$id_fecha_propuesta;
        $this->hora_propuesta=$hora_propuesta;
        $this->id_pareja=$id_pareja;
        $this->nombre_campeonato=$nombre_campeonato;
        $this->nombre_categoria=$nombre_categoria;
        $this->id_grupo=$id_grupo;
        $this->nivel = $nivel;
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor
    

 function tuplasEnfrentamientos($grupo,$categoria,$campeonato,$nivel){

     $milogin=$_SESSION['login'];
    $sql="SELECT DISTINCT
                    pe.ID_ENFRENTAMIENTO,
                    pe.ID_GRUPO,
                    pe.NOMBRE_CATEGORIA,
                    pe.NOMBRE_CAMPEONATO,
                    pe.NIVEL,
                    pe.ID_PAREJA,
                    p.CAPITAN,
                    p.LOGIN_PAREJA
                    
                FROM pareja_enfrentamiento pe,pareja p,enfrentamiento e
                WHERE 
                    (  
                    e.ID_ENFRENTAMIENTO = pe.ID_ENFRENTAMIENTO AND   
                    pe.ID_PAREJA = p.ID_PAREJA AND p.CAPITAN ='$milogin' AND
                    pe.ID_GRUPO='$grupo' AND pe.NOMBRE_CATEGORIA='$categoria' AND
                    pe.NOMBRE_CAMPEONATO = '$campeonato' AND pe.NIVEL = '$nivel'
                    )";

                    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }
 }
    
    function EDIT($campeonato,$categoria,$grupo,$nivel){
    
        
                       
                                $date = $this->convertirFecha($this->fecha_propuesta);
                       
                                $sql ="SELECT ID_PISTA FROM horario_pista  where  FECHA='$date' AND HORA='$this->hora_propuesta'";

                                $resul = $this->mysqli->query( $sql );
                                $a=$resul->num_rows;
         
                         
						       $sql ="SELECT FECHA, HORA FROM horario  where HORA='$this->hora_propuesta' AND FECHA='$date'";
                                $result = $this->mysqli->query( $sql );
                              
                             
                              
                                if($result->num_rows != 0){
                                    
                                    $sql= "SELECT ID_PISTA FROM pista";
                                    
                                    $resultado = $this->mysqli->query( $sql );
                                    $b=$resultado->num_rows;
                                    
                                    
                                    $pista="";
                                    $cogidas =array();
                                    $totales= array();
                            
                                    
                                    if($a==$b){
                                          return "estan ocupadas todas las pistas";
                                    }
                                    
                                    
                                    
                                    if($a==0){
                                        $pistasTotales=mysqli_fetch_array($resultado);
                                        $pista=$pistasTotales[$a];
                                        
                                        
                                    }
                                    else{
                                        $i=0;
                                        $j=0;
                                        
                                        $iguales = array();
                                        
                                         while ( $fila = mysqli_fetch_array($resul) ) { 
                                                
                                                $cogidas[$i] = $fila['ID_PISTA'];
                                               
                                                $i++;
                                                
                                        }
                                        while ( $fila = mysqli_fetch_array($resultado) ) { 
                                           
                                                $totales[$j] = $fila['ID_PISTA'];
                                                $j++;
                                        }
                                  
                                      
                                      $pistasT = array_diff($totales, $cogidas);
                                      $indices = array();
                                        
                                      
                                    $salir=0;
                                    
                                    
                                while (current($pistasT)) {
                                    
                                    
                                   if($salir==0){
                                          $a =key($pistasT);
                                    
                                           $pista=$pistasT[$a];
                                        
                                        $salir=-1;
                                    }
                                      
                                    
                                    next($pistasT);
                                }        
                                    
                                      
                                   
                                    }
                                   
                               
                       
                            include_once '../Models/ENFRENTAMIENTOS_MODEL.php';//incluimos el modelo USU_GRUPO
							$ENFRENTAMIENTOS = new ENFRENTAMIENTOS_MODEL($this->id_enfrentamiento,$grupo,$categoria,$campeonato,$nivel,'',$this->fecha_propuesta,$this->hora_propuesta,$pista);//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
                                
							$m= $ENFRENTAMIENTOS->editar();//insertamos el login en el grupo alumnos*   
                             
                         

                        $milogin='admin';
                        
                        include_once '../Models/RESERVA_MODEL.php';//incluimos el modelo USU_GRUPO
                        
							$RESERVA = new Reserva('', '', '', '');//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
                            $RESERVA->setIdPista($pista);
                            $RESERVA->setHora($this->hora_propuesta);
                            $RESERVA->setFecha($date);
                            $RESERVA->setLogin($milogin);
                            
							$m=$RESERVA->add();//insertamos el login en el grupo alumnos*                  
                                        
                                                    
						return 'Insercion realizada con exito';
						
					
                  }
        else{
            return "no hay disponibles fechas y horas para pistas";
        }
    
    }
    
    
  function enfrentamientosAcordados(){
      $sql ="SELECT DISTINCT e.ID_ENFRENTAMIENTO,e.NOMBRE_CAMPEONATO, e.NOMBRE_CATEGORIA, e.NIVEL,e.ID_GRUPO,FECHA_ENFRENTAMIENTO,HORA_ENFRENTAMIENTO FROM enfrentamiento e, fecha_propuesta f where e.ID_ENFRENTAMIENTO=f.ID_ENFRENTAMIENTO AND e.NOMBRE_CAMPEONATO=f.NOMBRE_CAMPEONATO AND e.NOMBRE_CATEGORIA=f.NOMBRE_CATEGORIA AND e.ID_GRUPO=f.ID_GRUPO AND e.NIVEL=f.NIVEL AND  e.ID_ENFRENTAMIENTO='$this->id_enfrentamiento' AND e.NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND e.NOMBRE_CATEGORIA='$this->nombre_categoria' AND e.NIVEL='$this->nivel' AND e.ID_GRUPO='$this->id_grupo' AND FECHA_ENFRENTAMIENTO IS NOT NULL AND HORA_ENFRENTAMIENTO IS NOT NULL";
      
      if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }
      
      
  }
    
    
    function verPropuestas($grupo,$categoria,$campeonato,$nivel){
        $milogin=$_SESSION['login'];
        
        $sql = "SELECT DISTINCT e.ID_ENFRENTAMIENTO,e.NOMBRE_CAMPEONATO,e.NOMBRE_CATEGORIA,e.ID_GRUPO,e.NIVEL,FECHA_PROPUESTA,HORA_PROPUESTA,ID_PAREJA FROM fecha_propuesta f, enfrentamiento e where
        e.ID_ENFRENTAMIENTO=f.ID_ENFRENTAMIENTO AND e.NOMBRE_CAMPEONATO=f.NOMBRE_CAMPEONATO AND e.NOMBRE_CATEGORIA=f.NOMBRE_CATEGORIA AND e.NIVEL=f.NIVEL AND
        e.ID_ENFRENTAMIENTO='$this->id_enfrentamiento' AND e.NOMBRE_CAMPEONATO = '$campeonato' AND e.NOMBRE_CATEGORIA = '$categoria' AND e.ID_GRUPO='$grupo' AND e.NIVEL='$nivel' AND ID_PAREJA='$this->id_pareja' AND e.ID_PISTA IS NULL";
        
       
        
        
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }
            
        
    }
    

    function enfrentamientosRival($categoria,$campeonato,$nivel){
        
    $sql = "SELECT e.ID_ENFRENTAMIENTO,e.NOMBRE_CATEGORIA,e.NOMBRE_CAMPEONATO,e.NIVEL FROM pareja_enfrentamiento p, enfrentamiento e where e.ID_ENFRENTAMIENTO=p.ID_ENFRENTAMIENTO AND ID_PAREJA='$this->id_pareja' AND e.NOMBRE_CATEGORIA='$categoria' 
    AND e.NOMBRE_CAMPEONATO='$campeonato' AND e.NIVEL = '$nivel'AND RESULTADO IS NOT NULL;";
        
    if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }
}    
    
    function misEnfrentamientos($categoria,$campeonato,$nivel){
        
        $milogin =$_SESSION['login'];
        
        $sq = "SELECT ID_PAREJA FROM pareja where CAPITAN='$milogin' AND NOMBRE_CATEGORIA='$categoria' AND NOMBRE_CAMPEONATO='$campeonato' AND NIVEL = '$nivel'";
        
        $res = $this->mysqli->query( $sq );//hacemos la consulta en la base de datos
        $row = $res->fetch_array(MYSQLI_ASSOC);
        $pareja=$row['ID_PAREJA'];
        
        
        
         $sql = "SELECT e.ID_ENFRENTAMIENTO FROM pareja_enfrentamiento p, enfrentamiento e where e.ID_ENFRENTAMIENTO=p.ID_ENFRENTAMIENTO AND e.NOMBRE_CAMPEONATO=p.NOMBRE_CAMPEONATO AND e.NOMBRE_CATEGORIA=p.NOMBRE_CATEGORIA AND e.NIVEL=p.NIVEL AND ID_PAREJA='$pareja' AND RESULTADO IS NOT NULL; ";
   
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }
    }
    
    
        

function dosPartidos($rival,$yo){

$cont=0;
$i=0;
$j=0;    
$misE= array();
$rivalE = array();    
    
                    while($fil = mysqli_fetch_array( $yo ) ){
                            $misE[$i]= $fil['ID_ENFRENTAMIENTO'];
                            $i++;
                    }
    
                    while($fila = mysqli_fetch_array( $rival )){
                        $rivalE[$j] = $fila['ID_ENFRENTAMIENTO'];
                        $j++;
                    }
    
                for ($x = 0; $x<$i; $x++) {
                     for($y = 0; $y<$j; $y++){
                        
                        
                         
                        if($misE[$x] == $rivalE[$y]){
                            $cont++;
                        }
                         
                }
                 
                    }
					
               
                
    
    return $cont;
   
}



function ADD($campeonato){
    
    $sql ="SELECT FECHA_INICIO,FECHA_FINAL FROM campeonato where NOMBRE_CAMPEONATO='$campeonato'";
    
    
     $resultado = $this->mysqli->query( $sql );//hacemos la consulta en la base de datos
     $row = $resultado->fetch_array(MYSQLI_ASSOC);
    
                    $fecha_actual=date("d/m/Y");
                    $porciones = explode("/", $fecha_actual);
                    $dia_actual= intval($porciones[0]);
                    $mes_actual= intval($porciones[1]);
                    $ano_actual= intval($porciones[2]);
    
    
                    $porciones = explode("/", $row['FECHA_INICIO']);
                    $dia_comienzo= intval($porciones[0]);
                    $mes_comienzo= intval($porciones[1]);
                    $ano_comienzo= intval($porciones[2]);
                    
                    
    
                    $partes=explode("/", $row['FECHA_FINAL']);
                    $dia_fin= intval($partes[0]);
                    $mes_fin= intval($partes[1]);
                    $ano_fin= intval($partes[2]);
                    
                
                    
                    
                    $propuesto=explode("/", $this->fecha_propuesta);
                    $dia_propuesto= intval($propuesto[0]);
                    $mes_propuesto= intval($propuesto[1]);
                    $ano_propuesto= intval($propuesto[2]);
    
    
                    
    
                    
                  
                    if(($dia_fin >= $dia_propuesto && $mes_fin >= $mes_propuesto && $ano_fin >=$ano_propuesto) || ($mes_fin > $mes_propuesto && $ano_fin >=$ano_propuesto) || ($ano_fin >$ano_propuesto) ){
              
                        
                        if(($dia_propuesto >= $dia_comienzo && $mes_propuesto >= $mes_comienzo && $ano_propuesto >=$ano_comienzo) || ($mes_propuesto > $mes_comienzo && $ano_propuesto >=$ano_comienzo) || ($ano_propuesto >$ano_comienzo) ){
                            
                            if(($dia_propuesto >= $dia_actual && $mes_propuesto >= $mes_actual && $ano_propuesto >=$ano_actual) || ($mes_propuesto > $mes_actual && $ano_propuesto >=$ano_actual) || ($ano_propuesto >$ano_actual) ){
                
                            
							$sql = "INSERT INTO fecha_propuesta (
                                FECHA_PROPUESTA,
                                HORA_PROPUESTA,
                                ID_ENFRENTAMIENTO,
                                NOMBRE_CAMPEONATO,
                                NOMBRE_CATEGORIA,
                                ID_GRUPO,
                                NIVEL,
                                ID_PAREJA
                                 ) 
								VALUES(
                                '$this->fecha_propuesta',
                                '$this->hora_propuesta',
                                '$this->id_enfrentamiento',
                                '$this->nombre_campeonato',
                                '$this->nombre_categoria',
                                '$this->id_grupo',
                                '$this->nivel',
                                '$this->id_pareja'
								)";
							
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
				            return "Inserción realizada con exito";
						
					}
                          }
                            else{
                                return "No puedes insertar una fecha inferior al dia actual";
                            }
                            
                            
                        }
                        else{
                             return "No se corresponde esta fecha con el calendario del campeonato";
                        }
                
                  }
            
                else{
                    return "No se corresponde esta fecha con el calendario del campeonato";
                }

}

    private function convertirFecha($fecha){ 
        $date = str_replace('/', '-', $fecha); 
        return date('Y-m-d', strtotime($date)); 
    }

} //fin de clase

?>