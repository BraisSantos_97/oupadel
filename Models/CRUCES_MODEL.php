<?php
//Acceso a BD
include_once '../Functions/BdAdmin.php';

class CRUCE{
    var $id_grupo;
    var $nombre_campeonato;
    var $nombre_categoria;
    var $nivel;
    var $id_partido;
    var $id_pareja_1;
    var $id_pareja_2;
    var $resultado;
    var $ganador;
    var $fase;
    var $fecha;
    var $hora;

    public function __construct($id_grupo=NULL,$nombre_campeonato=NULL,$nombre_categoria=NULL,$nivel=NULL,$id_partido=NULL,$id_pareja_1=NULL,$id_pareja_2=NULL,$resultado=NULL,$ganador=NULL,$fase=NULL,$fecha=NULL,$hora=NULL){
		//asignación de valores de parámetro a los atributos de la clase
        $this->id_grupo = $id_grupo;
        $this->nombre_campeonato = $nombre_campeonato;
        $this->nombre_categoria = $nombre_categoria;
        $this->nivel = $nivel;
        $this->id_partido = $id_partido;
        $this->id_pareja_1 = $id_pareja_1;
        $this->id_pareja_2 = $id_pareja_2;
        $this->resultado = $resultado;
        $this->ganador = $ganador;
        $this->fase = $fase;
        $this->fecha = $fecha;
        $this->hora = $hora;

        // conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");
    }


    public function getAllCruces(){ 
        $sentencia = "SELECT ID_CONTENIDO FROM CRUCES"; 
        $resultado = $this->mysqli->query($sentencia); 
        $toRet = array();
        if($resultado != false){ 
            $matrix = $resultado->fetch_all(); 
            $toRet = $this->matrixToArray($matrix); 
        }else{ 
            $toRet = NULL; 
        } 
 
        return $toRet; 
    }

    public function findAllByGrupo($id_grupo,$nombre_categoria,$nombre_campeonato,$nivel){
        $sentencia = "SELECT * FROM cruces WHERE ID_GRUPO=$id_grupo AND NOMBRE_CATEGORIA='$nombre_categoria' AND NOMBRE_CAMPEONATO='$nombre_campeonato' AND NIVEL=$nivel"; 
        $resultado = $this->mysqli->query($sentencia);
        return $resultado;
    }
    
    public function findAllByGrupo_2($id_grupo,$nombre_categoria,$nombre_campeonato,$nivel){
        $sentencia = "SELECT * FROM cruces WHERE ID_GRUPO=$id_grupo AND NOMBRE_CATEGORIA='$nombre_categoria' AND NOMBRE_CAMPEONATO='$nombre_campeonato' AND NIVEL='$nivel' AND FECHA IS NULL AND HORA IS NULL" ; 
        $resultado = $this->mysqli->query($sentencia);
        return $resultado;
    }

    public function findById($id_grupo,$nombre_categoria,$nombre_campeonato,$nivel,$id_partido){
        $sentencia = "SELECT * FROM cruces WHERE ID_GRUPO=$id_grupo AND NOMBRE_CATEGORIA=$nombre_categoria AND NOMBRE_CAMPEONATO=$nombre_campeonato AND NIVEL=$nivel AND ID_PARTIDO=$id_partido";
        $resultado = $this->mysqli->query($sentencia);
        $pista = $resultado->fetch_assoc();

        if($pista != NULL){
            return new CRUCE($cruce["ID_GRUPO"],$cruce["NOMBRE_CATEGORIA"],$cruce["NOMBRE_CAMPEONATO"],$cruce["NIVEL"],$cruce["ID_PARTIDO"],$cruce["ID_PAREJA_1"],$cruce["ID_PAREJA_2"],$cruce["RESULTADO"],$cruce["GANADOR"],$cruce["FASE"],$cruce["FECHA"],$cruce["HORA"]);
        } else {
            return NULL;
        }
    }

    public function add(){
        $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA)
         VALUES ('$this->id_grupo', '$this->nombre_categoria', '$this->nombre_campeonato', '$this->nivel', '$this->id_partido', '$this->id_pareja_1', '$this->id_pareja_2', '$this->resultado', '$this->ganador', '$this->fase', '$this->fecha', '$this->hora')";
        if($this->mysqli->query($sentencia) == TRUE){
            return "Partido de cruce añadido correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    public function update(){
        $sentencia = "UPDATE cruces SET ID_PAREJA_1=$cruce->getId_pareja_1(), ID_PAREJA_2=$cruce->getId_pareja_2(), RESULTADO=$cruce->getResultado(), GANADOR=$cruce->getGanador() WHERE ID_GRUPO=$cruce->getId_grupo() AND NOMBRE_CATEGORIA=$cruce->getNombre_categoria() AND NOMBRE_CAMPEONATO=$cruce->getNombre_campeonato() AND NIVEL=$cruce->getNivel() AND ID_PARTIDO=$cruce->getId_partido()";
        if($this->mysqli->query($sentencia) === TRUE){
            return "Ganador actualizado correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    private function matrixToArray($matrix){ 
        $toRet = array(); 
        foreach ($matrix as $value) { 
            array_push($toRet,$value[0]); 
        } 
 
        return $toRet; 
    }

    public function generarCruces($grupo){
        $sentencia = "SELECT  
            ID_PAREJA, CAPITAN, LOGIN_PAREJA FROM pareja WHERE 
            (
            NOMBRE_CATEGORIA = '$this->nombre_categoria' AND
            NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND
            ID_GRUPO = '$grupo' AND
            NIVEL ='$this->nivel'
            )
            ORDER BY PUNTOS DESC LIMIT 8";

        $parejas = $this->mysqli->query($sentencia);
        
        $arrayparejas = array();
        foreach($parejas as $pareja){
            array_push($arrayparejas,$pareja['ID_PAREJA']);
        }

        if(sizeof($arrayparejas) == 0){
            return "Para generar los cruces es necesario que todos los grupos tengan parejas.";
        } else {
            $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA) VALUES('$grupo','$this->nombre_campeonato','$this->nombre_categoria','$this->nivel', 0, '$arrayparejas[0]', '$arrayparejas[7]',NULL,NULL,'CUARTOS',NULL,NULL)";
            $result = $this->mysqli->query($sentencia);
            
            $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA) VALUES('$grupo','$this->nombre_campeonato','$this->nombre_categoria','$this->nivel', 1, '$arrayparejas[1]', '$arrayparejas[6]',NULL,NULL,'CUARTOS',NULL,NULL)";
            $result = $this->mysqli->query($sentencia);
            
            $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA) VALUES('$grupo','$this->nombre_campeonato','$this->nombre_categoria','$this->nivel', 2, '$arrayparejas[2]', '$arrayparejas[5]',NULL,NULL,'CUARTOS',NULL,NULL)";
            $result = $this->mysqli->query($sentencia);
            
            $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA) VALUES('$grupo','$this->nombre_campeonato','$this->nombre_categoria','$this->nivel', 3, '$arrayparejas[3]', '$arrayparejas[4]',NULL,NULL,'CUARTOS',NULL,NULL)";
            $result = $this->mysqli->query($sentencia);
            
            $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA) VALUES('$grupo','$this->nombre_campeonato','$this->nombre_categoria','$this->nivel', 4,NULL,NULL,NULL,NULL,'SEMIFINALES',NULL,NULL)";
            $result = $this->mysqli->query($sentencia);
            
            $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA) VALUES('$grupo','$this->nombre_campeonato','$this->nombre_categoria','$this->nivel', 5,NULL,NULL,NULL,NULL,'SEMIFINALES',NULL,NULL)";
            $result = $this->mysqli->query($sentencia);
            
            $sentencia = "INSERT INTO cruces(ID_GRUPO, NOMBRE_CAMPEONATO, NOMBRE_CATEGORIA, NIVEL, ID_PARTIDO, ID_PAREJA_1, ID_PAREJA_2, RESULTADO, GANADOR, FASE, FECHA, HORA) VALUES('$grupo','$this->nombre_campeonato','$this->nombre_categoria','$this->nivel', 6,NULL,NULL,NULL,NULL,'FINAL',NULL,NULL)";
            $result = $this->mysqli->query($sentencia);
            return "Cruces generados";
        }
    }

    
    function actualizarfecha(){
        
        
        
					  $date = $this->convertirFecha($this->fecha);
                       
                       
                       
                      $sql ="SELECT ID_PISTA FROM horario_pista  where  FECHA='$date' AND HORA='$this->hora'";

                      $resul = $this->mysqli->query( $sql );
                      $a=$resul->num_rows;
         
                         
				      $sql ="SELECT FECHA, HORA FROM horario  where HORA='$this->hora' AND FECHA='$date'";
                      $result = $this->mysqli->query( $sql );
                              
                             
                              
                      if($result->num_rows != 0){
                                    
                           $sql= "SELECT ID_PISTA FROM pista";
                                    
                           $resultado = $this->mysqli->query( $sql );
                           $b=$resultado->num_rows;
                           
                                    
                           $pista="";
                           $cogidas =array();
                           $totales= array();
                            
                          
                                    
                          if($a==$b){
                              
                              return "estan ocupadas todas las pistas";
                              
                          }
                          
                                    
                              if($a==0){
                                  $pistasTotales=mysqli_fetch_array($resultado);
                                  $pista=$pistasTotales[$a];
                                       
                                  }
                                 else{
                                     $i=0;
                                     $j=0;
                                        
                                         while ( $fila = mysqli_fetch_array($resul) ) { 
                                                
                                                $cogidas[$i] = $fila['ID_PISTA'];
                                                $i++;
                                        }
                                        while ( $fila = mysqli_fetch_array($resultado) ) { 
                                                $totales[$j] = $fila['ID_PISTA'];
                                                $j++;
                                        }
                                        
                                     
                                      $pistasT = array_diff($totales, $cogidas);
                                      $indices = array();
                                        
                                      
                                    $salir=0;
                                    
                                    
                                while (current($pistasT)) {
                                    
                                    
                                    if($salir==0){
                                          $a =key($pistasT);
                                    
                                           $pista=$pistasT[$a];
                                        $salir=-1;
                                    }
                                      
                                    
                                    next($pistasT);
                                }  
                                   
                                    
                            }
                                    
                             
                        
                            $milogin='admin';
                        
                            include_once '../Models/RESERVA_MODEL.php';
                        
							$RESERVA = new Reserva('', '', '', '');//instanciamos un objeto del modelo USU_GRUPO donde metemos un  usuario en el grupo alumnos
                            $RESERVA->setIdPista($pista);
                            $RESERVA->setHora($this->hora);
                            $RESERVA->setFecha($date);
                            $RESERVA->setLogin($milogin);
                            
							$RESERVA->add();//insertamos el login en el grupo alumnos* 
                          
                          
                            $sql = "UPDATE cruces SET 
                                FECHA = '$this->fecha',
                                HORA = '$this->hora',
                                PISTA = '$pista'
                                WHERE (ID_PARTIDO= '$this->id_partido' AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";

                        $this->mysqli->query($sql);
                          
                          
                          
                           return "Inserción fecha y hora con éxito";
                        
				  }
        
            else{
                    return "No hay pista para esa fecha y hora";
            }
        
        
    
      
        
    }
    
    
	private function convertirFecha($fecha){ 
        
        $date = str_replace('/', '-', $fecha); 
        return date('Y-m-d', strtotime($date)); 
        
    }
    
    function ganador(){

        $ganador="";
       
        $sql = "UPDATE cruces SET 
                RESULTADO= '$this->resultado'
                WHERE (ID_PARTIDO= '$this->id_partido' AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
            // si hay un problema con la query se envia un mensaje de error en la modificacion
            if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
                return 'Error en la modificación';
            } else { // si no hay problemas con la modificación se indica que se ha modificado
             
                if (strlen($this->resultado)<8) {
                    $porciones = explode("/", $this->resultado);
                    $set1 = explode("-",$porciones[0]);
                    $set2 = explode("-", $porciones[1]);


                    if((intval($set1[0]) > intval($set1[1])) && ( intval($set2[0]) >  intval($set2[1]))) {
                        $ganador=$this->id_pareja_1;
                    }else{
                        if ((intval($set1[0]) < intval($set1[1])) && ( intval($set2[0]) < intval($set2[1]))) {
                            $ganador=$this->id_pareja_2;
                        }
                    }
                }else{
                    if (strlen($this->resultado)>=8) {
                        $porciones = explode("/", $this->resultado);
                        $set1 = explode("-", $porciones[0]);
                        $set2 = explode("-", $porciones[1]);
                        $set3 = explode("-", $porciones[2]);
                       
                       
                    
                       
                        if ((intval($set1[0]) > intval($set1[1])) && ( intval($set2[0]) <  intval($set2[1])) && (intval($set3[0]) > intval($set3[1]))) {
                            $ganador=$this->id_pareja_1;
                        }else{
                            if ((intval($set1[0]) < intval($set1[1])) && ( intval($set2[0]) >  intval($set2[1])) && (intval($set3[0]) < intval($set3[1]))) {
                                $ganador=$this->id_pareja_2;
                            }
                            else{
                                if((intval($set1[0]) < intval($set1[1])) && ( intval($set2[0]) >  intval($set2[1])) && (intval($set3[0]) > intval($set3[1]))){
                                    $ganador=$this->id_pareja_1;
                                }
                                else{
                                    if((intval($set1[0]) > intval($set1[1])) && ( intval($set2[0]) <  intval($set2[1])) && (intval($set3[0]) < intval($set3[1]))){
                                        $ganador=$this->id_pareja_2;
                                    }
                                }
                            }
                        }
                    }
                }
                $sql = "UPDATE cruces SET 
                GANADOR= '$ganador'
                WHERE ( ID_PARTIDO= '$this->id_partido' AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel'
                )";
                // si hay un problema con la query se envia un mensaje de error en la modificacion
                if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
                    return 'Error en la modificación';
                }

                if($this->fase == "CUARTOS"){
                    $sql = "SELECT ID_PAREJA_1,ID_PAREJA_2 
                    FROM cruces
                    WHERE FASE='SEMIFINALES' AND ID_PARTIDO=4 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel'";

                    if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
                        return 'Error en la modificación';
                    } else {
                        $resultado=$resultado->fetch_assoc();
                        
                        if($resultado['ID_PAREJA_1'] == NULL){
                            $sql = "UPDATE cruces SET 
                            ID_PAREJA_1= '$ganador'
                            WHERE (ID_PARTIDO= 4 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
                    
                            if (!($resultado = $this->mysqli->query($sql))){
                                return 'Error en la modificación';
                            }
                        } else if($resultado['ID_PAREJA_2'] == NULL){
                            $sql = "UPDATE cruces SET 
                            ID_PAREJA_2= '$ganador'
                            WHERE (ID_PARTIDO= 4 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
                            
                            if (!($resultado = $this->mysqli->query($sql))){
                                return 'Error en la modificación';
                            }
                        } else {
                            $sql = "SELECT ID_PAREJA_1,ID_PAREJA_2 
                            FROM cruces
                            WHERE FASE='SEMIFINALES' AND ID_PARTIDO=5  AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel'";

                            if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
                                return 'Error en la modificación';
                            } else {
                                $resultado=$resultado->fetch_assoc();
                        
                                if($resultado['ID_PAREJA_1'] == NULL){
                                    $sql = "UPDATE cruces SET 
                                    ID_PAREJA_1= '$ganador'
                                    WHERE (ID_PARTIDO= 5 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
                                    if (!($resultado = $this->mysqli->query($sql))){
                                        return 'Error en la modificación';
                                    }
                                } else if($resultado['ID_PAREJA_2'] == NULL){
                                    $sql = "UPDATE cruces SET 
                                    ID_PAREJA_2= '$ganador'
                                    WHERE (ID_PARTIDO= 5 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
                                    if (!($resultado = $this->mysqli->query($sql))){
                                        return 'Error en la modificación';
                                    }
                                }
                            }
                        }
                    }
                }

                if($this->fase == "SEMIFINALES"){
                    $sql = "SELECT ID_PAREJA_1,ID_PAREJA_2 
                    FROM cruces
                    WHERE FASE='FINAL' AND ID_PARTIDO=6  AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel'";

                    if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
                        return 'Error en la modificación';
                    } else {
                        $resultado=$resultado->fetch_assoc();
                        
                        if($resultado['ID_PAREJA_1'] == NULL){
                            $sql = "UPDATE cruces SET 
                            ID_PAREJA_1= '$ganador'
                            WHERE (ID_PARTIDO= 6 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
                            if (!($resultado = $this->mysqli->query($sql))){
                                return 'Error en la modificación';
                            }
                        } else if($resultado['ID_PAREJA_2'] == NULL){
                            $sql = "UPDATE cruces SET 
                            ID_PAREJA_2= '$ganador'
                            WHERE (ID_PARTIDO= 6 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
                            if (!($resultado = $this->mysqli->query($sql))){
                                return 'Error en la modificación';
                            }
                        }
                    }
                }

                if($this->fase == "FINAL"){
                    /*$sql = "SELECT GANADOR 
                    FROM cruces
                    WHERE FASE='FINAL' AND ID_PARTIDO=6";

                    if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
                        return 'Error en la modificación';
                    } else {*/
                        $sql = "UPDATE cruces SET 
                            GANADOR= '$ganador'
                            WHERE (ID_PARTIDO= 6 AND NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND NOMBRE_CATEGORIA='$this->nombre_categoria' AND ID_GRUPO='$this->id_grupo' AND NIVEL='$this->nivel')";
                            if (!($resultado = $this->mysqli->query($sql))){
                                return 'Error en la modificación';
                            }
                    //}
                }



               return 'Modificado correctamente';
            }
    }

    /**
     * Get the value of id_grupo
     */ 
    public function getId_grupo()
    {
        return $this->id_grupo;
    }

    /**
     * Set the value of id_grupo
     *
     * @return  self
     */ 
    public function setId_grupo($id_grupo)
    {
        $this->id_grupo = $id_grupo;

        return $this;
    }

    /**
     * Get the value of nombre_campeonato
     */ 
    public function getNombre_campeonato()
    {
        return $this->nombre_campeonato;
    }

    /**
     * Set the value of nombre_campeonato
     *
     * @return  self
     */ 
    public function setNombre_campeonato($nombre_campeonato)
    {
        $this->nombre_campeonato = $nombre_campeonato;

        return $this;
    }

    /**
     * Get the value of nombre_categoria
     */ 
    public function getNombre_categoria()
    {
        return $this->nombre_categoria;
    }

    /**
     * Set the value of nombre_categoria
     *
     * @return  self
     */ 
    public function setNombre_categoria($nombre_categoria)
    {
        $this->nombre_categoria = $nombre_categoria;

        return $this;
    }

    /**
     * Get the value of nivel
     */ 
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set the value of nivel
     *
     * @return  self
     */ 
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get the value of id_partido
     */ 
    public function getId_partido()
    {
        return $this->id_partido;
    }

    /**
     * Set the value of id_partido
     *
     * @return  self
     */ 
    public function setId_partido($id_partido)
    {
        $this->id_partido = $id_partido;

        return $this;
    }

    /**
     * Get the value of id_pareja_1
     */ 
    public function getId_pareja_1()
    {
        return $this->id_pareja_1;
    }

    /**
     * Set the value of id_pareja_1
     *
     * @return  self
     */ 
    public function setId_pareja_1($id_pareja_1)
    {
        $this->id_pareja_1 = $id_pareja_1;

        return $this;
    }

    /**
     * Get the value of id_pareja_2
     */ 
    public function getId_pareja_2()
    {
        return $this->id_pareja_2;
    }

    /**
     * Set the value of id_pareja_2
     *
     * @return  self
     */ 
    public function setId_pareja_2($id_pareja_2)
    {
        $this->id_pareja_2 = $id_pareja_2;

        return $this;
    }

    /**
     * Get the value of resultado
     */ 
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Set the value of resultado
     *
     * @return  self
     */ 
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Get the value of ganador
     */ 
    public function getGanador()
    {
        return $this->ganador;
    }

    /**
     * Set the value of ganador
     *
     * @return  self
     */ 
    public function setGanador($ganador)
    {
        $this->ganador = $ganador;

        return $this;
    }

    /**
     * Get the value of fase
     */ 
    public function getFase()
    {
        return $this->fase;
    }

    /**
     * Set the value of fase
     *
     * @return  self
     */ 
    public function setFase($fase)
    {
        $this->fase = $fase;

        return $this;
    }

    /**
     * Get the value of fecha
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get the value of hora
     */ 
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set the value of hora
     *
     * @return  self
     */ 
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }
}

?>