<?php

//declaración de la clase
class DEPORTISTA_PAREJA_MODEL{ 

	var $id_pareja; // declaración del atributo login
    var $login;
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($login,$id_pareja) {
		
		$this->id_pareja = $id_pareja;//declaracion de la variable que almacena login
        $this->login =$login;
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
		$this->mysqli = ConectarBD();
		$this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor


function ADD() {

		if ( ( $this->id_pareja <> ''  && $this->login <> '') ) { // si el atributo clave de la entidad no esta vacio
            
			// construimos el sql para buscar esa clave en la tabla
			$sql = "SELECT * FROM deportista_pareja WHERE (  LOGIN = '$this->login' AND ID_PAREJA ='$this->id_pareja')";

			if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows != 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						return 'Ya existe esa  categoria en la base de datos';// ya existe
						
					} else {

							$sql = "INSERT INTO deportista_pareja (
                                LOGIN,
                                ID_PAREJA
                                 ) 
								VALUES(
                                '$this->login',
                                '$this->id_pareja'
								)";

					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						return 'Insercion realizada con exito';
						
					}

				}
			
		} 
	} else { // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
			return 'Introduzca un valor'; // introduzca un valor para el usuario
		}

    }
	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct



} //fin de clase

?>