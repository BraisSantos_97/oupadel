
<?php

//declaración de la clase
class GRUPOS_MODEL{ 

	var $id_grupo; // declaración del atributo login
    var $nombre_categoria;
    var $nombre_campeonato;
    var $nivel;
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($id_grupo,$nombre_categoria,$nombre_campeonato,$nivel) {
		
		$this->nombre_categoria = $nombre_categoria;//declaracion de la variable que almacena login
        $this->id_grupo=$id_grupo;//declaracion de la variable que almacena password
		$this->nombre_campeonato = $nombre_campeonato;//declaracion de la variable que almacena nombre
		$this->nivel = $nivel;
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
		$this->mysqli = ConectarBD();
		$this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor

	//funcion SEARCH: hace una búsqueda en la tabla con
	//los datos proporcionados. Si van vacios devuelve todos
	function SEARCH() {
        
		// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
		$sql = "select  
                    ID_GRUPO,
					NOMBRE_CATEGORIA,
					NOMBRE_CAMPEONATO,
                    NIVEL
       			from grupo
    			where 
    				(
                    (BINARY NOMBRE_CATEGORIA = '$this->nombre_categoria') &&
	 				(BINARY NOMBRE_CAMPEONATO = '$this->nombre_campeonato') &&
                    (BINARY NIVEL = '$this->nivel')
    				)";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
	} // fin metodo SEARCH


    function ver_grupos(){
           $milogin =$_SESSION['login'];
    
        $sql = "SELECT g.ID_GRUPO,g.NOMBRE_CATEGORIA,g.NOMBRE_CAMPEONATO,g.NIVEL FROM grupo g, pareja p where g.ID_GRUPO = p.ID_GRUPO 
        AND g.NOMBRE_CATEGORIA=p.NOMBRE_CATEGORIA_GRUPO AND g.NOMBRE_CAMPEONATO=p.NOMBRE_CAMPEONATO_GRUPO AND CAPITAN='$milogin'
        AND g.NOMBRE_CAMPEONATO='$this->nombre_campeonato' AND g.NOMBRE_CATEGORIA='$this->nombre_categoria' AND g.ID_GRUPO='$this->id_grupo' AND g.NIVEL ='$this->nivel'";

         // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado

            return $resultado;
        }

    }
    
    function siCapitan(){
        
        $milogin=$_SESSION['login'];
        
        $sql = "SELECT * FROM pareja WHERE ( NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA ='$this->nombre_categoria' AND NIVEL = '$this->nivel' AND CAPITAN='$milogin')";
        
        if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows != 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						 
                        
                        return true ;// ya existe
						
					}
            else{
                 
                return false;
            }
        
    }
    }
    
    
	//Metodo ADD()
	//Inserta en la tabla  de la bd  los valores
	// de los atributos del objeto. Comprueba si la clave/s esta vacia y si 
	//existe ya en la tabla
	function ADD() {
		if ( ( $this->nombre_campeonato <> ''  && $this->nombre_categoria <> '' && $this->nivel <> '') ) { // si el atributo clave de la entidad no esta vacio
            
			// construimos el sql para buscar esa clave en la tabla
			$sql = "SELECT * FROM grupo WHERE ( ID_GRUPO = '$this->id_grupo' AND NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA ='$this->nombre_categoria' AND NIVEL='$this->nivel')";

			if ( !$result = $this->mysqli->query( $sql ) ) { // si da error la ejecución de la query
				return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
			} else { // si la ejecución de la query no da error
					if ( $result->num_rows != 0 ) {// miramos si el resultado de la consulta no es vacio ( existe el dni)
						// si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
						return 'Ya existe en la base de datos';// ya existe
						//ID_GRUPO		NOMBRE_CATEGORIA	NOMBRE_CAMPEONATO
					} else {

							$sql = "INSERT INTO grupo (
                                ID_GRUPO,
                                NOMBRE_CATEGORIA,
                                NOMBRE_CAMPEONATO,
                                NIVEL
                                 ) 
								VALUES(
                                '$this->id_grupo',
                                '$this->nombre_categoria',
                                '$this->nombre_campeonato',
                                '$this->nivel'
								)";

				
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						/*if($mensaje == 'Inserción realizada con éxito'){//miramos si la inserción en USU_GRUPO tuvo exito
							return 'Inserción realizada con éxito'; //operacion de insertado correcta
						}else{//si la insercion no tuvo exito
							return $mensaje;
						}*/
						return 'Insercion realizada con exito';
						
					}

				}
			
		} 
	} else { // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
			return 'Introduzca un valor'; // introduzca un valor para el usuario
		}

    }
	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct

	// funcion DELETE()
	// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
	// se manda un mensaje de que ese valor de clave no existe
	function DELETE() {
		// se construye la sentencia sql de busqueda con los atributos de la clase
		$sql = "SELECT * FROM grupo WHERE (ID_GRUPO = '$this->id_grupo' AND NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA ='$this->nombre_categoria' AND NIVEL ='$this->nivel' )";
		
		$result = $this->mysqli->query( $sql );
		

		if ( $result->num_rows == 1 ) {// si existe una tupla con ese valor de clave
			// se construye la sentencia sql de borrado
			$sql = "DELETE FROM grupo WHERE (ID_GRUPO = '$this->id_grupo' AND NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA ='$this->nombre_categoria' AND NIVEL='$this->nivel' )";
			// se ejecuta la query
            
			$this->mysqli->query( $sql );
            
			// se devuelve el mensaje de borrado correcto
			return "Borrado correctamente";
		} // si no existe el login a borrar se devuelve el mensaje de que no existe
		else{
            return "No existe";
        }
			
	} // fin metodo DELETE
    
    
    function devolverFechaLimite($campeonato){
        $sql ="SELECT FECHA_LIMITE_INSCRIPCION FROM campeonato where NOMBRE_CAMPEONATO ='$campeonato' ";
         $resultado = $this->mysqli->query( $sql );//hacemos la consulta en la base de datos
         $row = $resultado->fetch_array(MYSQLI_ASSOC);
        
         return $row['FECHA_LIMITE_INSCRIPCION'];
        
    }

	// funcion RellenaDatos()
	// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
	// en el atributo de la clase
	function RellenaDatos($id_grupo,$categoria,$campeonato,$nivel) { 

		$sql = "SELECT * FROM grupo WHERE (NOMBRE_CAMPEONATO = '$campeonato' AND NOMBRE_CATEGORIA ='$categoria'  AND ID_GRUPO ='$id_grupo' AND NIVEL = '$nivel')";// se construye la sentencia de busqueda de la tupla
		// Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; // 
		} else { // si existe se devuelve la tupla resultado
            //Aplicamos fetch_array sobre $resultado para crear un array y se guarda en $result
			$result = $resultado->fetch_array();
			return $result;
		}
        
	}   // fin del metodo RellenaDatos()

	public function findAllByCategoria(){
        $sentencia = "SELECT * FROM grupo WHERE (NOMBRE_CAMPEONATO = '$this->nombre_campeonato' AND NOMBRE_CATEGORIA ='$this->nombre_categoria' AND NIVEL='$this->nivel')";
		$resultado = $this->mysqli->query($sentencia);

        return $resultado;
    }
    
	


} //fin de clase


?>