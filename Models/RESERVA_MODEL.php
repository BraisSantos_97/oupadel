<?php
//Acceso a BD
include_once '../Functions/BdAdmin.php';

class Reserva{
    var $id_pista;
    var $hora;
    var $fecha;
    var $login;

    public function __construct($id_pista=NULL, $fecha=NULL, $hora=NULL, $login=NULL){
        $this->id_pista=$id_pista;
        $this->hora=$hora;
        $this->fecha=$fecha;
        $this->login=$login;
        // conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    /**
     * Get the value of id_pista
     */ 
    public function getIdPista()
    {
        return $this->id_pista;
    }

    /**
     * Set the value of id_pista
     *
     * @return  self
     */ 
    public function setIdPista($id_pista)
    {
        $this->id_pista = $id_pista;

        return $this;
    }

    /**
     * Get the value of hora
     */ 
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set the value of hora
     *
     * @return  self
     */ 
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get the value of fecha
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get the value of login
     */ 
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of login
     *
     * @return  self
     */ 
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    public function findReserva($id_pista,$fecha,$hora){
        $sentencia = "SELECT * FROM HORARIO_PISTA WHERE ID_PISTA='$id_pista' AND FECHA='$fecha' AND hora='$hora'";
        $resultado = $this->mysqli->query($sentencia);
        $reserva = $resultado->fetch_assoc();

        if($reserva != NULL){
            return new Reserva($reserva["ID_PISTA"],$reserva["FECHA"],$reserva["HORA"],$reserva["LOGIN"]);
        } else {
            return false;
        }
    }

    public function findByFecha($fecha){
        $toRet="";
        $sentencia = "SELECT ID_PISTA,HORA FROM HORARIO_PISTA WHERE FECHA='".$fecha."' ORDER BY ID_PISTA";
        $resultado = $this->mysqli->query($sentencia);
        $tabla = array();
        if($resultado != false){
            if($resultado->num_rows > 0){
                $tabla = $resultado->fetch_all();
                $toRet = $this->matrixToAssoc($tabla);
            }
        }
        
        return $toRet;
    }

    public function findByLogin($login){
        $sentencia = "SELECT * FROM HORARIO_PISTA WHERE LOGIN='$login'";
        $resultado = $this->mysqli->query($sentencia);

        return $resultado;
    }

    public function findAll(){
        $sentencia = "SELECT * FROM HORARIO_PISTA";
        $resultado = $this->mysqli->query($sentencia);

        return $resultado;
    }

    public function add(){
        $this->borrarAntiguas($this->login);
        $sentencia = "INSERT INTO HORARIO_PISTA(ID_PISTA, FECHA, HORA, LOGIN) VALUES ('$this->id_pista', '$this->fecha', '$this->hora', '$this->login')";
        $comprobacion = "SELECT * FROM HORARIO_PISTA WHERE ID_PISTA='$this->id_pista' AND FECHA='$this->fecha' AND hora='$this->hora'";
        $esVacio = $this->mysqli->query($comprobacion);
        if($esVacio->num_rows != 0){
            return "Ya hay una reserva para ese dia, esa hora y esa pista.";
        }

      
        
        $numReservas = $this->getNumReservas($this->login);
        if($numReservas > 4 && $this->login != "admin"){
            return "No puede haber más de 5 reservas por usuario a la vez";
        }

        $result = $this->mysqli->query($sentencia);
        if($result != FALSE){
            $horaCompleta = $this->checkHoraCompleta($this->fecha,$this->hora);
            if($horaCompleta == true){
                $fechaAux = $this->convertirFecha($this->fecha);
                $sentencia2 = "SELECT ID_PARTIDO FROM PARTIDO WHERE FECHA_PARTIDO='".$fechaAux."' AND HORA_PARTIDO='".$this->hora."'";
                $result2 = $this->mysqli->query($sentencia2);
                $idPartidos = $result2->fetch_array();
                if($idPartidos != null){
                    foreach($idPartidos as $idPartido){
                        $sentencia2 = "DELETE FROM PARTIDO WHERE (ID_PARTIDO='$idPartido')";
                        $this->mysqli->query($sentencia2);
                    }
                }
            }
            return "Reserva añadida correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    public function delete(){
        $sentencia = "DELETE FROM HORARIO_PISTA WHERE (ID_PISTA='$this->id_pista' AND FECHA='$this->fecha' AND HORA='$this->hora')";
        if($this->mysqli->query($sentencia) == TRUE){
            return "Reserva borrada correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    public function comprobarReserva($id_pista,$fecha,$hora){
        $sentencia = "SELECT * FROM HORARIO_PISTA WHERE ID_PISTA='$id_pista' AND FECHA='$fecha' AND hora='$hora'";
        $resultado = $this->mysqli->query($sentencia);
        $reserva = $resultado->fetch_assoc();

        if($reserva != NULL){
            return true;
        } else {
            return false;
        }
    }

    //Esta función comprueba si la fecha y hora que se le pasan son anteriores a la actual, en caso afirmativo devuelve true, en caso contrario false
    public function esPasada($fecha,$hora){
        //$hoy=localtime();
        $hoyFecha=date("Y-m-d");
        $hoyHora=date("H:i");
    
     
        if($fecha < $hoyFecha){
            return true;
        } else {  
            if($fecha == $hoyFecha){
                if($hora > $hoyHora){            
                    return false;
                } else{
                    return true;
                }
            }
            
            if($fecha > $hoyFecha){
                return false;
            }

            return true;
        }
    }

    public function menosDeDoceHoras($fecha,$hora){
        $hoy=localtime();

        $hoy=$this->arrayToDate($hoy);
        $datetime = new DateTime($fecha." ".$hora);
        
        $toAdd = new DateInterval('PT12H');

        $hoy->add($toAdd);

        if($datetime <= $hoy){
            return true;
        } else {
            return false;
        }
    }

    private function arrayToDate($datearray){
        foreach($datearray as $value){
            $time = new DateTime($value['date']);
            $date = $time->format('Y-m-d');
            $time = $time->format('H:i');

            $toRet = new DateTime($date." ".$time);
            return $toRet;
        }
    }

    private function getNumReservas($login){
        $sentencia="SELECT * FROM HORARIO_PISTA WHERE LOGIN='$login'";
        $result = $this->mysqli->query($sentencia);
        $numRows = $result->num_rows;

        return $numRows;
    }

    private function borrarAntiguas($login){
        $sentencia="SELECT * FROM HORARIO_PISTA WHERE LOGIN='$login'";
        $result = $this->mysqli->query($sentencia);
        $numRows = $result->num_rows;

        for($i=0;$i<$numRows;$i++){
            $sentencia2="SELECT ID_PISTA,FECHA, HORA FROM HORARIO_PISTA WHERE LOGIN='$login'";
            $result2 = $this->mysqli->query($sentencia2);
            $reserva = $result2->fetch_assoc();
            if($this->esPasada($reserva['FECHA'],$reserva['HORA'])){
                $sentencia3 = "DELETE FROM HORARIO_PISTA WHERE (ID_PISTA='".$reserva['ID_PISTA']."' AND FECHA='".$reserva['FECHA']."' AND HORA='".$reserva['HORA']."')";
                $this->mysqli->query($sentencia3);
            }
        }
    }

    public function checkHoraCompleta($fecha,$hora){
        $horaCompleta = TRUE;
        $sentencia="SELECT * FROM PISTA";
        $result = $this->mysqli->query($sentencia);
        $numPistas = $result->num_rows;
        for($i=1;$i<=$numPistas;$i++){
            $sentencia2="SELECT * FROM HORARIO_PISTA WHERE ID_PISTA='$i' AND FECHA='$fecha' AND HORA='$hora'";
            $result2=$this->mysqli->query($sentencia2);
            if($result2->num_rows == 0){
                $horaCompleta = FALSE;
            }
        }
        return $horaCompleta;
        
    }

    private function matrixToAssoc($matrix){
        $aux = array();
        $toRet = array();
        for ($i=0; $i < count($matrix); $i++) { 
            if(array_key_exists($matrix[$i][0],$toRet)){
                array_push($aux, $matrix[$i][1]);
                $toRet[$matrix[$i][0]] = $aux;
            }else{
                $aux = array();
                array_push($aux, $matrix[$i][1]);
                $toRet[$matrix[$i][0]] = $aux;
            }
        }
        
        return $toRet;
    }

    private function convertirFecha($fechaC){ 
        $date = str_replace('-', '/', $fechaC); 
        return date('d/m/Y', strtotime($date)); 
    }
}
?>