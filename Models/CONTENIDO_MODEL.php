<?php
//Acceso a BD
include_once '../Functions/BdAdmin.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../Libraries/PHPMailer/src/Exception.php';
require '../Libraries/PHPMailer/src/PHPMailer.php';
require '../Libraries/PHPMailer/src/SMTP.php';

class Contenido{
    var $id_contenido;
    var $titulo;
    var $texto;
    var $fecha_publicacion;

    public function __construct($id_contenido=NULL,$titulo=NULL,$texto=NULL,$fecha_publicacion=NULL){
		//asignación de valores de parámetro a los atributos de la clase
        $this->id_contenido = $id_contenido;
        $this->titulo = $titulo;
        $this->texto = $texto;
        $this->fecha_publicacion = $fecha_publicacion;

        // conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    public function getIdContenido(){
        return $this->id_contenido;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setTitulo($titulo){
        $this->titulo = $titulo;
    }

    public function getTexto(){
        return $this->texto;
    }

    public function setTexto(){
        $this->texto = $texto;
    }

    public function getFechaPublicacion(){
        return $this->fecha_publicacion;
    }

    public function getAllContenido(){ 
        $sentencia = "SELECT ID_CONTENIDO FROM CONTENIDO"; 
        $resultado = $this->mysqli->query($sentencia); 
        $toRet = array(); 
        if($resultado != false){ 
            $matrix = $resultado->fetch_all(); 
            $toRet = $this->matrixToArray($matrix); 
        }else{ 
            $toRet = NULL; 
        } 
 
        return $toRet; 
    }

    public function findAll(){
        $sentencia = "SELECT * FROM CONTENIDO"; 
        $resultado = $this->mysqli->query($sentencia);
        return $resultado;
    }
    
    public function findById($id_contenido){
        $sentencia = "SELECT * FROM CONTENIDO WHERE ID_CONTENIDO=$id_contenido";
        $resultado = $this->mysqli->query($sentencia);
        $contenido = $resultado->fetch_assoc();

        if($contenido != NULL){
            return new Contenido($contenido["ID_CONTENIDO"],$contenido["TITULO"],$contenido["TEXTO"],$contenido["FECHA_PUBLICACION"]);
        } else {
            return NULL;
        }
    }

    public function add(){
        $sentencia = "INSERT INTO CONTENIDO(TITULO, TEXTO, FECHA_PUBLICACION) VALUES ('$this->titulo', '$this->texto', '$this->fecha_publicacion')";
        if($this->mysqli->query($sentencia) == TRUE){
            $this->sendEmails();
            return "Contenido añadido correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    public function delete(){
        $sentencia = "DELETE FROM CONTENIDO WHERE (ID_CONTENIDO='$this->id_contenido')";
        $resultado = $this->mysqli->query($sentencia);
        if($resultado != FALSE){
            return "Contenido eliminado correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    public function update(){
        $sentencia = "UPDATE CONTENIDO SET TITULO=$contenido->getTitulo(), TEXTO=$contenido->getTexto() WHERE ID_CONTENIDO =$contenido->getIdContenido()";
        if($this->mysqli->query($sentencia) === TRUE){
            return "Contenido actualizado correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    private function matrixToArray($matrix){ 
        $toRet = array(); 
        foreach ($matrix as $value) { 
            array_push($toRet,$value[0]); 
        } 
 
        return $toRet; 
    }

    private function sendEmails(){
        $emails = $this->getEmailList();
        
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPAuth = true; // enable SMTP authentication
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
        $mail->Port = 587; // set the SMTP port for the GMAIL server
        $mail->Username = "oupadel@gmail.com"; // GMAIL username
        $mail->Password = "mailabp42"; // GMAIL password

        foreach($emails as $email){
            $mail->AddAddress($email["EMAIL"]);
            $mail->SetFrom("oupadel@gmail.com");
            $mail->Subject = "Nueva noticia en Oupadel";
            $mail->Body = "Se ha publicado una nueva noticia en la sección de contenido de OuPadel. Si quieres estar al tanto de todas las novedades, te recomentamos que la revises.";
            try{
                $mail->Send();
            } catch(Exception $e){
                //Something went bad
                echo "Fail :(";
            }
        }
    }

    private function getEmailList(){
        $sentencia = "SELECT EMAIL FROM DEPORTISTA WHERE EMAIL IS NOT NULL AND EMAIL<>''";
        $resultado = $this->mysqli->query($sentencia);
        return $resultado;
    }
}

?>