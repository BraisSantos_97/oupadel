<?php
//Acceso a BD
include_once '../Functions/BdAdmin.php';

class Pista{
    var $id_pista;
    var $tipo_pista;
    var $descripcion_pista;

    public function __construct($id_pista=NULL,$tipo_pista=NULL,$descripcion_pista=NULL){
		//asignación de valores de parámetro a los atributos de la clase
        $this->id_pista = $id_pista;
        $this->tipo_pista = $tipo_pista;
        $this->descripcion_pista = $descripcion_pista;

        // conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    public function getIdPista(){
        return $this->id_pista;
    }

    public function getTipoPista(){
        return $this->tipo_pista;
    }

    public function setTipoPista($tipo_pista){
        $this->tipo_pista = $tipo_pista;
    }

    public function getDescripcionPista(){
        return $this->descripcion_pista;
    }

    public function setDescripcionPista(){
        $this->descripcion_pista = $descripcion_pista;
    }

    public function getAllPistas(){ 
        $sentencia = "SELECT ID_PISTA FROM PISTA"; 
        $resultado = $this->mysqli->query($sentencia); 
        $toRet = array(); 
        if($resultado != false){ 
            $matrix = $resultado->fetch_all(); 
            $toRet = $this->matrixToArray($matrix); 
        }else{ 
            $toRet = NULL; 
        } 
 
        return $toRet; 
    }
    
    public function findById($id_pista){
        $sentencia = "SELECT * FROM PISTA WHERE ID_PISTA=$id_pista";
        $resultado = $this->mysqli->query($sentencia);
        $pista = $resultado->fetch_assoc();

        if($pista != NULL){
            return new Pista($pista["ID_PISTA"],$pista["TIPO_PISTA"],$pista["DESCRIPCION_PISTA"]);
        } else {
            return NULL;
        }
    }

    public function findAll(){
        $sentencia = "SELECT * FROM PISTA";
        $resultado = $this->mysqli->query($sentencia);

        return $resultado;
    }

    public function add(){
        $sentencia = "INSERT INTO PISTA(TIPO_PISTA, DESCRIPCION_PISTA) VALUES ('$this->tipo_pista', '$this->descripcion_pista')";
        if($this->mysqli->query($sentencia) == TRUE){
            $this->corregirAdd();
            return "Pista añadida correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    public function delete(){
        $sentencia = "DELETE FROM PISTA WHERE (ID_PISTA='$this->id_pista')";
        //$this->actualizarId($this->id_pista);
        $resultado = $this->mysqli->query($sentencia);
        if($resultado != FALSE){
            $this->actualizarId($this->id_pista);
            return "Pista eliminada correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    public function update(){
        $sentencia = "UPDATE PISTA SET TIPO_PISTA=$pista->getTipoPista(), DESCRIPCION_PISTA=$pista->getDescripcionPista() WHERE ID_PISTA =$pista->getIdPista()";
        if($this->mysqli->query($sentencia) === TRUE){
            return "Pista actualizada correctamente";
        } else {
            return "Error: " . $sentencia . "<br>" . $this->mysqli->error;
        }
    }

    private function matrixToArray($matrix){ 
        $toRet = array(); 
        foreach ($matrix as $value) { 
            array_push($toRet,$value[0]); 
        } 
 
        return $toRet; 
    }

    private function actualizarId($id_pista){
        $sentencia="SELECT * FROM PISTA";
        $result = $this->mysqli->query($sentencia);
        $numRows = $result->num_rows;
        $numRows= $numRows+1;
        $aux = (int) $id_pista;
        $aux= $aux+1;
        for($aux; $aux<=$numRows; $aux++){
            $aux2 = $aux-1;
            $update = "UPDATE PISTA SET ID_PISTA=". $aux2 ." WHERE ID_PISTA =".$aux;
            $resultado = $this->mysqli->query($update);
            
        }
       
    }

    private function corregirAdd(){
        $sentencia = "SELECT MAX(ID_PISTA) AS maxid FROM PISTA";
        $result = $this->mysqli->query($sentencia);
        $aux = $result->fetch_assoc();
        $max = (int) $aux['maxid'];

        $sentencia2="SELECT * FROM PISTA";
        $result2 = $this->mysqli->query($sentencia2);
        $numRows = $result2->num_rows;
        $update = "UPDATE PISTA SET ID_PISTA=". $numRows ." WHERE ID_PISTA =".$max;
        $this->mysqli->query($update);
    }
}

?>