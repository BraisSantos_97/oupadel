<?php

//declaración de la clase
class PAREJA_ENFRENTAMIENTO_MODEL{ 

	var $id_pareja; // declaración del atributo login
    var $id_enfrentamiento;
    var $nombre_grupo;//declaración del atributo password
	var $nombre_categoria; // declaración del atributo Nombre
	var $nombre_campeonato; // declaración del atributo Apellidos
	var $nivel;
    
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($id_pareja,$id_enfrentamiento,$nombre_grupo,$nombre_categoria,$nombre_campeonato,$nivel) {
		
        $this->id_pareja=$id_pareja;
		$this->id_enfrentamiento=$id_enfrentamiento;
        $this->nombre_grupo=$nombre_grupo;
        $this->nombre_categoria=$nombre_categoria;
        $this->nombre_campeonato=$nombre_campeonato;
        $this->nivel = $nivel;
       
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
        $this->mysqli = ConectarBD();
        $this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor
    

    function ADD() {
			
							$sql = "INSERT INTO pareja_enfrentamiento (
                                                      ID_PAREJA,
                                                      ID_ENFRENTAMIENTO,
                                                      ID_GRUPO,
                                                      NOMBRE_CATEGORIA,
                                                      NOMBRE_CAMPEONATO,
                                                      NIVEL
                                                         ) 
                                                        VALUES(
                                                        '$this->id_pareja',
                                                        '$this->id_enfrentamiento',
                                                        '$this->nombre_grupo',
                                                        '$this->nombre_categoria',
                                                        '$this->nombre_campeonato',
                                                        '$this->nivel'
                                                        )";
							

					
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						return 'Insercion realizada con exito';
						
					}


    }

	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct



} //fin de clase

?>