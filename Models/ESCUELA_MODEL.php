<?php

//declaración de la clase
class ESCUELA_MODEL{ 

	var $nombre_escuela; // declaración del atributo login
    var $descripcion;//declaración del atributo password
	var $tipo_clase; // declaración del atributo Nombre
	
	var $mysqli; // declaración del atributo manejador de la bd
	

    //Constructor de la clase
	function __construct($nombre_escuela,$descripcion,$tipo_clase) {
		
		$this->nombre_escuela=$nombre_escuela;
        $this->descripcion=$descripcion;
        $this->tipo_clase=$tipo_clase;
        
		// incluimos la funcion de acceso a la bd
		include_once '../Functions/BdAdmin.php';
		// conectamos con la bd y guardamos el manejador en un atributo de la clase
		$this->mysqli = ConectarBD();
		$this->mysqli->query("SET NAMES 'utf8'");

	} // fin del constructor
    

    function RellenaDatos() { 

		$sql = "SELECT * FROM escuela_deportiva WHERE (NOMBRE_ESCUELA = '$this->nombre_escuela')";// se construye la sentencia de busqueda de la tupla
		// Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; // 
		} else { // si existe se devuelve la tupla resultado
            //Aplicamos fetch_array sobre $resultado para crear un array y se guarda en $result
			$result = $resultado->fetch_array();
			return $result;
		}
        
	}


	function SEARCH() {
		// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
		$sql = "select  
                    NOMBRE_ESCUELA,
					DESCRIPCION,
					TIPO_CLASE
       			from escuela_deportiva
    			where 
    				(
					(BINARY NOMBRE_ESCUELA LIKE '%$this->nombre_escuela%') &&
                    (BINARY DESCRIPCION LIKE '%$this->descripcion%') &&
					(BINARY TIPO_CLASE LIKE '%$this->tipo_clase%')
    				)";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return $resultado;
		}
	}


	function DELETE() {    

			$sql = "DELETE FROM escuela_deportiva WHERE (NOMBRE_ESCUELA = '$this->nombre_escuela')";
		// si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en el borrado sobre la base de datos';
		} else { // si la busqueda es correcta devolvemos el recordset resultado

			return 'Borrado correctamente';
		}
	} 


	function ADD() {
        
        if($this->nombre_escuela <> ''){
            
      
				
			$sql = "INSERT INTO escuela_deportiva (
                                    NOMBRE_ESCUELA,
                                    DESCRIPCION,
                                    TIPO_CLASE
                                       ) 
                                      VALUES(
                                      '$this->nombre_escuela',
                                      '$this->descripcion',
                                      '$this->tipo_clase'
                                      )";
            
            
            
						
					if ( !$this->mysqli->query( $sql )) { // si da error en la ejecución del insert devolvemos mensaje
						return 'Error en la inserción';
					} else { //si no da error en la insercion devolvemos mensaje de exito
						
						/*if($mensaje == 'Inserción realizada con éxito'){//miramos si la inserción en USU_GRUPO tuvo exito
							return 'Inserción realizada con éxito'; //operacion de insertado correcta
						}else{//si la insercion no tuvo exito
							return $mensaje;
						}*/
						return 'Insercion realizada con exito';
						
					}
          }
        else{
            return "Introduzca un valor";
        }
        


    }

	//funcion de destrucción del objeto: se ejecuta automaticamente
	//al finalizar el script
	function __destruct() {

	} // fin del metodo destruct



} //fin de clase

?>