<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class CONTENIDO_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$datos) {
        $this->lista=$lista;
        $this->datos=$datos;
		$this->render($this->datos,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($datos,$lista) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->datos =$datos;
        $this->lista=$lista;
?>

<section>
    <h1 class="h1-responsive text-center">Contenido</h1>
    <div class="container">
        
        <?php
        if($_SESSION['login'] == 'admin'){
        ?>
            <form action='../Controllers/CONTENIDO_CONTROLLER.php'>
                <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir contenido <i class="fas fa-plus-circle"></i></button>
            </form>
        <?php
        }
        ?>
        <?php
        while($fila = mysqli_fetch_array($this->datos)){ //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos
        ?>
        <div class="">
            <div class="panel panel-default">
                <p class="panel-heading h3"><?= $fila["TITULO"] ?></p>
                <textarea class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-body" rows="5" readonly><?php echo $fila["TEXTO"]; ?></textarea>
                <div class="row">
                    <p class="col-xs-6 col-sm-9 col-md-10 col-lg-10"><?= $fila["FECHA_PUBLICACION"] ?></p>
                    <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                        <form class="" action="../Controllers/CONTENIDO_CONTROLLER.php" method="get" style="display:inline" >
                            <input type="hidden" name="id_contenido" value="<?php echo $fila['ID_CONTENIDO']; ?>">

                            <button class="btn btn-default" type="submit" name="action" value="SHOWCURRENT" ><i class="far fa-eye"></i></button>
                        </form>
                        <?php
                        if($_SESSION['login'] =='admin'){     
                        ?>
                            <form class="" action="../Controllers/CONTENIDO_CONTROLLER.php" method="get" style="display:inline" >
                                <input type="hidden" name="id_contenido" value="<?php echo $fila['ID_CONTENIDO']; ?>">
                                
                                <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                            </form>
                        <?php
                        }              
                        ?>
                    </div>
                </div>
            <div>
        </div>
        <?php
        }
        ?>
    </div>
</section>

<?php
	include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

} //fin Login

?>