<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class PISTA_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$datos) {
        $this->lista=$lista;
        $this->datos=$datos;
		$this->render($this->datos,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($datos,$lista) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->datos =$datos;
        $this->lista=$lista;
?>

	<!--==========================
    Hero Section
    ============================-->

<section   class="section">
    <div class="container">
        <h2>Lista de reservas</h2>    
        <table class="table table-bordred table-striped">
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/RESERVA_CONTROLLER.php'>
                        <button type="submit" name="action" value="ADD">Añadir reserva <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            <tr>
                <?php
			    foreach ($lista as $atributo) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
                ?>
				<th>
					<?php echo $atributo; ?>
				</th>
                <?php
				}              
                ?>
                <th>Opciones</th>
			</tr>
            <?php
			while ( $fila = mysqli_fetch_array( $this->datos ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos
            ?>
			<tr>
                <?php
				foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
                ?>
				<td>
                <?php 
                echo $fila[ $atributo ];
                ?>
                </td>
            <?php
            }
            ?>
                <td>
					<form action="../Controllers/RESERVA_CONTROLLER.php" method="get" style="display:inline" >
				        <input type="hidden" name="id_pista" value="<?= $fila['ID_PISTA']; ?>">
                        <input type="hidden" name="fecha" value="<?= $fila['FECHA']; ?>">
                        <input type="hidden" name="hora" value="<?= $fila['HORA']; ?>">
                        <input type="hidden" name="login" value="<?= $fila['LOGIN']; ?>">
				        <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                    </form>
				</td>
			</tr>
            <?php
			}
            ?>
        </table>
    </div>
</section><!-- #hero -->

<?php
	include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

} //fin Login

?>