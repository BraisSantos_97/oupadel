<!--==========================
    Footer
  ============================-->
  <footer class="footer">
    <div class="container">
      <div class="row">       
        <div class="col-sm-6 col-md-3 col-lg-2">
          <div class="list-menu">

            <h4>Creadores de OuPadel</h4>

            <ul class="list-unstyled">
              <li><a href="#">Brais Santos</a></li>
              <li><a href="#">Sergio Martínez</a></li>
              <li><a href="#">Martín Varela</a></li>
              <li><a href="#">Iván hervella</a></li>
            </ul>

          </div>
        </div>

      </div>
    </div>

  </footer>

  <!-- JavaScript Libraries -->
  <script src="../Views/lib/jquery/jquery.min.js"></script>
  <!-- <script src="../Viewslib/jquery/jquery-migrate.min.js"></script>-->
  <script src="../Views/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../Views/lib/superfish/hoverIntent.js"></script>
  <script src="../Views/lib/superfish/superfish.min.js"></script>
  <script src="../Views/lib/easing/easing.min.js"></script>
  <script src="../Views/lib/modal-video/js/modal-video.js"></script>
  <script src="../Views/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="../Views/lib/wow/wow.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="../Views/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="../Views/js/main.js"></script>

</body>
</html>
