<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class GRUPO_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$campeonato,$categoria,$nivel,$fecha_limite,$capitan) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->campeonato=$campeonato;
        $this->categoria=$categoria;
        $this->fecha_limite = $fecha_limite;
		$this->render($this->valores,$this->lista,$this->campeonato,$this->categoria,$nivel,$this->fecha_limite,$capitan);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$campeonato,$categoria,$nivel,$fecha_limite,$capitan) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->categoria = $categoria;
        $this->fecha_limite = $fecha_limite;
        $this->capitan=$capitan;
        $this->nivel = $nivel;
       
		?>

<section class="wow fadeIn">
    <div class="container">
      <h2>Grupos de <?php echo $this->campeonato; ?> <?php echo $this->categoria; ?> <?php echo $this->nivel;   ?> </h2>
        
        <table class="table table-bordred table-striped">
            
            <?php
                if($_SESSION['login'] == 'admin'){
                    
                    $fecha_actual=date("d/m/Y");
                    $porciones = explode("/", $fecha_actual);
                    $dia_actual= intval($porciones[0]);
                    $mes_actual= intval($porciones[1]);
                    $ano_actual= intval($porciones[2]);
                    
                    $partes=explode("/", $fecha_limite);
                    $dia_limite= intval($partes[0]);
                    $mes_limite= intval($partes[1]);
                    $ano_limite= intval($partes[2]);
                    
                  
                    if(($dia_actual >= $dia_limite && $mes_actual >= $mes_limite && $ano_actual >=$ano_limite) || ($mes_actual > $mes_limite && $ano_actual >=$ano_limite) || ($ano_actual >$ano_limite) ) {
                ?>
                <tr>
                    <th colspan="100%">
                        <form action='../Controllers/GRUPOS_CONTROLLER.php'>
                            <input type="hidden" name="nombre_campeonato" value="<?php echo $this->campeonato; ?>">
                            <input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria; ?>">
                            <input type="hidden" name="nivel" value="<?php echo $this->nivel; ?>">
                            
                            
                            <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir grupo <i class="fas fa-plus-circle"></i></button>
                        </form>
                    </th>
                </tr>
                            
            
                <?php
                        
                        
                    }
                    
                ?>
            
            
            <?php
                }
            ?>
       
            
            
        <tr>
<?php
$nombreatributos=array("ID_GRUPO"=>"Grupo","NOMBRE_CAMPEONATO"=>"Campeonato","NOMBRE_CATEGORIA"=>"Categoria","NIVEL"=>"Nivel");
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
						<?php echo $nombreatributos[$atributo]; ?>
					</th>
<?php
					}
                    
 ?>
                    <?php
                if($_SESSION['login'] =='admin'){ 
                    
            ?>
                    <th>Borrar</th>
                    <th>Insertar parejas</th>
                    <th>Mostrar Parejas</th>
                
            <?php
                }
    
            ?>
                   <th>Ver parejas</th>
           <?php
                if($_SESSION['login'] =='admin'){ 
                    
            ?>
                    <th>Gestionar Resultados</th>

                
            <?php
                }
    
            ?>
                 <th>Ver Clasificación</th>
            
            
            
            <?php
            
            
        
                if($_SESSION['login']!='admin' && $this->capitan==true){
              
           ?>
            
                 <th>Fechas propuestas</th>
            <?php
                    
                }
                  
            ?>
                <th>Enfrentamientos disputados</th>
            
                
                <th>Ver tabla de cruces</th>
        
            
                        
				</tr>
<?php
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 
                     echo $fila[ $atributo ];
                       
            
?>
					</td>
<?php
					}
                    
                    
                    
                    if($_SESSION['login'] =='admin'){
                        
                  
?>
                    <td>
						<form action="../Controllers/GRUPOS_CONTROLLER.php" method="get" style="display:inline" >
                            
                           
                            <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                            <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
					
						  <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                            </form>
                        
					</td>
                    
                    <td>
						<form action="../Controllers/PAREJA_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
				         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
							
						  <button class="btn btn-default" type="submit" name="action" value="INSERTAR_PAREJAS"><i class="fas fa-handshake"></i></button>
                            </form>
                        
					</td>
                    
                    
                     <td>
						<form action="../Controllers/PAREJA_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
				         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
							
						  <button class="btn btn-default" type="submit" name="action" value="MOSTRAR_PAREJAS"><i class="fas fa-handshake"></i></button>
                            </form>
                        
					</td>
                    
                  
    <?php
        }           
                   
    ?>
                     <td>
						<form action="../Controllers/PAREJA_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
				         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
				         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">			
                            
						  <button class="btn btn-default" type="submit" name="action" value="VER_PAREJAS_GRUPO" ><i class="fas fa-eye"></i></button>
                            </form>
                        
					</td>

                    <?php

                        if($_SESSION['login'] =='admin'){
                    ?>
                    
                    <td>
                        <form action="../Controllers/ENFRENTAMIENTOS_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
                         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
                            
                          <button class="btn btn-default" type="submit" name="action" value="GESTIONAR_RESULTADOS" ><i class="fas fa-poll"></i></button>
                            </form>
                        
                    </td>


                    <?php


                    }

                    ?>


                    <td>
                        <form action="../Controllers/ENFRENTAMIENTOS_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
                         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">    
                            
                            
                          <button class="btn btn-default" type="submit" name="action" value="VER_CLASIFICACION" ><i class="fas fa-award"></i></button>
                        </form>
                        
                    </td>



<?php
    if($_SESSION['login']!='admin'){

      


?>
            <?php
         
                    if($this->capitan == true){
                        
                  
           ?>
                    
                     <td>
                        <form action="../Controllers/GRUPOS_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
                         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">     
                            
                            
                          <button class="btn btn-default" type="submit" name="action" value="FECHA_PROPUESTA" ><img src="../Views/img/enfrentamiento.png" alt="GESTIONAR_RESULTADOS" width="20" height="20" /></button>
                            </form>
                        
                    </td>
                    
            <?php
                  }
            ?>
                   

<?php
 }

?>  
                    <td>
                        <form action="../Controllers/ENFRENTAMIENTOS_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
                         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">     
                            
                            
                          <button class="btn btn-default" type="submit" name="action" value="TODOS_ENFRENTAMIENTOS" ><i class="fas fa-table-tennis"></i></button>
                            </form>
                        
                    </td>
                    
                    <td>
                        <form action="../Controllers/CRUCES_CONTROLLER.php" method="get" style="display:inline" >
                            
                         <input type="hidden" name="id_grupo" value="<?php echo $fila['ID_GRUPO']; ?>">  
                         <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
                         <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                         <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">     
                            
                            
                          <button class="btn btn-default" type="submit" name="action" value="" ><i class="fas fa-eye"></i></button>
                            </form>
                        
                    </td>
                        					   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>