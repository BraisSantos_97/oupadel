<script language="JavaScript" type="text/javascript" >


// function hayEspacio(campo): Comprueba que no tenga espacios el campo
function sinEspacio(campo) {
	//Comprueba si hay algun espacio
	if (/[\s]/.test(campo.value)) {
		//mensaje multidioma
		msgError('El atributo ' + atributo[campo.name] + ' no puede tener espacios');
		campo.focus();
		return false;
	}
	return true; //Devuelve "true"
}
/*
	function comprobarVacio(campo): realiza una comprobación de si el campo es vacío o está compuesto de espacios en blanco.
*/
function comprobarVacio(campo) {
	/*Si el campo es nulo, tiene longitud 0 o está compuesto por espacios en blanco, se muestra un mensaje de error y se retorna false*/
	if (campo.value == null || campo.value.length == 0 || /^\s+$/.test(campo.value)) {
		msgError('El atributo ' + atributo[campo.name] + ' no puede ser vacio');
		campo.focus();
		return false;
	}
	return true;
}
/*
	function comprobarLongitud(campo,size): realiza una comprobación de si el campo es mayor que el indicado en el parámetro size.
*/
function comprobarLongitud(campo, size) {
	/*Si el campo tiene mayor longitud que size, se manda un aviso de error llamando a la función msgError y se retorna false */
	if (campo.value.length > size) {
		msgError('Longitud incorrecta. El atributo ' + atributo[campo.name] + ' puede tener una longitud máxima de ' + size + '< y es de  ' + campo.value.length);
		campo.focus();
		return false;
	}
	return true;
}
/*
	function comrpobarTexto(campo,size): realiza una comprobación de si el valor de campo contiene algún carácter especial.
*/
function comprobarTexto(campo, size) {

	var i; //variable auxiliar de control
	/*Estructura que permite recorrer todos los caracteres del valor de campo */
	for (i = 0; i < size; i++) {
		/*Comprueba que el carácter seleccionado de campo no es un carácter especial, si es así muestra un mensaje de error y retorna false */
		if (/[^!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~ñáéíóúÑÁÉÍÓÚüÜ ]/.test(campo.value.charAt(i))) {
			msgError('"El atributo ' + atributo[campo.name] + ' contiene algún carácter no válido: ' + campo.value.charAt(i));
			campo.focus();
			return false;
		}
	}

	return true;
}
/*
	function comprobarAlfabetico(campo,size): realiza una comprobación de si el campo contiene letras minúsculas o mayúsculas (los espacios también están incluidos).
*/
function comprobarAlfabetico(campo, size) {
	var i; //variable auxiliar de control
	/*Estructura que permite recorrer todos los caracteres del valor de campo */
	for (i = 0; i < size; i++) {
		/*Comprueba que el carácter seleccionado de campo no es una letra o un espacio, si es así se muestra un mensaje de error y retorna false */
		if (/[^A-Za-zñáéíóúÑÁÉÍÓÚüÜ -]/.test(campo.value.charAt(i))) {
			msgError('El atributo ' + atributo[campo.name] + ' solo admite carácteres alfabéticos ');
			campo.focus();
			return false;
		}
	}
	return true;
}
/*
	function comprobarCampoNumFormSearch(campo,size,valormenor,valormayor): realiza una comprobación de si el contenido de un campo numérico de un 
	formulario de búsqueda es correcto.
*/
function comprobarCampoNumFormSearch(campo, size, valormenor, valormayor) {

	/*Si el campo es nulo, tiene longitud 0 o está compuesto por espacios en blanco, se retorna true. Si no es así se valida el campo*/
	if (campo.value == null || campo.value.length == 0 || /^\s+$/.test(campo.value)) {
		return true;
	} else {// si no cumple con la condición del if anterior,
		/*Comprueba que el campo no tenga una longitud mayor que el indicado por size, si la supera, se retorna false*/
		if (!comprobarLongitud(campo, size)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprueba que el campo no contenga carácteres especiales, si no es así, se retorna false */
			if (!comprobarTexto(campo, size)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				/*Comprueba que el campo es un dígito y es mayor que valormenor y es menor que valormayor, si no es así, se retorna false */
				if (!comprobarEntero(campo, valormenor, valormayor)) {
					return false;
				}
			}
		}
	}

	return true;
}
/*
	function comprobarEntero(campo,valormenor,valormayor): realiza una comprobación de que el campo sea entero. Si es así comprueba que es un número
	comprendido entre el valormenor y valormayor
*/
function comprobarEntero(campo, valormenor, valormayor) {

	/*Comprueba que campo es un dígito*/
	if (!/^([0-9])*$/.test(campo.value)) {
		msgError(' El atributo ' + atributo[campo.name] + ' tiene que ser un dígito');
		campo.focus();
		return false;
	} else {
		/*Comprueba que el valor de campo es mayor que valormayor, si es así muestra un mensaje de error y retorna false */
		if (campo.value > valormayor) {
			msgError('El atributo ' + atributo[campo.name] + ' no puede ser mayor que ' + valormayor);
			campo.focus();
			return false;
		} else {
			/*Comprueba que el valor de campo es menor que valormenor, si es así muestra un mensaje de error y retorna false */
			if (campo.value < valormenor) {
				msgError('El atributo ' + atributo[campo.name] + ' no puede ser menor que ' + valormenor);
				campo.focus();
				return false;
			}
		}
	}

	return true;
}


/*
	function comprobarTelf(campo): realiza la comprobación de que el valor de campo tenga el formato de teléfono español, tanto para nacional como internacional
*/
function comprobarTelf(campo) {
	/*Si el valor del campo no cumple el formato de la expresión, se muestra un mensaje de error y se retorna false*/
	if (!/^(34)?[6|7|9][0-9]{8}$/.test(campo.value)) {
		msgError('El atributo ' + atributo[campo.name] + ' tiene un formato erróneo');
		campo.focus();
		return false;
	}
	return true;
}


/*
	function abrirVentana(): realiza la función de abrir una ventana emergente, a través de una capa, capaFondo1, para que no se pueda interacionar con la capa base. Estas dos capas se activan y su visibility pasa a visible.
*/
function abrirVentana() {
	document.getElementById("capaFondo1").style.visibility = "visible";//Se establece la capa de fondo a visible
	document.getElementById("capaVentana").style.visibility = "visible";//Se establece la capa de ventana a visible
	document.formError.bAceptar.focus();
}
/*
	function cerrarVentana(): realiza la función de cerrar una ventana emergente, a través de una capa, capaFondo1, para que no se pueda interacionar con la capa base. Estas dos capas se desactivas y su visibility pasa a hidden.
*/
function cerrarVentana() {
	document.getElementById("capaFondo1").style.visibility = "hidden";
	document.getElementById("capaVentana").style.visibility = "hidden";//Se establece la capa de ventana a oculta
	document.formError.bAceptar.blur();
}
/*
	msgError(msg): realiza la función de mostrar el valor del parámetro msg en el div, cuyo id es "miDiv". Además se encarga de llamar a la función abrirVentana(), la cual muestra un mensaje de error cuya información está en html
*/
function msgError(msg) {

	var miDiv = document.getElementById("miDiv"); // Cogemos la referencia al nuestro div.
	var html = ""; //En esta variable guardamos lo que queramos añadir al div.

	miDiv.innerHTML = ""; //innerHTML te añade código a lo que ya haya por eso primero lo ponemos en blanco.
	html = msg;
	miDiv.innerHTML = html;//añadimos el texto que queramos al div
	abrirVentana();
	return true;
}

/*
	function comprobarLogin():valida todos los campos del formulario login antes de realizar el submit
*/
function comprobarLogin() {

	var login; /*variable que representa el elemento login del formulario de login */
	var pwd; /*variable que representa el elemento password del formulario de login */

	login = document.forms['Form'].elements[0];
	pwd = document.forms['Form'].elements[1];

	/*Comprueba si el login es vacio, retorna false*/
	if (!comprobarVacio(login)) {
		return false;
	} else {// si no cumple con la condición del if anterior,
		/*Comprobamos su longitud, si es mayor que 9, retorna false*/
		if (!comprobarLongitud(login, 9)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprobamos si tiene caracteres especiales, si es así, retorna false */
			if (!comprobarTexto(login, 9)) {
				return false;
			}
		}
		/*Comprueba si la password es vacio, retorna false*/
		if (!comprobarVacio(pwd)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprueba su longitud, si es mayor que 20, retorna false*/
			if (!comprobarLongitud(pwd, 20)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				/*Comprueba si tiene caracteres especiales, si es así, retorna false */
				if (!comprobarTexto(pwd, 20)) {
					return false;
				}
			}
		}

	}

//	encriptar();

	return true;
}
/*
	function encriptar(): encripta en md5 el valor del campo password
*/
/*function encriptar() {
	document.getElementById('password').value = hex_md5(document.getElementById('password').value); //cambia el valor del campo password introducido por el usuario, 																							   //por el valor de la password encriptada
}*/
/*
	function comprobarAdd():valida todos los campos del formulario add antes de realizar el submit
*/

function comprobarLogeo() {

	var login; /*variable que representa el elemento login del formulario add */
	var pwd; /*variable que representa el elemento password del formulario add */
	
	
    
    
	login = document.forms['LOGIN'].elements[0];
	pwd = document.forms['LOGIN'].elements[1];
	


	/*Comprueba si login es vacio, retorna false*/
	if (!comprobarVacio(login)) {
		return false;
	} 
   /* else {// si no cumple con la condición del if anterior,
		
		if (!sinEspacio(login)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			
			if (!comprobarLongitud(login, 20)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				
				if (!comprobarTexto(login, 20)) {
					return false;
				}
			}
		}
	}
    
	/*Comprueba si password es vacio, retorna false*/
	/*if (!comprobarVacio(pwd)) {
		return false;
	} else {// si no cumple con la condición del if anterior,
		//Comprobamos que no hay espacio s intermedios
		if (!sinEspacio(pwd)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
        */
			/*Comprueba su longitud, si es mayor que 20, retorna false*/
			/*if (!comprobarLongitud(pwd, 20)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
            */
				/*Comprueba si tiene caracteres especiales, si es así, retorna false */
		/*		if (!comprobarTexto(pwd, 20)) {
					return false;
				}
			}
		}
	}*/
	
	//encriptar();
	return true;
}



function comprobarAddUsuario() {

	var login; /*variable que representa el elemento login del formulario add */
	var pwd; /*variable que representa el elemento password del formulario add */
	var nombreuser; /*variable que representa el elemento nombresuser del formulario add */
	var apellidosuser; /*variable que representa el elemento apellidosuser del formulario add */
	var telefono; /*variable que representa el elemento telefono del formulario add */
	
    
    
	login = document.forms['LOGIN'].elements[0];
	pwd = document.forms['LOGIN'].elements[1];
	nombreuser = document.forms['LOGIN'].elements[2];
	apellidosuser = document.forms['LOGIN'].elements[3];
    telefono = document.forms['LOGIN'].elements[4];


	/*Comprueba si login es vacio, retorna false*/
	if (!comprobarVacio(login)) {
		return false;
	} else {// si no cumple con la condición del if anterior,
		//Comprobamos que no hay espacio s intermedios
		if (!sinEspacio(login)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprobamos su longitud, si es mayor que 15, retorna false*/
			if (!comprobarLongitud(login, 20)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				/*Comprobamos si tiene caracteres especiales, si es así, retorna false */
				if (!comprobarTexto(login, 20)) {
					return false;
				}
			}
		}
	}
	/*Comprueba si password es vacio, retorna false*/
	if (!comprobarVacio(pwd)) {
		return false;
	} else {// si no cumple con la condición del if anterior,
		//Comprobamos que no hay espacio s intermedios
		if (!sinEspacio(pwd)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprueba su longitud, si es mayor que 20, retorna false*/
			if (!comprobarLongitud(pwd, 20)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				/*Comprueba si tiene caracteres especiales, si es así, retorna false */
				if (!comprobarTexto(pwd, 20)) {
					return false;
				}
			}
		}
	}
	
	/*Comprueba si nombreuser es vacio, retorna false*/
	if (!comprobarVacio(nombreuser)) {
		return false;
	} else {// si no cumple con la condición del if anterior,
		/*Comprueba su longitud, si es mayor que 30, retorna false*/
		if (!comprobarLongitud(nombreuser, 25)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprueba si tiene caracteres especiales, si es así, retorna false */
			if (!comprobarTexto(nombreuser, 25)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				/*Comprueba si tiene carácteres no alfanuméricos, si es así, retorna false */
				if (!comprobarAlfabetico(nombreuser, 25)) {
					return false;
				}
			}

		}
	}
	/*Comprueba si apellidosuser es vacio, retorna false*/
	if (!comprobarVacio(apellidosuser)) {
		return false;
	} else {// si no cumple con la condición del if anterior,
		/*Comprueba su longitud, si es mayor que 50, retorna false*/
		if (!comprobarLongitud(apellidosuser, 50)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprueba si tiene caracteres especiales, si es así, retorna false */
			if (!comprobarTexto(apellidosuser, 50)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				/*Comprueba si tiene carácteres no alfanuméricos, si es así, retorna false */
				if (!comprobarAlfabetico(apellidosuser, 50)) {
					return false;
				}

			}
		}
	}

	/*Comprueba si telelefono es vacio, retorna false*/
	if (!comprobarVacio(telefono)) {
		return false;
	} else {// si no cumple con la condición del if anterior,
		/*Comprueba su longitud, si es mayor que 11, retorna false*/
		if (!comprobarLongitud(telefono, 9)) {
			return false;
		} else {// si no cumple con la condición del if anterior,
			/*Comprueba si tiene caracteres especiales, si es así, retorna false */
			if (!comprobarTexto(telefono, 9)) {
				return false;
			} else {// si no cumple con la condición del if anterior,
				/*Comprueba si el formato no es correcto, si es así, retorna false */
				if (!comprobarTelf(telefono)) {
					return false;
				}

			}
		}
	}
	//encriptar();
	return true;
}
</script>