<?php
//es la clase DELETE de USUARIO que nos permite borrar un usuario
class HORARIO_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($datos,$fecha,$hora) { 
		$this->datos = $datos;//pasamos los datos de cada uno de los campos
		$this->fecha = $fecha;
		$this->hora = $hora;
		$this->render($this->datos,$fecha,$hora);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render($datos,$fecha,$hora) { 
		$this->datos=$datos;//pasamos los datos de cada uno de los campos
		$this->fecha=$fecha;
		$this->hora=$hora;
		include_once '../Views/header.php';//incluimos la cabecera
?>
	<section   class="section">
        <div class="container">
            <h2>Borrar horario: <?= $this->fecha?>, <?= $this->hora?></h2>
			<form name="DELETE" action="../Controllers/HORARIO_CONTROLLER.php?action=DELETE" method="post" enctype="multipart/form-data" onsubmit="return esVacio(fecha) && esVacio(hora) && esVacio(dia)">
                <div class="form-group">
					<label>Fecha</label>
					<input class="form-control" type="text" id="fecha" name="fecha" value="<?= $this->datos->getFecha();?>" readonly/><br>
				</div>
				<div class="form-group">
					<label>Hora</label>
					<input class="form-control" type="text" id="hora" name="hora" value="<?= $this->datos->getHora();?>" readonly/><br>
				</div>
				<div class="form-group">
					<label>Dia</label>
					<input class="form-control" type="text" id="dia" name="dia" value="<?= $this->datos->getDia();?>" readonly/><br>
				</div>
                <button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar horario  <i class="fas fa-trash-alt"></i></button>
			</form>		
		</div>
    </section>
<?php
        include '../Views/footer.php';//incluimos el footer
        }       
    }
?>