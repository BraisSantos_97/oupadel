<?php

//es la clase ADD de USUARIO que nos permite añadir un usuario
class GRUPO_ADD {
//es el constructor de la clase USUARIO_ADD
	function __construct($campeonato,$categoria,$nivel) {
		$this->render($campeonato,$categoria,$nivel);//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render($campeonato,$categoria,$nivel) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->campeonato = $campeonato;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        
?>
<section class="section">
    <div class="container">
      <h2>Añadir grupos para campeonato: <?php echo $campeonato; ?>  Categoria: <?php echo $categoria; ?> y  nivel  <?php echo $this->nivel; ?> </h2>
        
			<form name="ADD" action="../Controllers/GRUPOS_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_grupo) && comprobarExpresionRegular(id_grupo,/[0-9]+/) && esVacio(nombre_campeonato) && comprobarExpresionRegular(nombre_campeonato,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(nombre_categoria) && comprobarExpresionRegular(nombre_categoria,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(nivel)">
                <div class="form-group">
                    <label>Número del grupo</label>
                    <input class="form-control" type="number" id="id_grupo" name="id_grupo" value="" />
				</div>
				<div class="form-group">
					<label>Nombre Campeonato</label>
					<input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="<?php echo $this->campeonato ?>" maxlength="50" size="50" readonly />
				</div>
				<div class="form-group">
					<label>Nombre Categoria</label>
					<input class="form-control" type="text" id="nombre_categoria" name="nombre_categoria" value="<?php echo $this->categoria ?>" maxlength="50" size="50" readonly/>
				</div>
				<div class="form-group">     
					<label>Nivel </label>
					<input class="form-control" type="text" id="nivel" name="nivel" value="<?php echo $this->nivel ?>" maxlength="20" size="20" readonly/>
				</div>
				<button class="btn btn-default" type="submit" name="action" value="ADD">Añadir <i class="fas fa-plus-circle"></i></button>
			 </form>
						
					
				
		</div>
    </section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>