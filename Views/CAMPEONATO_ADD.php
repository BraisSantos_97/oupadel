<?php

//es la clase ADD de USUARIO que nos permite añadir un usuario
class CAMPEONATO_ADD {
//es el constructor de la clase USUARIO_ADD
	function __construct() {
		$this->render();//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render() {
		include_once '../Views/header.php';//incluimos la cabecera
?>
<section class="wow fadeIn">
    <div class="container">
      	<h2>Añadir Campeonato</h2>
		<form name="ADD" action="../Controllers/CAMPEONATO_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(nombre_campeonato) && comprobarExpresionRegular(nombre_campeonato,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(fecha_inicial) && esVacio(fecha_final) && esVacio(descripcion_campeonato) && esVacio(fecha_limite_inscripcion)">
			<div class="form-group">
				<label>Nombre Campeonato</label>
				<input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="" maxlength="50" required/>
			</div>
			<div class="form-group">
				<label>Fecha inicio</label>
				<input class="form-control tcal" type="text" id="fecha_inicial" name="fecha_inicial" value="" maxlength="20" readonly required/>
			</div>
			<div class="form-group" style="position:relative">	
				<label>Fecha final</label>
				<input class="form-control tcal" type="text" id="fecha_final" name="fecha_final" value="" maxlength="20" readonly required/>
			</div>
			<div class="form-group">
				<label>Descripcion campeonato</label>
				<textarea class="form-control" rows="4" cols="50" type="text" id="descripcion_campeonato" name="descripcion_campeonato" maxlength="100" required></textarea>
			</div>
			<div class="form-group">
				<label>Fecha limite inscripcion</label>   
				<input class="form-control tcal" type="text" id="fecha_limite_inscripcion" name="fecha_limite_inscripcion" maxlength="20" readonly required/>
			</div>
			
			<button class="btn btn-default" type="submit" name="action" value="ADD">Añadir campeonato  <i class="fas fa-plus-circle"></i></button>
		</form>
	</div>
</section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>