<?php
//es la clase DELETE de USUARIO que nos permite borrar un usuario
class PISTA_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($datos,$pista) { 
		$this->datos = $datos;//pasamos los datos de cada uno de los campos
		$this->render($this->datos,$pista);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render($datos,$pista) { 
		$this->datos=$datos;//pasamos los datos de cada uno de los campos
		$this->pista=$pista;
		include_once '../Views/header.php';//incluimos la cabecera
?>
	<section class="section">
        <div class="container">
            <h2>Borrar pista: <?=$this->pista?></h2>
			<form name="DELETE" action="../Controllers/PISTA_CONTROLLER.php?action=DELETE" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_pista) && esVacio(tipo_pista) && esVacio(descripcion_pista)">
                <div class="form-group">
					<label>ID de pista</label>
					<input class="form-control" type="text" id="id_pista" name="id_pista" value="<?= $this->datos->getIdPista()?>" readonly/>
				</div>
				<div class="form-group">
					<label>Tipo de pista</label>
					<input class="form-control" type="text" id="tipo_pista" name="tipo_pista" value="<?= $this->datos->getTipoPista();?>" maxlength="20" readonly/>
				</div>
				<div class="form-group">
					<label>Descripcion de pista</label>
					<input class="form-control" type="text" id="descripcion_pista" name="descripcion_pista" value="<?= $this->datos->getDescripcionPista();?>" maxlength="20" readonly/>
				</div>
				<button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar pista <i class="fas fa-trash-alt"></i></button>
			</form>		
		</div>
    </section>
<?php
        include '../Views/footer.php';//incluimos el footer
        }       
    }
?>