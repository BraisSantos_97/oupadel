<?php

class INSCRIPCIONCLASE_ADD {
//es el constructor de la clase USUARIO_ADD
	function __construct() {
		$this->render();//llamamos a l/llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render() {

		
		include_once '../Views/header.php';//incluimos la cabecera
		include '../Locales/Strings_SPANISH.php';
?>
<section   class="wow fadeIn">
    <div class="hero-container">
      <h1>Inscribirme en clase</h1>
        
			<form name="ADD"  action="../Controllers/INSCRIPCIONCLASE_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(login) && esVacio(id_clase)">
			
					  
						<input type="hidden" id="login" name="login" value="<?=$_SESSION['login']?>" maxlength="50" size="50" readonly/><br>
				
                        ID Clase<br>
						<input type="text" id="id_clase" name="id_clase" value="" maxlength="20" size="20"/><br>

							<button type="submit" name="action" value="ADD"><i class="fas fa-plus-circle"></i></button>
			 </form>
						
				
				
		</div>
    </section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>