<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class TABLA_CRUCES_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$datos) {
        $this->lista=$lista;
        $this->datos=$datos;
		$this->render($this->datos,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($datos,$lista) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->datos =$datos;
        $this->lista=$lista;
?>

<section>
    <h2 class="h1-responsive text-center">Cruces</h2>
    <div class="container">
    <?php
    if($datos != NULL){
    ?>
        <table class="table table-bordred table-striped">
            <?php
                if($_SESSION['login'] == "admin"){
            ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/CRUCES_CONTROLLER.php'>
                        <input type="hidden" name="id_grupo" value="<?= $datos[0][0]; ?>">
                        <input type="hidden" name="nombre_campeonato" value="<?= $datos[0][1]; ?>">
                        <input type="hidden" name="nombre_categoria" value="<?= $datos[0][2]; ?>">
                        <input type="hidden" name="nivel" value="<?= $datos[0][3]; ?>">
                        <button class="btn btn-default" type="submit" name="action" value="VER_ENFRENTAMIENTOS">Editar resultados del cuadro <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            <?php
                }
            ?>
            
               <?php
                if($_SESSION['login'] != "admin"){
            ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/CRUCES_CONTROLLER.php'>
                        <input type="hidden" name="id_grupo" value="<?= $datos[0][0]; ?>">
                        <input type="hidden" name="nombre_campeonato" value="<?= $datos[0][1]; ?>">
                        <input type="hidden" name="nombre_categoria" value="<?= $datos[0][2]; ?>">
                        <input type="hidden" name="nivel" value="<?= $datos[0][3]; ?>">
                        <button class="btn btn-default" type="submit" name="action" value="VER_ENFRENTAMIENTOS_DEPORTISTA">Ver cruces <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            <?php
                }
            ?>
            
            
            <tr>
                <th>Cuartos de final</th>
                <th>Semifinal</th>
                <th>Final</th>
                <th>Ganador</th>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[0][5] ?>
                </td>
                <td class="align-middle" rowspan="2">
                    <?= $datos[4][5] ?>
                </td>
                <td class="align-middle" rowspan="4">
                    <?= $datos[6][5] ?>
                </td>
                <td class="align-middle" rowspan="8">
                    <?= $datos[6][8] ?>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[0][6] ?>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[1][5] ?>
                </td>
                <td class="align-middle" rowspan="2">
                    <?= $datos[4][6] ?>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[1][6] ?>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[2][5] ?>
                </td>
                <td class="align-middle" rowspan="2">
                    <?= $datos[5][5] ?>
                </td>
                <td class="align-middle" rowspan="4">
                    <?= $datos[6][6] ?>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[2][6] ?>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[3][5] ?>
                </td>
                <td class="align-middle" rowspan="2">
                    <?= $datos[5][6] ?>
                </td>
            </tr>
            <tr>
                <td class="align-middle">
                    <?= $datos[3][6] ?>
                </td>
            </tr>
        </table>
    <?php
    } else {
    ?>
    <div class="text-center">
        <h2>Aún no hay datos</h2>
    </div>
    <?php
    }
    ?>
    
    <?php
        if($_SESSION['login'] == "admin"){
                    if($datos != NULL){
            ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/CRUCES_CONTROLLER.php'>
                        <input type="hidden" name="id_grupo" value="<?= $datos[0][0]; ?>">
                        <input type="hidden" name="nombre_campeonato" value="<?= $datos[0][1]; ?>">
                        <input type="hidden" name="nombre_categoria" value="<?= $datos[0][2]; ?>">
                        <input type="hidden" name="nivel" value="<?= $datos[0][3]; ?>">
                        <button class="btn btn-default" type="submit" name="action" value="METER_FECHA_HORA">Meter Fecha y Hora Enfrentamientos<i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>    
        <?php
                 }
        }
        ?>
        
    </div>
</section>

<?php
	include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

} //fin Login

?>