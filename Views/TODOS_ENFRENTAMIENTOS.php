<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class TODOS_ENFRENTAMIENTOS {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$grupo,$categoria,$campeonato,$nivel) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
		$this->render($this->valores,$this->lista,$this->grupo,$categoria,$campeonato,$nivel);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$grupo,$categoria,$campeonato,$nivel) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        
		?>

<section   class="section">
    <div class="container">
      <h1>Grupo <?php echo $this->grupo; ?> de <?php echo $this->campeonato; ?> <?php echo $this->categoria; ?> y Nivel <?php echo $this->nivel; ?> </h1>
        
        
        <table class="table table-bordred table-striped">
            		<tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
            			<th colspan="2">
            				Pareja 1	
            			</th>
            			<th colspan="2">
            				Pareja 2
            			</th>
                        <?php
                            if($_SESSION['login']=='admin'){

                        ?>
                        <th rowspan="2">Operaciones</th>
                        <?php
                                                          
                            }      
                        ?>        
                        
                        
                        
            		</tr>
          
       
            
            
        			<tr>
<?php
$nombreatributos=array("FECHA_ENFRENTAMIENTO"=>"Fecha","HORA_ENFRENTAMIENTO"=>"Hora","ID_PISTA"=>"Pista","RESULTADO"=>"Resultado","CAPITAN"=>"Capitán","LOGIN_PAREJA"=>"Login pareja");
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
                       <?php 
                        if($atributo != "ID_ENFRENTAMIENTO"){
                           echo $nombreatributos[$atributo];
                        }
                      ?>
						
					</th>
<?php
					}
                
                    
                    
 ?>
                    
                    
				</tr>
<?php
				$pareja1= "";
				$pareja2="";

				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php 
					$aux=0;
					$i=0;
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 	
					$aux++;
	                if ($aux==8) {
	                	$i++;
	                	$aux=0;
	                	$fila = mysqli_fetch_array( $this->valores );

	                	if($atributo == "ID_PAREJA"){
                        	$pareja2= $fila[$atributo];

                        	
                        }
	                }
                       if($atributo != "ID_ENFRENTAMIENTO"){
                            
                            if($fila[$atributo]==NULL){
                                echo "<i>No fijado<i>";
                            }
                           else{
                                echo "<b><i>".$fila[ $atributo ]."<i><b>";
                           }
                           
                        }
                       
                        
                        if($i==0){
                        	 if($atributo == "ID_PAREJA"){
                        	$pareja1= $fila[$atributo];

                        }
                        }
                       
                       

?>
					</td>
<?php
                        
					}
                    
                    
?>
            
                    <?php
                        if($_SESSION['login']=='admin'){
                            
                      
                    ?>
                    
                <td>

                <form action="../Controllers/ENFRENTAMIENTOS_CONTROLLER.php" method="get" style="display:inline" >
                            
                            <input type="hidden" name="id_enfrentamiento" id="id_enfrentamiento" value="<?php echo $fila['ID_ENFRENTAMIENTO'] ?>">
                           
							
						  <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                </form>
                       
				</td>
              
             <?php
                }
                ?>
            
            
                    		   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>