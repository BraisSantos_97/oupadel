<?php

//es la clase ADD de USUARIO que nos permite añadir un usuario
class CLASES_ADD {
//es el constructor de la clase USUARIO_ADD
	function __construct($escuela) {
		$this->render($escuela);//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render($escuela) {
		include_once '../Views/header.php';//incluimos la cabecera
?>
<section class="section">
    <div class="container">
      <h2>Añadir Clase</h2>
        
			<form name="ADD" action="../Controllers/CLASES_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(fecha_clase) && esVacio(hora_clase)">
				<div class="form-group">
                    <label>Fecha clase</label>
					<input class="form-control tcal" type="text" id="fecha_clase" name="fecha_clase" maxlength="20"/>
				</div>
				<div class="form-group">
                        <label>Hora clase</label>
						<select class="custom-select"  name="hora_clase" id="hora_clase"  placeholder="Hora">
                            <option value="10:00">10:00</option>
                            <option value="11:30">11:30</option>
                            <option value="13:00">13:00</option>
                            <option value="14:30">14:30</option>
                            <option value="16:00">16:00</option>
                            <option value="17:30">17:30</option>
                            <option value="19:00">19:00</option>
                            <option value="20:30">20:30</option>
                     
                         </select>
				</div>	
				<div class="form-group">	
						<label>Nombre escuela</label>
						<input class="form-control" type="text" id="nombre_escuela" name="nombre_escuela" value="<?php echo $escuela ?>" maxlength="50" readonly/>
				</div>
						<button class="btn btn-default" type="submit" name="action" value="ADD">Añadir <i class="fas fa-plus-circle"></i></button>
			 </form>
						
					
				
		</div>
    </section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>