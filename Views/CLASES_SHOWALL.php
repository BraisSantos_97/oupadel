<?php

class CLASES_SHOWALL {
	function __construct($lista,$valores,$escuela) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->escuela=$escuela;
		$this->render($this->valores,$lista,$escuela);
	}
	function render($valores,$lista,$escuela) {
        
		include_once '../Views/header.php';
        $this->valores =$valores;
        $this->lista=$lista;
        
		?>


<section class="wow fadeIn">
    <div class="container">
      <h2>Clases de la escuela</h2>
        
        
        <table class="table table-bordred table-striped">
            
            <?php
                if($_SESSION['login'] == 'admin'){
             ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/CLASES_CONTROLLER.php'>
                        <input type="hidden" name="nombre_escuela" value="<?php echo $this->escuela;?>">
                        <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir clase <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            
            
            <?php
                }
            ?>
       
            
            
        			<tr>
<?php
$nombreatributo=array("ID_CLASE"=>"Clase","FECHA_CLASE"=>"Fecha","HORA_CLASE"=>"Hora","NOMBRE_ESCUELA"=>"Escuela","ID_PISTA"=>"Pista");
				foreach ( $lista as $atributo ) { 
?>
					<th>
						<?php echo $nombreatributo[$atributo]; ?>
					</th>
<?php
					}
    
                              
 ?>
            <?php
                if($_SESSION['login'] =='admin'){ 
            ?>
                    <th>Opciones</th>
            <?php
                }
            ?>
             <?php    
                        if($_SESSION['login']!='admin'){
                            
                        
                    ?>
                        <th>Inscribirse en Clase </th>
                        
                    <?php
                        }
                    ?>
           
                    <!-- <th>Categoria</th> -->
				</tr>
<?php

				while ( $fila = mysqli_fetch_array( $this->valores ) ) { 

?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { 
?>
					<td>
<?php 
                             echo $fila[ $atributo ];
                       
                       

?>
					</td>
<?php
					}
                    if($_SESSION['login'] =='admin'){
                        
                    
?>
                    <td>
						<form action="../Controllers/CLASES_CONTROLLER.php" method="get" style="display:inline" >
				    <input type="hidden" name="id_clase" value="<?php echo $fila['ID_CLASE']; ?>">
								
				    
				        <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>


                            </form>
                        
					</td>
    <?php
        }              
    ?>
<?php



if($_SESSION['login']!='admin'){
                
     ?>                   
                    
                    
    <td>
        <form action="../Controllers/INSCRIPCIONCLASE_CONTROLLER.php">
                <input type="hidden" name ="id_clase" id="id_clase" value="<?php echo $fila['ID_CLASE']; ?>" >
               <input type="hidden" name="fecha_clase" value="<?php echo $fila['FECHA_CLASE']; ?>">
               <input type="hidden" name="hora_clase" value="<?php echo $fila['HORA_CLASE']; ?>">
               <input type="hidden" name="nombre_escuela" value="<?php echo $fila['NOMBRE_ESCUELA']; ?>">
                       <button class="btn btn-default" type="submit" name="action" value="ADD"><i class="fas fa-plus-circle"></i></button>
            </form>
                   
    </td>
                    
       <?php
                }  
                
            
    ?>  
                    
                    
					   
				</tr>
<?php
				}
        
?>
        </table>
    </div>
  </section>
 

<?php
		include '../Views/footer.php';
	} 

	} 

?>