<?php

//es la clase DELETE de USUARIO que nos permite borrar un usuario
class PARTIDO_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($valores,$partido) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        
		$this->render( $this->valores,$partido);//llamamos a la funci�n render donde se mostrar� el formulario DELETE con los campos correspondientes
	}
//funcion que mostrar� el formulario DELETE con los campos correspondientes
	function render( $valores,$partido) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        $this->partido=$partido;
		include_once '../Views/header.php';//incluimos la cabecera
        
?>
		<section   class="section">
    <div class="container">
      <h2>Borrar Partido <?php echo $this->partido ?></h2>
        
        <form name="DELETE" action="../Controllers/PARTIDO_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_partido) && esVacio(fecha_partido) && esVacio(hora_partido) && esVacio(id_pista)">
            <div class="form-group">
                <label>ID Partido</label>
                <input class="form-control" type="text" id="id_partido" name="id_partido" value="<?php echo $this->valores['ID_PARTIDO']?>" maxlength="10" readonly/>
            </div>
            <div class="form-group">
                <label>Fecha Partido</label>
                <input class="form-control tcal" type="text" id="fecha_partido" name="fecha_partido" value="<?php echo $this->valores['FECHA_PARTIDO']?>" maxlength="20" readonly />
            </div>
            <div class="form-group">
                <label>Hora Partido</label>
                <select class="custom-select" name="hora_partido" id="hora_partido" readonly>
                    <option value="<?php echo $this->valores['HORA_PARTIDO']?>" selected><?php echo $this->valores['HORA_PARTIDO']?></option>
                </select>
            </div>
            <div class="form-group">
                <label>Pista</label>
                <input class="form-control" type="int" id="id_pista" name="id_pista" value="<?php echo $this->valores['ID_PISTA']?>"  maxlength="10" size="10" readonly />
            </div>
            <button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar partido <i class="fas fa-trash-alt"></i></button>
        </form>
						
					
				
		</div>
    </section>
<?php
        include '../Views/footer.php';//incluimos el footer
            }
		
                
     }
        
	


?>