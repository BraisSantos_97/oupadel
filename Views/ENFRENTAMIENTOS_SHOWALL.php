<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class ENFRENTAMIENTOS_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$grupo,$categoria,$campeonato,$nivel) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
		$this->render($this->valores,$this->lista,$this->grupo,$categoria,$campeonato,$nivel);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$grupo,$categoria,$campeonato,$nivel) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        
		?>

<section class="section">
    <div class="container">
      <h2>Grupo <?php echo $this->grupo; ?> de <?php echo $this->campeonato; ?> <?php echo $this->categoria; ?> Nivel <?php echo $this->nivel; ?> </h2>
        
        
        <table class="table table-bordred table-striped">
					<tr>
            			<th colspan="3" style="border:0px">
            				
            			</th>
            			<th colspan="3">
            				PAREJA1	
            			</th>
            			<th colspan="3">
            				PAREJA2
            			</th>
            			<th style="border:0px">
            			
            			</th>

            		</tr>
          
       
            
            
        			<tr>
<?php
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
					$nombresatributos = array("ID_ENFRENTAMIENTO"=>"Enfrentamiento", "FECHA_ENFRENTAMIENTO"=>"Fecha", "HORA_ENFRENTAMIENTO"=>"Hora", "ID_PAREJA"=>"ID Pareja", "CAPITAN"=>"Capitán", "LOGIN_PAREJA"=>"Login pareja", "Opciones"=>"Opciones");
?>
					<th>
						<?= $nombresatributos[$atributo]; ?>
					</th>
<?php
					}
                
                    
                    
 ?>
                    <th>Resultado</th>
                    <!-- <th>Opciones</th> -->
				</tr>
<?php
				$pareja1= "";
				$pareja2="";

				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php 
					$aux=0;
					$i=0;
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 	
					$aux++;
	                if ($aux==7) {
	                	$i++;
	                	$aux=0;
	                	$fila = mysqli_fetch_array( $this->valores );

	                	if($atributo == "ID_PAREJA"){
                        	$pareja2= $fila[$atributo];

                        	
                        }
	                }
                        
                       echo $fila[ $atributo ];
                        
                        if($i==0){
                        	 if($atributo == "ID_PAREJA"){
                        	$pareja1= $fila[$atributo];

                        }
                        }
                       
                       

?>
					</td>
<?php
                        
					}
                    
?>
                
				<td>




					<form action="../Controllers/ENFRENTAMIENTOS_CONTROLLER.php" method="get" style="display:inline" onsubmit="return esVacio(resultado) && comprobarExpresionRegular(resultado,/([0-7]-[0-7]\/[0-7]-[0-7]\/[0-7]-[0-7])|([0-7]-[0-7]\/[0-7]-[0-7])/)">
                            
                            <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                            <input type="hidden" name="id_enfrentamiento" value="<?php echo $fila['ID_ENFRENTAMIENTO']; ?>"> 
                            <input type="hidden" name="pareja1" value="<?php echo $pareja1 ?>">
                            <input type="hidden" name="pareja2" value="<?php echo $pareja2 ?>">
                            <input type="hidden" name="nivel" value="<?php echo $this->nivel; ?>"> 
                            
                            <input type="text" name="resultado" id="resultado" placeholder="X-Y/X-Y/X-Y">
                        
                            
							
						  <button type="submit" name="action" value="EDIT" ><img src="../Views/img/edit.jpg" alt="ADD_RESULTADO" width="40" height="40" /></button>
                </form>
				</td>
                    		   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>