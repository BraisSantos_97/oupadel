<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class CRUCES_FECHA_HORA {
	//es el constructor de la clase Login
	function __construct($lista,$valores) {
        $this->lista=$lista;
		$this->valores =$valores;
		$this->render($this->lista,$this->valores);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($lista,$valores) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
		$this->lista=$lista;
		?>

<section class="section">
    <div class="container">
      <h2>Grupo</h2>
        <table class="table table-bordred table-striped">
        			<tr>
<?php
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
						<?= $atributo; ?>
					</th>
<?php
					}
                
                    
                    
 ?>
                    <th>Fecha y Hora</th>
                    <!-- <th>Opciones</th> -->
				</tr>
<?php

				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos
?>
				<tr>
<?php 

					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 	
                        
                       echo $fila[ $atributo ];
?>
					</td>
<?php
                        
					}
                    
?>
                
				<td>
					<form action="../Controllers/CRUCES_CONTROLLER.php" method="get" style="display:inline" onsubmit="return esVacio(resultado) && comprobarExpresionRegular(resultado,/([0-7]-[0-7]\/[0-7]-[0-7]\/[0-7]-[0-7])|([0-7]-[0-7]\/[0-7]-[0-7])/)">
                            
						<input type="hidden" name="id_grupo" id="id_grupo" value="<?= $fila['ID_GRUPO']; ?>">
						<input type="hidden" name="nombre_campeonato" value="<?= $fila['NOMBRE_CAMPEONATO'];?>">
						<input type="hidden" name="nombre_categoria" value="<?= $fila['NOMBRE_CATEGORIA']; ?>">
						<input type="hidden" name="id_partido" value="<?= $fila['ID_PARTIDO']; ?>"> 
						<input type="hidden" name="id_pareja_1" value="<?= $fila['ID_PAREJA_1']; ?>">
						<input type="hidden" name="id_pareja_2" value="<?= $fila['ID_PAREJA_2']; ?>">
						<input type="hidden" name="nivel" value="<?= $fila['NIVEL']; ?>">
						<input type="hidden" name="fase" value="<?= $fila['FASE']; ?>"> 
                        
                        <input type="text"   class="tcal"  name="fecha" placeholder="Fecha" readonly >
                            <!--<input type="text" name="hora_propuesta" placeholder="Hora" >-->
                            <select  name="hora" id="hora"  placeholder="Hora">
                            <option value="10:00">10:00</option>
                            <option value="11:30">11:30</option>
                            <option value="13:00">13:00</option>
                            <option value="14:30">14:30</option>
                            <option value="16:00">16:00</option>
                            <option value="17:30">17:30</option>
                            <option value="19:00">19:00</option>
                            <option value="20:30">20:30</option>
                        </select>
				
					
						<button class="btn btn-default" type="submit" name="action" value="EDIT2" style="display:inline"><i class="fas fa-pencil-alt"></i></button>
                	</form>
				</td>
                    		   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>