<?php
class RESERVA_ADD2 {
    //es el constructor de la clase USUARIO_ADD
    function __construct($fecha,$tabla,$horas,$pistas) {
        $this->render($fecha,$tabla,$horas,$pistas);//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
    }
    //funcion que  mostrará el formulario ADD con los campos correspondientes
    function render($fecha,$tabla,$horas,$pistas) {
        include_once '../Views/header.php';//incluimos la cabecera
        $this->fecha=$fecha;
        $this->tabla=$tabla;   
        $this->horas=$horas;
        $this->pistas=$pistas;
?>
    <section   class="section">
      <div class="container">
        <h1>Reservar pista</h1>
        <table id="horario">
          <tr>
            <th></th>
            <?php
            foreach ($pistas as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla RESERVA
            ?>
            <th>
            <?= "Pista ".$atributo ?>
            </th>
            <?php
            }
            ?>
          </tr>
          <?php
          $numHoras = count($horas);
          $numPistas = count($pistas);
          
          for($i=0;$i < $numHoras;$i++){
          ?>
          <tr>
            <td>
              <p><?= $horas[$i]?></p>
            </td>
            <?php 
            for($j=1;$j<$numPistas+1;$j++){
            ?>
            <td>
              <?php
              if($tabla!=NULL){
                if(array_key_exists($j,$tabla)){//Si la pista existe en la tabla de reservas
                  for ($t=0; $t < count($tabla[$j]);$t++) {
                    if($horas[$i]==$tabla[$j][$t]){
                    ?>
                      <p class="ocupado">Ocupado</p>
                    <?php  
                    }
                  }              
                }
              } 
              ?>
            </td>
            <?php
            }
            ?>
          </tr>
          <?php
          }
          ?>
        </table>

        <div class="container">
          <form name="ADD" action="../Controllers/RESERVA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_pista) && esVacio(hora) && esVacio(login)">	
            <div class="form-group">
              <label>Pista</label>
              <select class="custom-select" id="id_pista" name="id_pista">
                <?php
                foreach($pistas as $pista){
                ?>
                <option value="<?=$pista?>">Pista <?= $pista?></option>
                <?php
                }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>Hora</label>
              <select class="custom-select" id="hora" name="hora">
                <?php
                foreach($horas as $hora){
                ?>
                <option value="<?=$hora?>"><?= $hora?></option>
                <?php
                }
                ?>
              </select>
            </div>

            <input type="hidden" id="fecha" name="fecha" value="<?= $fecha?>" maxlength="20" />
            
            <div class="form-group">
              <?php if($_SESSION['login'] == 'admin'){ ?>
              <label>Login</label>
              <input class="form-control" type="text" id="login" name="login" value="<?=$_REQUEST['login']?>" maxlength="50" readonly/>
              <?php } else {?>
              <input type="hidden" id="login" name="login" value="<?=$_SESSION['login']?>" maxlength="50"/>
              <?php } ?>
            </div>
            
            <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir reserva <i class="fas fa-plus-circle"></i></button>
          </form>
        </div>		
      </div>
    </section>
<?php
		include '../Views/footer.php';//incluimos el footer
	}
}
?>