<?php
class ESCUELA_SHOWALL {
	function __construct($lista,$valores) {
        $this->lista=$lista;
        $this->valores =$valores;
		$this->render($this->valores,$lista);
	}
	function render($valores,$lista) {
        
		include_once '../Views/header.php';
        $this->valores =$valores;
        $this->lista=$lista;
        
		?>


<section class="section">
    <div class="container">
      <h1>Escuelas del Club</h1>
        
        
        <table class="table table-bordred table-striped">
            
            <?php
                if($_SESSION['login'] == 'admin'){
             ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/ESCUELA_CONTROLLER.php'>
                        <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir escuela <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            
            
            <?php
                }
            ?>
       
            
            
        			<tr>
<?php
$nombreatributos = array("NOMBRE_ESCUELA"=>"Nombre","DESCRIPCION"=>"Descripcion","TIPO_CLASE"=>"Tipo");
				foreach ( $lista as $atributo ) { 
?>
					<th>
						<?php echo $nombreatributos[$atributo]; ?>
					</th>
<?php
					}
    
                              
 ?>
            <?php
                if($_SESSION['login'] =='admin'){ 
            ?>
                    <th>Opciones</th>
            <?php
                }
            ?>
            <th>ACCIONES</th>
                    <!-- <th>Categoria</th> -->
				</tr>
<?php
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { 

?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { 
?>
					<td>
<?php 
	               if($atributo =="DESCRIPCION"){
                       ?>
                        
                       <textarea class="form-control" rows="4" cols="50" style = "resize:none" readonly><?php echo $fila[ $atributo ]; ?></textarea>

                        <?php
                   }
                        else{
                             echo $fila[ $atributo ];
                        }
                        
                       

?>
					</td>
<?php
					}
                    if($_SESSION['login'] =='admin'){
                        
                    
?>
                    <td>
						<form action="../Controllers/ESCUELA_CONTROLLER.php" method="get" style="display:inline" >
				    <input type="hidden" name="nombre_escuela" value="<?php echo $fila['NOMBRE_ESCUELA']; ?>">
								
				    
				        <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>


                            </form>
                        
					</td>
    <?php
        }              
    ?>

            <td>
            <form action="../Controllers/CLASES_CONTROLLER.php" method="get" style="display:inline" >
            <input type="hidden" name="nombre_escuela" value="<?php echo $fila['NOMBRE_ESCUELA']; ?>">
                
                <button class="btn btn-default" type="submit" name="action" value="" ><i class="fas fa-eye"></i></button>


                            </form>
                        
          </td>

                     <!--  <td>
                    <a href="../Controllers/ESCUELA_CONTROLLER.php"><img src="../Views/img/flechah.jpg" height="40"  width="40" ></a>
                  </td> -->
                    
                    
                    
					   
				</tr>
<?php
				}
        
?>
        </table>
    </div>
  </section>
 

<?php
		include '../Views/footer.php';
	} 

	} 

?>