<?php
class CONTENIDO_ADD {
    //es el constructor de la clase USUARIO_ADD
    function __construct() {
        $this->render();//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
    }
    //funcion que  mostrará el formulario ADD con los campos correspondientes
    function render() {
        include_once '../Views/header.php';//incluimos la cabecera
?>
        <section class="wow fadeIn">
            <div class="container">
                <h3>Añadir contenido</h3>
                <form name="ADD" action="../Controllers/CONTENIDO_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(titulo) && comprobarExpresionRegular(titulo,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(texto) && comprobarExpresionRegular(texto,/^([A-Za-zá-úÁ-Ú]+\s*)+$/)">	
                    <div class="form-group">
                        <label>Titulo</label>
                        <input class="form-control" type="text" id="titulo" name="titulo" maxlength="50" required />
                    </div>

                    <div class="form-group">
                        <label>Texto</label>
                        <input class="form-control" type="text" id="texto" name="texto" maxlength="50"  required/>
                    </div>

                    <div class="form-group">
                        <label>Fecha de publicación</label>
                        <input class="form-control" type="text" id="fecha_publicacion" name="fecha_publicacion" value="<?= date("d/m/Y H:i",time()) ?>" maxlength="255" />
                    </div>
                           
                    <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir  <i class="fas fa-plus-circle"></i></button>
                </form>		
            </div>
        </section>
<?php
		include '../Views/footer.php';//incluimos el footer
	}
}
?>