<?php 
class RESERVA_ADD { 
    //Constructor de la clase RESERVA_ADD 
    function __construct() { 
        $this->render();//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes 
    } 
    //funcion que  mostrará el formulario ADD con los campos correspondientes
    function render() { 
        include_once '../Views/header.php';//incluimos la cabecera 
?>
    <section   class="section"> 
        <div class="container"> 
            <h2>Reservar pista</h2> 
            <form name="ADD" action="../Controllers/RESERVA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(login)">	 
                <div class="form-group">
                    <label>Fecha</label> 
                    <input class="form-control" id="dateReserva" type="date" id="date" name="date" maxlength="10" require min="<?=$minDate = date('Y-m-d')?>" max="<?= $maxDate = date('Y-m-d', strtotime($minDate. ' + 7 days'))?>" /> 
                <div>
                <div class="form-group">
                    <?php if($_SESSION['login'] == 'admin'){ ?> 
                    <label>Login</label> 
                    <input class="form-control" type="text" id="login" name="login" maxlength="50"/> 
                    <?php } else {?> 
                    <input type="hidden" id="login" name="login" value="<?=$_SESSION['login']?>" maxlength="50"/> 
                    <?php } ?> 
                <div>
                <br>
                <button class="btn btn-default" type="submit" name="action" value="ADD">Confirmar <i class="fas fa-plus-circle"></i></button> 
            </form>		 
        </div> 
    </section> 
<?php 
		include '../Views/footer.php';//incluimos el footer 
	} 
} 
?>