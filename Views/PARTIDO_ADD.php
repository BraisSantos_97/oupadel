<?php

//es la clase ADD de USUARIO que nos permite añadir un usuario
class PARTIDO_ADD {
//es el constructor de la clase USUARIO_ADD
	function __construct() {
		$this->render();//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render() {
		include_once '../Views/header.php';//incluimos la cabecera
?>
<section   class="section">
    <div class="container">
        <h2>Añadir Partido</h2>
        <form name="ADD" action="../Controllers/PARTIDO_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_partido)  && comprobarExpresionRegular(id_partido,/[0-9]+/) && esVacio(fecha_partido) && esVacio(hora_partido)">
            <div class="form-group">
                <label>ID Partido</label>
                <input class="form-control" type="text" id="id_partido" name="id_partido" maxlength="10"  /><br>
            </div>
            <div class="form-group">
                <label>Fecha Partido</label>
                <input class="form-control tcal" type="text" id="fecha_partido" name="fecha_partido" maxlength="20" readonly /><br>
            </div>
            <div class="form-group">   
                <label>Hora Partido</label>
                <select class="custom-select" name="hora_partido" id="hora_partido"  placeholder="Hora">
                    <option value="10:00">10:00</option>
                    <option value="11:30">11:30</option>
                    <option value="13:00">13:00</option>
                    <option value="14:30">14:30</option>
                    <option value="16:00">16:00</option>
                    <option value="17:30">17:30</option>
                    <option value="19:00">19:00</option>
                    <option value="20:30">20:30</option>        
                </select>
            </div>
            <input type="hidden" id="id_pista" name="id_pista"/>
            <button class="btn btn-default" type="submit" name="action" value="ADD">Promocionar partido<i class="fas fa-plus-circle"></i></button>
        </form>
	</div>
</section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>