<?php
include_once '../Functions/Authentication.php';
//header('Content-Type: text/html; charset=ISO-8859-1');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 
  <title>OuPadel</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="../Views/js/nuestro.js"></script>

  <!-- Librer�as de JQuery -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link href="../Views/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="../Views/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="../Views/lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
  <link href="../Views/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../Views/lib/animate/animate.min.css" rel="stylesheet">
  <link href="../Views/lib/modal-video/css/modal-video.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="../Views/css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="../Views/tcal/tcal.css" hreflang="es">
  <script language="JavaScript" type="text/javascript" src="../Views/tcal/tcal.js"></script>
  <script type="text/javascript" src="../Views/js/validaciones.js"></script>
     
    
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link href ="../Views/css/estilos.css" rel=stylesheet>
    
  <?php include '../Views/tcal/validaciones.js' ?>

</head>

<body>

  <header id="header" class="header header-hide">
    <div class="container">

      <div id="logo">
        <h3><a href="#body" class="scrollto"><span>OuPadel</span> <?php if (IsAuthenticated()){ //miramos si el usuario esta autenticado ?><?php echo 'Usuario' . ': ' . $_SESSION['login']; ?><?php }  ?></a></h3>
      </div>
      
      <div id="navbar" class="navbar">
        <?php
        if (IsAuthenticated()){   
        ?>
        <a href="../Controllers/index_controller.php">Inicio</a>
        <div class="dropdown">
          <button class="dropbtn">Campeonato
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <a href="../Controllers/CAMPEONATO_CONTROLLER.php">Todos los campeonatos</a>
            <a href="../Controllers/PAREJA_CONTROLLER.php?action=VER_PAREJAS">Ver parejas</a>
          </div>
        </div>
         <div class="dropdown">
          <button class="dropbtn">Escuelas deportivas
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <a href="../Controllers/ESCUELA_CONTROLLER.php">Todas las escuelas deportivas</a>
             <?php
        if($_SESSION['login'] !=='admin'){
        ?>
            <a href="../Controllers/INSCRIPCIONCLASE_CONTROLLER.php?action=INSCRIBIRSE">Mis clases</a>
            <?php
          }
          ?>
           <?php
          if($_SESSION['login'] =='admin'){
        ?>
            <a href="../Controllers/INSCRIPCIONCLASE_CONTROLLER.php?action=INSCRIBIRSE">Todas las clases</a>
            <?php
          }
          ?>
            
          </div>
        </div>
        <div class="dropdown">
          <button class="dropbtn">Partidos Promocionados
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <?php
            if($_SESSION['login'] =='admin'){
            ?>
            <a href="../Controllers/INSCRIPCION_CONTROLLER.php?action=INSCRIBIRSE">Deportistas apuntados a partidos</a>
            <?php
            } else {
            ?>
            <a href="../Controllers/INSCRIPCION_CONTROLLER.php?action=INSCRIBIRSE">Mis Partidos</a>
            <?php
            }
            ?>
            <a href="../Controllers/PARTIDO_CONTROLLER.php">Partidos disponibles</a>
          </div>
        </div>
        <?php
        if($_SESSION['login'] =='admin'){
        ?>
        <div class="dropdown">
          <button class="dropbtn">Pistas
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <a href="../Controllers/PISTA_CONTROLLER.php">Pistas</a>
            <a href="../Controllers/PISTA_CONTROLLER.php?action=ADD">Anadir pista</a>
          </div>
        </div>
        <?php
        } else {
        ?>
        <div class="dropdown">
          <button class="dropbtn">Reservas
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <a href="../Controllers/RESERVA_CONTROLLER.php">Mis reservas</a>
            <a href="../Controllers/RESERVA_CONTROLLER.php?action=ADD">Reservar pistas</a>
          </div>
        </div>
        <?php
        }
        ?>
        <?php
        }
        ?>      
   
        <?php
        if (IsAuthenticated()){ //miramos si el usuario esta autenticado
        ?>
        <a href="../Functions/Desconectar.php" style="text-decoration:none"> <i class="fas fa-sign-out-alt"></i></a>
        <?php
        } 
        ?>
      </div><!-- #navbar -->

      <?php
        if (IsAuthenticated()){ //miramos si el usuario esta autenticado
      ?>
      <div id="burger-menu">
        <a href="javascript:void(0);" class="icon" onclick="navbar()">
          <i class="fa fa-bars"></i>
        </a>
      </div>
      <div id="vnavbar" class="vnavbar">
        <div class="vbar" id="vcampeonato">
          <h4>Campeonatos<i class="fas fa-sort-down"></i></h4>
          <a href="../Controllers/CAMPEONATO_CONTROLLER.php">Todos los campeonatos</a>
          <a href="../Controllers/PAREJA_CONTROLLER.php?action=VER_PAREJAS">Ver parejas</a>
        </div>
        <div class="vbar" id="vpartidopromocionado">
          <h4>Partidos promocionados<i class="fas fa-sort-down"></i></h4>
          <a href="../Controllers/INSCRIPCION_CONTROLLER.php?action=INSCRIBIRSE">Partidos disponibles</a>
          <a href="../Controllers/PARTIDO_CONTROLLER.php">Todos los partidos</a>
        </div>
        <div class="vbar" id="vpistas">
        <?php
        if($_SESSION['login'] =='admin'){
        ?>
          <h4>Pistas<i class="fas fa-sort-down"></i></h4>
          <a href="../Controllers/PISTA_CONTROLLER.php">Pistas</a>
          <a href="../Controllers/PISTA_CONTROLLER.php?action=ADD">A�adir pista</a>
        <?php
        } else {
        ?>
          <h4>Reservas<i class="fas fa-sort-down"></i></h4>
          <a href="../Controllers/RESERVA_CONTROLLER.php">Mis reservas</a>
          <a href="../Controllers/RESERVA_CONTROLLER.php?action=ADD">Reservar pistas</a>
        <?php
        }
        ?>
        </div>
      </div>
      <?php
        } 
      ?>
        
    </div> <!--container-->
      
  </header><!-- #header -->

