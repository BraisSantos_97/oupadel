<?php
class HORARIO_ADD {
    //es el constructor de la clase USUARIO_ADD
    function __construct() {
        $this->render();//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
    }
    //funcion que  mostrará el formulario ADD con los campos correspondientes
    function render() {
        include_once '../Views/header.php';//incluimos la cabecera
?>
        <section class="wow fadeIn">
            <div class="container">
            <h2>Añadir horario</h2>
                <form name="ADD" action="../Controllers/HORARIO_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(fecha) && esVacio(hora) && esVacio(dia)">   
                    <div class="form-group">
                        <label>Fecha</label>
                        <input class="form-control" type="date" id="fecha" name="fecha" value="" maxlength="50" size="50" />    
                    </div>
                    
                    <div class="form-group">
                        <label>Horario</label>
                        <select class="custom-select" name="hora" id="hora"  placeholder="Hora">
                            <option value="10:00">10:00</option>
                            <option value="11:30">11:30</option>
                            <option value="13:00">13:00</option>
                            <option value="14:30">14:30</option>
                            <option value="16:00">16:00</option>
                            <option value="17:30">17:30</option>
                            <option value="19:00">19:00</option>
                            <option value="20:30">20:30</option> 
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Dia</label> 
                        <select class="custom-select" name="dia" id="dia"  placeholder="Hora">
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                        </select>
                    </div>
                            
                    <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir horario <i class="fas fa-plus-circle"></i></button>
                </form>     
            </div>
        </section>
<?php
        include '../Views/footer.php';//incluimos el footer
    }
}
?>