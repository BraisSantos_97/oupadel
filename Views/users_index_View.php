<?php

//es la clase del inicio de la página
class Index {

    //es el constructor
	function __construct(){
		
		$this->render();//llamamos a la función para que muestre la vista
	}

    //función que muestra la vista
	function render(){
	
		include '../Views/Header.php';//incluimos la cabecera
		?>
		<div class="container">
			<div class="form-group main-links">
				<a href="../Controllers/CAMPEONATO_CONTROLLER.php"><h4>Campeonatos</h4></a>
				<textarea class="form-control">Entra aqui para gestionar todo lo referente a campeonatos, como las categorías, los grupos, la clasificación, etc.</textarea>
			</div>

			<div class="form-group main-links">
				<a href="../Controllers/RESERVA_CONTROLLER.php"><h4>Reservas</h4></a>
				<textarea class="form-control">Aqui podrás gestionar las reservas de pistas realizadas por los usuarios, así como reservarlas en su lugar. También podrás cancelar las reservas.</textarea>
			</div>

			<div class="form-group main-links">
				<a href="../Controllers/CONTENIDO_CONTROLLER.php"><h4>Contenido</h4></a>
				<textarea class="form-control">Esta es la sección de contenido. Esta sección está destinada a crear noticias (como la inauguración de una nueva pista, por ejemplo) para que los usuarios del club estén al tanto de todas las novedades. Estos usuarios serán avisados por correo tras la publicación de una noticia.</textarea>
			</div>
			<?php
            if($_SESSION['login'] =='admin'){     
            ?>
			<div class="form-group main-links">
				<a href="../Controllers/HORARIO_CONTROLLER.php"><h4>Horarios</h4></a>
				<textarea class="form-control">Aqui podrás gestionar los horarios del club de cada dia de manera individual. También podrás borrar un horario en caso de que el club necesite cerrar en esa franja.</textarea>
			</div>
			<?php
			}
			?>
		</div>
		
		<?php
		include '../Views/Footer.php';//incluimos el footer
	}

}

?>