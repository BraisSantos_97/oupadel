<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class MIS_ENFRENTAMIENTOS_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$grupo,$categoria,$campeonato,$nivel) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
		$this->render($this->valores,$this->lista,$this->grupo,$categoria,$campeonato,$nivel);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$grupo,$categoria,$campeonato,$nivel) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        
		?>

<section class="section">
    <div class="container">
      <h3>Grupo: <?php echo $this->grupo; ?> de Campeonato: <?php echo $this->campeonato; ?>, <?php echo $this->categoria; ?>, nivel: <?php echo $this->nivel; ?> </h3>
        
        
        <table class="table table-bordred table-striped">
            	
            
            
        			<tr>
<?php
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
						<?php echo $atributo; ?>
					</th>
<?php
					}
                
                    
                    
 ?>
                  
                    <th>Proponer Fechas/Aceptar Fechas</th>
                    <!-- <th>Opciones</th> -->
				</tr>
<?php
				

				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php 
					
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 	
				
                       echo $fila[ $atributo ];
                        

?>
					</td>
<?php
                        
					}
                    
?>
                
			
                <td>
                <form action="../Controllers/PROPONER_FECHA_CONTROLLER.php" method="get" style="display:inline" >
                            
                            <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
                            <input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                            <input type="hidden" name="nivel" value="<?php echo $this->nivel ?>">
                                                                   
                    <button type="submit" name="action" value="PROPONER_FECHA" ><i class="fas fa-pencil-alt"></i></button>
                </form>
                    
                </td>
                    		   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>