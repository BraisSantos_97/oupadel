<?php


//Es la clase Login que nos permite mostrar la vista para logearse
class MOSTRAR_PAREJAS {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$grupo,$categoria,$campeonato,$nivel) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
		$this->render($this->valores,$this->lista,$this->grupo,$categoria,$campeonato,$nivel);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$grupo,$categoria,$campeonato,$nivel) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->campeonato = $campeonato;
        $this->nivel=$nivel;
        
        
		?>

<section   class="section">
    <div class="container">
      <h2>Añadir parejas del grupo <?php echo $this->grupo ?> de <?php echo $this->campeonato; echo $this->categoria;  echo $this->nivel; ?></h2>
        
        
        <table class="table table-bordred table-striped">
            
          
       
            
            
        			<tr>
<?php
$nombreatributos = array("ID_PAREJA"=>"ID Pareja","CAPITAN"=>"Capitán","LOGIN_PAREJA"=>"Login de la pareja","NOMBRE_CATEGORIA"=>"Categoría","NOMBRE_CAMPEONATO"=>"Campeonato","NIVEL"=>"Nivel","ID_GRUPO"=>"Grupo","NOMBRE_CATEGORIA_GRUPO"=>"NOMBRE_CATEGORIA_GRUPO","NOMBRE_CAMPEONATO_GRUPO"=>"NOMBRE_CAMPEONATO_GRUPO");
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
						<?php echo $nombreatributos[$atributo]; ?>
					</th>
<?php
					}
                
                    
                    
 ?>
            <th>Añadir al grupo</th>
                        
				</tr>
<?php
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 
	                
                       echo $fila[ $atributo ];
                        
                        
                       

?>
					</td>
<?php
                        
					}
                    
?>
                    <td>
						<form action="../Controllers/PAREJA_CONTROLLER.php" method="get" style="display:inline" >
                            
                            <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                            <input type="hidden" name="id_pareja" value="<?php echo $fila['ID_PAREJA']; ?>">
                            <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
                            
							
						  <button class="btn btn-default" type="submit" name="action" value="EDIT" ><i class="fas fa-pencil-alt"></i></button>
                        
                        </form>
                        
					</td>
    
                  

                    		   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

               

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>