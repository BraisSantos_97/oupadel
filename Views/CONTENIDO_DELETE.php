<?php
//es la clase DELETE de USUARIO que nos permite borrar un usuario
class CONTENIDO_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($datos,$pista) { 
		$this->datos = $datos;//pasamos los datos de cada uno de los campos
		$this->render($this->datos,$pista);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render($datos,$pista) { 
		$this->datos=$datos;//pasamos los datos de cada uno de los campos
		$this->pista=$pista;
		include_once '../Views/header.php';//incluimos la cabecera
?>
	<section class="wow fadeIn">
        <div class="container">
            <h2>Borrar contenido</h2>
			<form name="DELETE" action="../Controllers/CONTENIDO_CONTROLLER.php?action=DELETE" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_pista) && esVacio(tipo_pista) && esVacio(descripcion_pista)">
				<div class="form-group">
					<label>ID de contenido</label>
					<input class="form-control" type="text" id="id_contenido" name="id_contenido" value="<?= $this->datos->getIdContenido()?>" readonly/>
				</div>

				<div class="form-group">
					<label>Titulo</label>
					<input class="form-control" type="text" id="titulo" name="titulo" value="<?= $this->datos->getTitulo();?>" maxlength="20" size="20" readonly/>
				</div>

				<div class="form-group">
					<label>Texto</label>
					<textarea class="form-control" rows="5" id="texto" name="texto" placeholder="<?= $this->datos->getTexto()?>" readonly></textarea>
				</div>

				<div class="form-group">
					<label>Fecha de publicacion</label>
					<input class="form-control" type="text" id="fecha_publicacion" name="fecha_publicacion" value="<?= $this->datos->getFechaPublicacion();?>" maxlength="20" size="20" readonly/><br>
				</div>	
                <button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar</button>
			</form>		
		</div>
    </section>
<?php
        include '../Views/footer.php';//incluimos el footer
        }       
    }
?>