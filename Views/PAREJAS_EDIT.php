<?php

//es la clase ADD de USUARIO que nos permite añadir un usuario
class PAREJAS_EDIT {
//es el constructor de la clase USUARIO_ADD
	function __construct($grupo,$campeonato,$categoria,$id_pareja,$nivel) {
		$this->render($grupo,$campeonato,$categoria,$id_pareja,$nivel);//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render($grupo,$campeonato,$categoria,$id_pareja,$nivel){
		include_once '../Views/header.php';//incluimos la cabecera
        $this->campeonato = $campeonato;
        $this->categoria = $categoria;
        $this->grupo = $grupo;
        $this->id_pareja = $id_pareja;
        $this->nivel=$nivel;
        
?>
<section   class="wow fadeIn">
    <div class="container">
      <h3>¿Deseas insertar esta pareja en el grupo <?php echo $grupo; ?> del campeonato <?php echo $campeonato; ?> y Categoria: <?php echo $categoria; ?> y Nivel <?php echo $nivel; ?> ?</h3>
        
			<form name="EDIT" action="../Controllers/PAREJA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_grupo) && esVacio(nombre_campeonato) && esVacio(nombre_categoria) && esVacio(nivel)">
                        <input type="hidden"  name="id_pareja" id="id_pareja" value="<?php echo $this->id_pareja ?>">
				<div class="form-group">
                    <label>Número del grupo</label>
                        <input class="form-control" type="number" id="id_grupo" name="id_grupo" value="<?php echo $this->grupo ?>" readonly />
                </div>
				<div class="form-group">
					<label>Nombre Campeonato</label>
						<input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="<?php echo $this->campeonato ?>" maxlength="50" readonly />
				</div>
				<div class="form-group">
                        
					<label>Nombre Categoria</label>
						<input class="form-control" type="text" id="nombre_categoria" name="nombre_categoria" value="<?php echo $this->categoria ?>" maxlength="50" readonly />
                </div>
				<div class="form-group">
					<label>Nivel</label>
                        <input class="form-control" type="text" id="nivel" name="nivel" value="<?php echo $this->nivel ?>" maxlength="20" readonly />
				</div>
				<button class="btn btn-default" type="submit" name="action" value="EDIT">Editar <i class="fas fa-pencil-alt"></i></button>
			 </form>
						
					
				
		</div>
    </section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>