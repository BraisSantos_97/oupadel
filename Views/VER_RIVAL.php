<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class VER_RIVAL {
	//es el constructor de la clase Login
	function __construct($valores,$lista,$categoria,$campeonato,$nivel,$grupo,$enfrentamiento) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
         
		$this->render($this->valores,$this->lista,$categoria,$campeonato,$nivel,$this->grupo,$enfrentamiento);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$categoria,$campeonato,$nivel,$grupo,$enfrentamiento) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        $this->enfrentamiento = $enfrentamiento;
        
		?>

<section class="section">
    <div class="container">
        <h2>Grupo <?php echo $this->grupo; ?> de <?php echo $this->campeonato; ?> <?php echo $this->categoria; ?> Nivel <?php echo $nivel  ?></h2>
        <table class="table table-bordred table-striped">
            <tr>
                <?php
                foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
                    if($atributo != 'ID_PAREJA'){
                    ?>
                    <th>
                        <?php echo $atributo; ?>
                    </th>
                    <?php
                    }			
                }
                ?>
                <th>Propuestas del rival</th>
			</tr>
            <?php
			while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos
            ?>
			<tr>
            <?php
			foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario                    
                if($atributo != 'ID_PAREJA'){
                ?>
				<td>
                <?php     
                echo "<i><b>".$fila[ $atributo ]."<b><i>";
                ?>
				</td>
                <?php
                }            
			}
            ?>       
                <td>
                    <form action="../Controllers/PROPONER_FECHA_CONTROLLER.php"method="get" style="display:inline" >
                        <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                        <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
						<input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                        <input type="hidden" name="nivel" value="<?php echo $this->nivel ?>">
                        <input type="hidden" name="id_enfrentamiento" value="<?php echo $this->enfrentamiento ?>">
                        <input type="hidden" name="id_fecha_propuesta" value=""> 
                        <input type="hidden" name="id_pareja" value="<?php echo $fila['ID_PAREJA'] ?>">
                            
						<button type="submit" name="action" value="VER_PROPUESTAS" ><i class="fas fa-arrow-right"></i></button>
                    </form>
                </td>	   
			</tr>
            <?php   
            }
            ?>
        </table>
    </div>
</section><!-- #hero -->

                    

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>