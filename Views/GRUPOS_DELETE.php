<?php

//es la clase DELETE de USUARIO que nos permite borrar un usuario
class GRUPOS_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($valores,$id_grupo) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
		$this->render( $this->valores,$id_grupo);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render( $valores,$id_grupo) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        $this->id_grupo = $id_grupo;
		include_once '../Views/header.php';//incluimos la cabecera
?>
		<section class="section">
    <div class="container">
      	<h2>Borrar Grupo <?php echo $this->id_grupo; ?> </h2>
        
			<form name="DELETE" action="../Controllers/GRUPOS_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_grupo) && comprobarExpresionRegular(id_grupo,/[0-9]+/)&& esVacio(nombre_categoria) && comprobarExpresionRegular(nombre_categoria,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(nombre_campeonato) && comprobarExpresionRegular(nombre_campeonato,/^([A-Za-zá-úÁ-Ú]+\s*)+$/)  && esVacio(nivel)">
				<div class="form-group">
                    <label>ID de grupo</label>
					<input class="form-control" type="text" id="id_grupo" name="id_grupo" value="<?php echo $this->valores['ID_GRUPO'];   ?>" maxlength="50" size="50" readonly />
                </div>
				<div class="form-group">
					<label>Nombre Categoria</label>
					<input class="form-control" type="text" id="nombre_categoria" name="nombre_categoria" value="<?php echo $this->valores['NOMBRE_CATEGORIA'];   ?>" readonly />
				</div>
				<div class="form-group">
					<label>Nombre Campeonato</label>
					<input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="<?php echo $this->valores['NOMBRE_CAMPEONATO'];   ?>" readonly />
                </div>
				<div class="form-group">
					<label>Nivel</label>
                    <input class="form-control" type="text" id="nivel" name="nivel" value="<?php echo $this->valores['NIVEL'];   ?>"readonly />
				</div>
                <button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar <i class="fas fa-trash-alt"></i></button>
			 </form>
						
					
				
		</div>
    </section>
<?php
        include '../Views/footer.php';//incluimos el footer
            }
		
                
     }
        
	


?>