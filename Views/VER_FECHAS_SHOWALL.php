<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class VER_FECHAS_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$grupo,$categoria,$campeonato,$nivel) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
		$this->render($this->valores,$this->lista,$this->grupo,$categoria,$campeonato,$nivel);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$grupo,$categoria,$campeonato,$nivel) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
       
		?>

<section class="section">
    <div class="container">
      <h2>Grupo <?php echo $this->grupo; ?> de Campeonato <?php echo $this->campeonato; ?> <?php echo $this->categoria; ?> y Nivel <?php echo $this->nivel; ?> </h2>
        
        
        <table class="table table-bordred table-striped">
            	
            
            
        			<tr>
<?php
				foreach ( $this->lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
                         
                if($atributo !='ID_ENFRENTAMIENTO'  && $atributo !='ID_PAREJA'){
                   
?>
                    
					<th>
						<?php echo $atributo; ?>
					</th>
<?php
					
                     
                        }   
                        
                }
          
 ?>
                        <th>Aceptar Fecha y Hora</th>
                 
                    
                    <!-- <th>Opciones</th> -->
				</tr>
<?php
				

				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php 
					
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
                    
                        if($atributo !='ID_ENFRENTAMIENTO' && $atributo !='ID_PAREJA' ){
                            
                      
?>
					<td>
<?php 	
				
                       echo $fila[ $atributo ];
                        

?>
					</td>
<?php
                        }
                          
					}
                    
?>
                
				<td>




				<form action="../Controllers/PROPONER_FECHA_CONTROLLER.php" method="post" style="display:inline" >
                            
                            <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                            <input type="hidden" name="id_enfrentamiento" value="<?php echo $fila['ID_ENFRENTAMIENTO']; ?>"> 
                            <input type="hidden" name="nivel" value="<?php echo $this->nivel; ?>">
                           
                            <input type="hidden" name="id_pareja" value="<?php echo $fila['ID_PAREJA']; ?>">
                            <input type="hidden" name="id_fecha_propuesta" value="">
                            <input type="hidden"    name="fecha_propuesta" readonly  value="<?php echo $fila['FECHA_PROPUESTA']; ?>"  >
                            <input type="hidden" name="hora_propuesta" readonly value="<?php echo $fila['HORA_PROPUESTA']; ?>"  >
                            							
                      <?php
                                $fecha_actual=date("d/m/Y");
                                $porciones = explode("/", $fecha_actual);
                                $dia_actual= intval($porciones[0]);
                                $mes_actual= intval($porciones[1]);
                                $ano_actual= intval($porciones[2]);  
                    
                    
                        

                                $fecha = explode("/", $fila['FECHA_PROPUESTA']);
                                $dia_propuesto= intval($fecha[0]);
                                $mes_propuesto= intval($fecha[1]);
                                $ano_propuesto= intval($fecha[2]); 
                    
                         
                    
                            if(($dia_propuesto >= $dia_actual && $mes_propuesto >= $mes_actual && $ano_propuesto >= $ano_actual) || 
                               ($mes_propuesto > $mes_actual && $ano_propuesto >= $ano_actual) || ($ano_propuesto>$ano_actual) ){

                    ?>
                    
						  <button type="submit" name="action" value="VER_PROPUESTAS" ><i class="fas fa-pencil-alt"></i></button>
                    <?php
                        }          
                    ?>
                    
                    
                </form>
				</td>

               	   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>