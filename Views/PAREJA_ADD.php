<?php

//es la clase ADD de USUARIO que nos permite añadir un usuario
class PAREJA_ADD {
//es el constructor de la clase USUARIO_ADD
	function __construct($categoria,$campeonato,$nivel,$capitan) {
		$this->render($categoria,$campeonato,$nivel,$capitan);//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render($categoria,$campeonato,$nivel,$capitan) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->campeonato = $campeonato;
        $this->categoria = $categoria;
        $this->nivel=$nivel;
        $this->capitan = $capitan;
        
        
?>
<section   class="section">
   <div class="container">
      <h1>Entra en el campeonato</h1>
			<form name="ADD" action="../Controllers/PAREJA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(capitan) && esVacio(login_pareja) && esVacio(nombre_categoria) && esVacio(nombre_campeonato) && esVacio(nivel)">
				
                        <input type="hidden" id="id_pareja" name="id_pareja" value="" />
                
    
                        <div class="form-group">
                        <label>Capitan</label>
                        <input class="form-control" type="text" id="capitan" name="capitan" value="<?php echo $this->capitan ?>" maxlength="20" size="20" readonly /><br>        
                        </div>    
                        
                        <div class="form-group">
                         <label>Login pareja</label>
                        <input  class="form-control" type="text" id="login_pareja" name="login_pareja" value="" maxlength="20" size="20"  /><br>
                        </div>  
                            
                            
                        <div class="form-group">    
                        <label>Nombre categoría</label>
						<input class="form-control" type="text" id="nombre_categoria" name="nombre_categoria" value="<?php echo $this->categoria ?>" maxlength="50" size="50" readonly/><br>
                        </div>      
                            
                            
                        <div class="form-group"> 
                        <label>Nombre campeoanto</label>
						<input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="<?php echo $this->campeonato ?>" maxlength="50" size="50" readonly /><br>
                        </div>    
                        
                    
                        <div class="form-group">
                        <label>Nivel</label>
                        <input class="form-control" type="text" id="nivel" name="nivel" value="<?php echo $this->nivel ?>" maxlength="20" size="20" readonly /><br>
                        </div> 
				
                        <input type="hidden" id="id_grupo" name="id_grupo" value="" /> 
                        <input type="hidden" id="nombre_categoria_grupo" name="nombre_categoria_grupo" value="" />   
                        <input type="hidden" id="nombre_campeonato_grupo" name="nombre_campeonato_grupo" value="" /><br>
					     
					
                       
				
                <button class="btn btn-default" type="submit" name="action" value="ADD">Entrar en campeonato<i class="fas fa-plus-circle"></i></button>
                
			 </form>
						
					
				
		</div>
    </section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>