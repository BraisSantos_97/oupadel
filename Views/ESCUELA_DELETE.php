<?php

//es la clase DELETE de USUARIO que nos permite borrar un usuario
class ESCUELA_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($valores,$escuela) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        
		$this->render( $this->valores,$escuela);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render( $valores,$escuela) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        $this->escuela=$escuela;
		include_once '../Views/header.php';//incluimos la cabecera
?>
		<section class="section">
    <div class="container">
      <h2>Borrar escuela: <?php echo $this->escuela ?></h2>
			<form name="DELETE" action="../Controllers/ESCUELA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(nombre_escuela) && comprobarExpresionRegular(nombre_escuela,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(descripcion_escuela) &&comprobarExpresionRegular(descripcion_escuela,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(tipo_escuela) && && comprobarExpresionRegular(tipo_escuela,/^([A-Za-zá-úÁ-Ú]+\s*)+$/)">
				<div class="form-group">
                    <label>Nombre Escuela</label>
					<input class="form-control" type="text" id="nombre_escuela" name="nombre_escuela" value="<?php echo $this->valores['NOMBRE_ESCUELA'];   ?>" readonly /><br>
				</div>
				<div class="form-group">
                    <label>Descripcion escuela</label>
                    <textarea class="form-control" type="text" id="descripcion_escuela" name="descripcion_escuela" readonly><?php echo $this->valores['DESCRIPCION'];  ?></textarea><br>
				</div>
				<div class="form-group">
                    <label>Tipo escuela</label>   
					<input class="form-control" type="text" id="tipo_escuela" name="tipo_escuela" readonly value="<?php echo $this->valores['TIPO_CLASE'];?>" /><br>
				</div>
                <button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar <i class="fas fa-trash-alt"></i></button>
			 </form>

	</div>
</section>
<?php
        include '../Views/footer.php';//incluimos el footer
            }
		
                
     }
        
	


?>