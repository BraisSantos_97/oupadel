<?php
//es la clase DELETE de USUARIO que nos permite borrar un usuario
class RESERVA_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($datos,$reserva) { 
		$this->datos=$datos;//pasamos los datos de cada uno de los campos
		$this->reserva=$reserva;
		$this->render($this->datos,$this->reserva);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render($datos,$reserva) { 
		$this->datos=$datos;//pasamos los datos de cada uno de los campos
		$this->reserva=$reserva;
		include_once '../Views/header.php';//incluimos la cabecera
?>
	<section   class="wow fadeIn">
        <div class="container">
            <h2>Cancelar reserva: <?=$this->reserva?></h2>
			<form name="DELETE" action="../Controllers/RESERVA_CONTROLLER.php?action=DELETE" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_pista) && esVacio(fecha) && esVacio(hora)">
					<div class="form-group">
					<label>ID de pista</label>
					<input class="form-control" type="text" id="id_pista" name="id_pista" value="<?=$this->datos->getIdPista();?>" readonly/>
					</div>
					<div class="form-group">
					<label>Fecha</label>
					<input class="form-control" type="text" id="fecha" name="fecha" value="<?=$this->datos->getFecha();?>" maxlength="20" readonly/>
					</div>
					<div class="form-group">
					<label>Hora</label>
					<input class="form-control" type="text" id="hora" name="hora" value="<?=$this->datos->getHora();?>" maxlength="20" readonly/>
					</div>
                <button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar reserva <i class="fas fa-trash-alt"></i></button>
			</form>		
		</div>
    </section>
<?php
        include '../Views/footer.php';//incluimos el footer
        }       
    }
?>