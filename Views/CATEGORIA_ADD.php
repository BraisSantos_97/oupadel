<?php

//es la clase ADD de USUARIO que nos permite añadir un usuario
class CATEGORIA_ADD {
//es el constructor de la clase USUARIO_ADD
	function __construct($campeonato) {
		$this->render($campeonato);//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
	}
//funcion que  mostrará el formulario ADD con los campos correspondientes
	function render($campeonato) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->campeonato = $campeonato;
?>
<section class="section">
    <div class="container">
        <h2>Añadir Categoria</h2>
        <form name="ADD" action="../Controllers/CATEGORIA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(nombre_campeonato) && comprobarExpresionRegular(nombre_campeonato,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(nombre_categoria) && comprobarExpresionRegular(nombre_categoria,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(descripcion_categoria) && esVacio(nivel)">
            <div class="form-group">
                <label>Nombre Campeonato</label>
                <input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="<?php echo $this->campeonato ?>" maxlength="50" size="50" readonly />
            </div>
            <div class="form-group">        
                <label>Nombre Categoria</label>
                <select class="custom-select" name="nombre_categoria" id="nombre_categoria"  placeholder="Categoria">
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
                    <option value="Mixto">Mixto</option>
                </select>
            </div>
            <div class="form-group">
                <label>Descripcion categoria</label>
                <textarea class="form-control" rows="4" cols="50" type="text" id="descripcion_categoria" name="descripcion_categoria"  value="" maxlength="100" size="100"></textarea>
            </div>
            <div class="form-group">
                <label>Nivel</label>
                <select class="form-control" name="nivel" id="nivel"  placeholder="Nivel">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
            <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir <i class="fas fa-plus-circle"></i></button>
        </form>			
	</div>
</section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>