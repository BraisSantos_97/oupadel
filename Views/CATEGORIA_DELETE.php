<?php

//es la clase DELETE de USUARIO que nos permite borrar un usuario
class CATEGORIA_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($valores,$categoria,$nivel) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        
		$this->render( $this->valores,$categoria,$nivel);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render( $valores,$categoria,$nivel) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        $this->categoria=$categoria;
        $this->nivel=$nivel;
        
		include_once '../Views/header.php';//incluimos la cabecera
?>
		<section class="wow fadeIn">
    <div class="container">
	    <h2>Borrar categoria: <?php echo $this->categoria ?>, nivel: <?php echo $this->nivel ?></h2>
		<form name="DELETE" action="../Controllers/CATEGORIA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(nombre_campeonato) && comprobarExpresionRegular(nombre_campeonato,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(nombre_categoria) && comprobarExpresionRegular(nombre_categoria,/^([A-Za-zá-úÁ-Ú]+\s*)+$/)&& esVacio(nivel) && esVacio(descripcion_categoria) ">
			<div class="form-group">
				<label>Nombre Campeonato</label>
				<input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="<?php echo $this->valores['NOMBRE_CAMPEONATO'];?>" maxlength="50" readonly />
			</div>
			<div class="form-group">
				<label>Nombre Categoria</label>
				<input class="form-control" type="text" id="nombre_categoria" name="nombre_categoria" value="<?php echo $this->valores['NOMBRE_CATEGORIA'];   ?>" maxlength="50" readonly />
			</div>
			<div class="form-group">
				<label>Nivel</label>
				<input class="form-control" type="text" id="nivel" name="nivel" value="<?php echo $this->valores['NIVEL'];?>" readonly />	
			</div>
			<div class="form-group">
				<label>Descripcion campeonato</label>
				<textarea class="form-control" type="text" id="descripcion_campeonato" name="descripcion_categoria" maxlength="100" size="100" readonly><?php echo $this->valores['DESCRIPCION_CATEGORIA'];?></textarea>
			</div>
			
			<button class="btn btn-default" type="submit" name="action" value="DELETE">Borrar <i class="fas fa-trash-alt"></i></button>
		</form>		
	</div>
</section>
<?php
        include '../Views/footer.php';//incluimos el footer
            }
		
                
     }
        
	


?>