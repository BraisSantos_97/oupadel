<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class Login {
	//es el constructor de la clase Login
	function __construct() {
		$this->render();//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render() {

		include_once '../Views/header.php';//incluimos la cabecera
		?>


  <div class="container text-center">
    <h1>Bienvenido a OuPadel</h1>
    <img id='img-login' src="../Views/img/padel.jpg" alt="Padel">
  </div>


  <div id="login-form" class="container">
    <h2>Iniciar Sesión</h2>
    <form name="LOGIN" method="post" action="#" enctype="multipart/form-data" onsubmit="return esVacio(login) && esVacio(password)">
    <div class="form-group">
      <label>Login</label>
      <input class="form-control" type="text" id="login" name="login" maxlength="20" required/>
    </div>
    <div class="form-group">
      <label>Password</label> 
      <input class="form-control" type="password" id="password" name="password" maxlength="20" required/>
    </div>
      <input class="btn btn-default" type="submit" value="Inciar sesion">
    </form>
  </div>

  <div id="register-form" class="container">
    <h2>Registrar</h2>
    <form name="ADD"  action='../Controllers/register_controller.php' method="post" enctype="multipart/form-data"  onsubmit="return esVacio(login) && esVacio(password) && esVacio(nombre) && comprobarExpresionRegular(nombre,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(apellido) && comprobarExpresionRegular(apellido,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(telefono) && comprobarExpresionRegular(telefono,/[0-9]{9}/) && esVacio(sexo)">
      <div class="form-group">
        <label >Login</label> 
        <input class="form-control" type="text" id="login" name="login" maxlength="20" required/>
      </div>
      <div class="form-group">
        <label>Email</label> 
        <input class="form-control" type="email" id="email" name="email" maxlength="30" required/>
      </div>  
      <div class="form-group">
        <label >Password</label> 
        <input class="form-control" type="password" id="password" name="password" maxlength="20" required/>
      </div>
      <div class="form-group">
        <label >Nombre</label> 
        <input class="form-control" type="text" id="nombre" name="nombre" maxlength="25" required/>
      </div>
      <div class="form-group">
        <label >Apellidos</label> 
        <input class="form-control" type="text" id="apellido" name="apellido" maxlength="50" required/>
      </div>
      <div class="form-group">
        <label >Telefono</label> 
        <input class="form-control" type="number" id="telefono" name="telefono" maxlength="9" required/>
      </div>
      <div class="form-group">
        <label >Sexo</label> 
        <select class="form-control" name="sexo" id="sexo" > 
          <option value="Masculino">Masculino</option>    
          <option value="Femenino">Femenino</option>    
        </select>
      </div>
      
      <input class="btn btn-default" type="submit" name="action" value="Registrar">
    </form>
  </div>

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>