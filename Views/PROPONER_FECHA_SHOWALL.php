<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class PROPONER_FECHA_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$grupo,$categoria,$campeonato,$nivel) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
		$this->render($this->valores,$this->lista,$this->grupo,$categoria,$campeonato,$nivel);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$grupo,$categoria,$campeonato,$nivel) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
       
		?>

<section class="section">
    <div class="container">
      <h2>Grupo <?php echo $this->grupo; ?> de Campeonato <?php echo $this->campeonato; ?> <?php echo $this->categoria; ?> y Nivel <?php echo $this->nivel; ?></h2>
        
        
        <table class="table table-bordred table-striped">
            	
            
            
        			<tr>
<?php
				foreach ( $this->lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
                $nombresatributos = array("ID_ENFRENTAMIENTO"=>"Enfrentamiento","ID_GRUPO"=>"Grupo","NOMBRE_CAMPEONATO"=>"Campeonato","NOMBRE_CATEGORIA"=>"Categoría","NIVEL"=>"Nivel","CAPITAN"=>"Capitán","LOGIN_PAREJA"=>"Pareja");
                  if($atributo !='ID_PAREJA'){       
                            
?>

                    <th>
						<?php echo $nombresatributos[$atributo]; ?>
					</th>
<?php
					
                       }   
                   }
            
 ?>
                    <th>Ver Rival</th>     
                    <th>Fecha y Hora</th>   
                    <th>Meter Fecha</th>
                    <th>Ver Fechas Aceptadas</th>
                     
                    
                    <!-- <th>Opciones</th> -->
				</tr>
<?php
				
                $i=1;
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php 
					
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario

                      if($atributo !='ID_PAREJA'){
                          
                                  
?>
                
					<td>
<?php 	
		          if($atributo == 'ID_ENFRENTAMIENTO'){
                      echo "nº".$i;
                      $i++;
                  }
                 else{
                        echo $fila[ $atributo ];
                     }
                       
                        

?>
					</td>
                     
<?php
                       }    
					}
                    
?>
                    
                 <td>
                     
                
                    <form action="../Controllers/PAREJA_CONTROLLER.php" method="post" style="display:inline" >
                            
                            <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                            <input type="hidden" name="nivel" value="<?php echo $this->nivel ?>">
                    
                        
                            <input type="hidden" name="id_enfrentamiento" value="<?php echo $fila['ID_ENFRENTAMIENTO'] ?>"> 
                            <input type="hidden" name="id_fecha_propuesta" value=""> 
                            <input type="hidden" name="id_pareja" value="<?php echo $fila['ID_PAREJA']; ?>">
                            
						  <button type="submit" name="action" value="VER_RIVAL" ><i class="fas fa-arrow-right"></i></button>
                
                </form>
                    
             
                </td>     
                           
                
				<td>




				<form action="../Controllers/PROPONER_FECHA_CONTROLLER.php" method="post" style="display:inline" onsubmit="return esVacio(fecha_propuesta) && esVacio(hora_propuesta)">
                            
                            <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                            <input type="hidden" name="nivel" value="<?php echo $this->nivel ?>">
                    
                            <input type="hidden" name="id_enfrentamiento" value="<?php echo $fila['ID_ENFRENTAMIENTO'] ?>"> 
                            <input type="hidden" name="id_fecha_propuesta" value=""> 
                            <input type="hidden" name="id_pareja" value="<?php echo $fila['ID_PAREJA']; ?>">
                            <input type="text"   class="tcal"  name="fecha_propuesta" placeholder="Fecha" readonly >
                            <!--<input type="text" name="hora_propuesta" placeholder="Hora" >-->
                            <select  name="hora_propuesta" id="hora_propuesta"  placeholder="Hora">
                            <option value="10:00">10:00</option>
                            <option value="11:30">11:30</option>
                            <option value="13:00">13:00</option>
                            <option value="14:30">14:30</option>
                            <option value="16:00">16:00</option>
                            <option value="17:30">17:30</option>
                            <option value="19:00">19:00</option>
                            <option value="20:30">20:30</option>
                     
                            </select>
                           
        	</td>
            
            <td>       
            							
						  <button type="submit" name="action" value="PROPONER_FECHA" ><i class="fas fa-pencil-alt"></i></button>
              </td>   
                </form>
        
    
                 <td>
                     
                
                    <form action="../Controllers/PROPONER_FECHA_CONTROLLER.php" method="post" style="display:inline" >
                            
                            <input type="hidden" name="id_grupo" id="id_grupo" value="<?php echo $this->grupo; ?>">
                            <input type="hidden" name="nombre_campeonato" value="<?php echo   $this->campeonato?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $this->categoria ?>">
                            <input type="hidden" name="nivel" value="<?php echo $this->nivel ?>">
                    
                        
                            <input type="hidden" name="id_enfrentamiento" value="<?php echo $fila['ID_ENFRENTAMIENTO'] ?>"> 
                            <input type="hidden" name="id_fecha_propuesta" value=""> 
                            <input type="hidden" name="id_pareja" value="<?php echo $fila['ID_PAREJA']; ?>">
                            
						  <button type="submit" name="action" value="FECHAS_ACEPTADAS" ><i class="fas fa-arrow-right"></i></button>
                
                </form>
                    
             
                </td>     
            
        
               	   
				</tr>
<?php
			
                        
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>