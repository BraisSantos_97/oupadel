<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class PARTIDO_SHOWALL {
    //es el constructor de la clase Login
    function __construct($lista,$valores) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->render($this->valores,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
    }
    //función render donde se mostrará el formulario login con los campos correspondientes
    function render($valores,$lista) {
        
        include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        
        ?>

         <!--==========================
    Hero Section
  ============================-->

<section   class="section">
    <div class="container">
      <h2>Partidos Promocionados</h2>
        
       

        <table class="table table-bordred table-striped">
            
            <?php
                if($_SESSION['login'] == 'admin'){
             ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/PARTIDO_CONTROLLER.php'>
                        <button class="btn btn-default" type="submit" name="action" value="ADD">Promocionar partido <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            
            
            <?php
                }
            ?>
       
            
            
                    <tr>
<?php
$nombreatributos=array("ID_PARTIDO"=>"Partido","FECHA_PARTIDO"=>"Fecha","HORA_PARTIDO"=>"Hora","ID_PISTA"=>"Pista");
                foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
                    <th>
                        <?php echo $nombreatributos[$atributo]; ?>
                    </th>
<?php
                    }
        
                ?>
                    <?php
                         if($_SESSION['login']=='admin'){
                             
                         
                    ?>
                        
                        <th>Operaciones</th>
                        
                    <?php
                        }
                    ?>
                    <?php    
                        if($_SESSION['login']!='admin'){
                            
                        
                    ?>
                        <th>Inscribirse en Partido </th>
                        
                    <?php
                        }
                    ?>
                        
                <?php
    
                              
 ?>
  
<?php
                while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
                <tr>
<?php
                    foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
                    <td>
<?php 
                if($_SESSION['login']!='admin'){
                    
                
                        $fecha_actual=date("d/m/Y");
                        $porciones = explode("/", $fecha_actual);
                        $dia_actual= intval($porciones[0]);
                        $mes_actual= intval($porciones[1]);
                        $ano_actual= intval($porciones[2]);

                        $partes=explode("/", $fila['FECHA_PARTIDO']);
                        $dia_limite= intval($partes[0]);
                        $mes_limite= intval($partes[1]);
                        $ano_limite= intval($partes[2]);

                  
                    if(($dia_actual > $dia_limite && $mes_actual >= $mes_limite && $ano_actual >=$ano_limite) || ($mes_actual > $mes_limite && $ano_actual >=$ano_limite) || ($ano_actual >$ano_limite) ) {
                        
                    }
                        else{
                            echo $fila[ $atributo ];
                        }
                   
                 }
                
                else{
                    echo $fila[ $atributo ];
                }
              
                        
                       

?>
                    </td>
<?php
                    }
                    if($_SESSION['login'] =='admin'){
                        
                    
?>
                    <td>
                        <form action="../Controllers/PARTIDO_CONTROLLER.php" method="get" style="display:inline" >
                            
                            <input type="hidden" name="id_partido" value="<?php echo $fila['ID_PARTIDO']; ?>">
                            
                            
                                <!--<button type="submit" name="action" value="EDIT" ><i class="fas fa-edit"></i></button>-->
                    
                                 <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                            </form>
                        
                    </td>
    <?php
        }   
            
            if($_SESSION['login']!='admin'){
                
                        $fecha_actual=date("d/m/Y");
                        $porciones = explode("/", $fecha_actual);
                        $dia_actual= intval($porciones[0]);
                        $mes_actual= intval($porciones[1]);
                        $ano_actual= intval($porciones[2]);

                
                        $partes=explode("/", $fila['FECHA_PARTIDO']);
                        $dia_limite= intval($partes[0]);
                        $mes_limite= intval($partes[1]);
                        $ano_limite= intval($partes[2]);

                  
                    if(($dia_actual <= $dia_limite && $mes_actual <= $mes_limite && $ano_actual <= $ano_limite) || ($mes_actual < $mes_limite && $ano_actual <= $ano_limite) || ($ano_actual < $ano_limite) ) {
                        
                  
                    
    ?>
                    
                    
    <td>
        <form action="../Controllers/INSCRIPCION_CONTROLLER.php">
                <input type="hidden" name ="id_partido" id="id_partido" value="<?php echo $fila['ID_PARTIDO']; ?>" >
               <input type="hidden" name="fecha_partido" value="<?php echo $fila['FECHA_PARTIDO']; ?>">
               <input type="hidden" name="hora_partido" value="<?php echo $fila['HORA_PARTIDO']; ?>">
                       <button class="btn btn-default" type="submit" name="action" value="ADD"><i class="fas fa-plus-circle"></i></button>
            </form>
                   
    </td>
                    
       <?php
                }  
                
            }
    ?>                
                    
                    
                       
                </tr>
<?php
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
        include '../Views/footer.php';//incluimos el footer
    } //fin metodo render

    } //fin Login

?>