<?php

//es la clase ADD de USUARIO que nos permite a�adir un usuario
class PARTIDO_EDIT {
//es el constructor de la clase USUARIO_ADD
	function __construct($valores) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos de la tupla que eligimos en el showall
		$this->render($this->valores);//funcion que mostrar� el formulario EDIT con los campos correspondientes
	}
//funcion que  mostrar� el formulario ADD con los campos correspondientes
	function render($valores) { 
 		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        
		include_once '../Views/header.php';//incluimos la cabecera
?>
<section   class="wow fadeIn">
    <div class="hero-container">
      <h1>Editar Partido</h1>
        
			<form name="EDIT" action="../Controllers/PARTIDO_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(id_partido) && esVacio(fecha_partido) && esVacio(hora_partido)">
				
                        ID Partido<br>
						<input type="text" id="id_partido" name="id_partido" value="<?php echo $this->valores['ID_PARTIDO']?>" maxlength="10" size="10"  readonly/><br>
				
                        Fecha Partido<br>
						<input type="text" id="fecha_partido" name="fecha_partido" value="<?php echo $this->valores['FECHA_PARTIDO']?>" maxlength="20" size="20" class="tcal" readonly /><br>
					
                        Hora Partido<br>
						  <select  name="hora_partido" id="hora_partido">
						  	<option value="<?php echo $this->valores['HORA_PARTIDO']?>" selected><?php echo $this->valores['HORA_PARTIDO']?></option>
                            <option value="10:00">10:00</option>
                            <option value="11:30">11:30</option>
                            <option value="13:00">13:00</option>
                            <option value="14:30">14:30</option>
                            <option value="16:00">16:00</option>
                            <option value="17:30">17:30</option>
                            <option value="19:00">19:00</option>
                            <option value="20:30">20:30</option>
                     </select><br>
                        <br>
                        
                        <input type="hidden" id="id_pista" name="id_pista"  value=""/>
					
				
							<button type="submit" name="action" value="EDIT"><i class="fas fa-edit"></i></button>
			 </form>
						
					
				
		</div>
    </section>
<?php
		include '../Views/footer.php';//incluimos el footer
		}
		}
?>