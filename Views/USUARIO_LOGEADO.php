<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class USUARIO_LOGEADO {
	//es el constructor de la clase Login
	function __construct($valores,$lista) {
        $this->lista=$lista;
        $this->valores =$valores;
		$this->render($this->valores,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        
		?>

		 <!--==========================
    Hero Section
  ============================-->
<?php
    if($_SESSION['login'] == 'admin'){
    ?>
        <section   class="wow fadeIn">
    <div class="container">
      <h2>Deportistas registrados en el sistema</h2>
        <table class="table">
					<thead class="thead-dark">
							<tr>
<?php
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
						<?php echo $atributo; ?>
					</th>
<?php
					}
 ?>
				</tr>
				</thead>
<?php
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos
                  
?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 
							echo $fila[ $atributo ];

?>
					</td>
<?php
					}
?>
					
				</tr>
<?php
				}
        
?>
        </table>
    </div>
  </section><!-- #hero -->
<?php
    }      
    else{
            
       
?>

  <section   class="section">
    <div class="container text-center">
      <h1>¡Diviértete en nuestro club!</h2>
      <img src="../Views/img/image.png" alt="Hero Imgs">
    </div>
  </section><!-- #hero -->

<?php
   }      
?>
  
 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>