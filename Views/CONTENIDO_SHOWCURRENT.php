<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class CONTENIDO_SHOWCURRENT {
	//es el constructor de la clase Login
	function __construct($lista,$datos) {
        $this->lista=$lista;
        $this->datos=$datos;
		$this->render($this->datos,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($datos,$lista) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->datos =$datos;
        $this->lista=$lista;
?>

<section>
    <h1 class="h1-responsive text-center">Contenido</h1>
    <div class="container">
        
        <?php
        $fila = mysqli_fetch_array( $this->datos )
        ?>
        <div class="container">
            <div class="panel panel-default">
                <p class="panel-heading h3"><?= $fila["TITULO"] ?></p>
                <textarea class="col-lg-12 panel-body" rows="15" readonly><?php echo $fila["TEXTO"]; ?></textarea>
                <div class="row">
                <p class="col-lg-11"><?= $fila["FECHA_PUBLICACION"] ?></p>
                <?php
                if($_SESSION['login'] =='admin'){     
                ?>
                    <form class="col-lg-1" action="../Controllers/CONTENIDO_CONTROLLER.php" method="get" style="display:inline" >
                        <input type="hidden" name="id_contenido" value="<?php echo $fila['ID_CONTENIDO']; ?>">
                        
                        <button type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                    </form>
                <?php
                }              
                ?>
                </div>
            <div>
        </div>
    </div>
</section>

<?php
	include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

} //fin Login

?>