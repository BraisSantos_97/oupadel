<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class CLASIFICACION_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$grupo,$categoria,$campeonato,$nivel) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->grupo = $grupo;
		$this->render($this->valores,$this->lista,$this->grupo,$categoria,$campeonato,$nivel);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$grupo,$categoria,$campeonato,$nivel) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->grupo = $grupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        
		?>

<section class="section">
    <div class="container">
      <h2>Clasificación de Grupo <?php echo $this->grupo; ?> de Campeonato <?php echo $this->campeonato; ?> <?php echo $this->categoria; ?> Nivel <?php echo $this->nivel; ?> </h2>
        
        
        <table class="table table-bordred table-striped">
            		
            
            
        			<tr>
                    <th>Posición</th>

<?php
$nombreatributos=array("CAPITAN"=>"Capitán","LOGIN_PAREJA"=>"Login pareja","PUNTOS"=>"Puntos");
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
                    if($atributo != 'ID_PAREJA'){
                  
?>
					<th>
						<?php echo $nombreatributos[$atributo]; ?>
					</th>
<?php
					}
                     
                 }
                      
                    
 ?>
				</tr>
<?php
			
                $pos=1;
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr> 
                <td><?php echo "<b>".$pos."º"."<b>"; ?></td>
<?php 
					
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
                        
                        if($atributo !='ID_PAREJA'){
                            
                      
?>
					
                   
                    <td>
<?php 	
                    if($atributo == 'CAPITAN' || $atributo == 'LOGIN_PAREJA'){
                        
                        echo "<i>".$fila[ $atributo ]."<i>";
                    }
                    else{
                       echo $fila[ $atributo ];
                    }

?>
					</td>
<?php
                   
                              }
					}
                    $pos++; 
                    
?>
                
                    		   
				</tr>
<?php
				
                  
                }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>