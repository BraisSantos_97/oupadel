<?php

//es la clase DELETE de USUARIO que nos permite borrar un usuario
class CLASES_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($valores,$clase) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        
		$this->render( $this->valores,$clase);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render( $valores,$clase) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        $this->clase=$clase;
		include_once '../Views/header.php';//incluimos la cabecera
?>
		<section class="wow fadeIn">
    <div class="container">
      	<h2>Borrar clase: <?php echo $this->clase ?></h2>
		<form name="DELETE" action="../Controllers/CLASES_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(fecha_clase) && esVacio(hora_clase)">
			<div class="form-group">
				<label>ID Clase</label>
				<input class="form-control" type="text" id="id_clase" name="id_clase" value="<?php echo $this->valores['ID_CLASE'];   ?>" maxlength="10" readonly />
			</div>
			<div class="form-group">
				<label>Fecha Clase</label>
				<input class="form-control" type="text" id="fecha_clase" name="fecha_clase"  value="<?php echo $this->valores['FECHA_CLASE'];   ?>"  maxlength="20"/>
			</div>
			<div class="form-group">
				<label>Hora Clase</label>   
				<input class="form-control" type="text" id="hora_clase" name="hora_clase"  value="<?php echo $this->valores['HORA_CLASE'];   ?>"  maxlength="20"/>
			<div class="form-group">
				<label>Nombre Escuela</label>   
				<input class="form-control" type="text" id="nombre_escuela" name="nombre_escuela"  value="<?php echo $this->valores['NOMBRE_ESCUELA'];   ?>"  maxlength="50"/>
			</div>
			<div class="form-group">
				<label>ID Pista</label>   
				<input class="form-control" type="text" id="id_pista" name="id_pista"  value="<?php echo $this->valores['ID_PISTA'];?>"  maxlength="10"/>	
			</div>
			<button type="submit" name="action" value="DELETE">Borrar clase <i class="fas fa-trash-alt"></i></button>
		</form>
						
					
				
		</div>
    </section>
<?php
        include '../Views/footer.php';//incluimos el footer
            }
		
                
     }
        
	


?>