<?php
class PISTA_ADD {
    //es el constructor de la clase USUARIO_ADD
    function __construct() {
        $this->render();//llamamos a la función render donde se mostrará el formulario ADD con los campos correspondientes
    }
    //funcion que  mostrará el formulario ADD con los campos correspondientes
    function render() {
        include_once '../Views/header.php';//incluimos la cabecera
?>
        <section class="section">
            <div class="container">
                <h1>Añadir Pista</h1>
                <form name="ADD" action="../Controllers/PISTA_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(tipo_pista) && comprobarExpresionRegular(tipo_pista,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(descripcion_pista) && comprobarExpresionRegular(descripcion_pista,/^([A-Za-zá-úÁ-Ú]+\s*)+$/)">	
                    <div class="form-group">
                        <label>Tipo de pista</label>
                        <input class="form-control" type="text" id="tipo_pista" name="tipo_pista" value="" maxlength="50"  required/>
                    <div>
                    <div class="form-group">
                        <label>Descripción de pista</label>
                        <input class="form-control" type="text" id="descripcion_pista" name="descripcion_pista" value="" maxlength="255" required/>
                    <div>
                    <button class="button-submit" type="submit" name="action" value="ADD">Añadir <i class="fas fa-plus-circle"></i></button>
                </form>		
            </div>
        </section>
<?php
		include '../Views/footer.php';//incluimos el footer
	}
}
?>