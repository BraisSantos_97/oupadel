<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class CAMPEONATO_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores) {
        $this->lista=$lista;
        $this->valores =$valores;
		$this->render($this->valores,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        
		?>

		 <!--==========================
    Hero Section
  ============================-->

<section class="section">
    <div class="container">
        <h2>Campeonatos del Club</h2>
        <table class="table table-bordred table-striped"> 
            <?php
                if($_SESSION['login'] == 'admin'){
             ?>
            <tr>
                <th colspan="100%">
                <form action='../Controllers/CAMPEONATO_CONTROLLER.php'>
                    <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir campeonato <i class="fas fa-plus-circle"></i></button>
                </form>
                </th>
            </tr>
            
            <?php
                }
            ?>
       
            
            
        			<tr>
<?php
$nombresatributos = array("NOMBRE_CAMPEONATO"=>"Nombre del campeonato", "FECHA_INICIO"=>"Fecha de inicio", "FECHA_FINAL"=>"Fecha final", "DESCRIPCION_CAMPEONATO"=>"Descripción", "FECHA_LIMITE_INSCRIPCION"=>"Límite de inscripción", "Opciones"=>"Opciones", "Categoria"=>"Categoría");
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
						<?= $nombresatributos[$atributo]; ?>
					</th>
<?php
					}
    
                              
 ?>
            <?php
                if($_SESSION['login'] =='admin'){ 
            ?>
                    <th>Opciones</th>
            <?php
                }
            ?>
                    <th>Categoria</th>
				</tr>
<?php
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 
	               if($atributo =="DESCRIPCION_CAMPEONATO"){
                       ?>
                        
                       <textarea class="form-control" rows="4" cols="100" style = "resize:none" readonly><?php echo $fila[ $atributo ]; ?></textarea>

                        <?php
                   }
                        else{
                             echo $fila[ $atributo ];
                        }
                        
                       

?>
					</td>
<?php
					}
                    if($_SESSION['login'] =='admin'){
                        
                    
?>
                    <td>
						<form action="../Controllers/CAMPEONATO_CONTROLLER.php" method="get" style="display:inline" >
				    <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
								<!--<button type="submit" name="action" value="EDIT" ><img src="../Views/img/edit.jpg" alt="EDIT" width="20" height="20" /></button>-->
				    
				        <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                            </form>
                        
					</td>
    <?php
        }              
    ?>
                      <td>
                    <a href="../Controllers/CATEGORIA_CONTROLLER.php?nombre_campeonato=<?php echo $fila['NOMBRE_CAMPEONATO']; ?>"><i class="fas fa-arrow-right"></i></a>
                  </td>
                    
                    
                    
					   
				</tr>
<?php
				}
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>