<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class INSCRIPCIONCLASE_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores) {
        $this->lista=$lista;
        $this->valores =$valores;
		$this->render($this->valores,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        
		?>

		 <!--==========================
    Hero Section
  ============================-->

<section class="section">
    <div class="container">
      <h2>Mis Clases</h2>
        
        
        <table class="table table-bordred table-striped"xº>
            
            
       
            
        			<tr>
<?php
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
				$nombreatributos = array("LOGIN" => "Login", "ID_CLASE"=>"Clase", "FECHA_CLASE" => "Fecha", "HORA_CLASE"=>"Hora", "NOMBRE_ESCUELA"=>"Escuela", "ID_PISTA"=>"Pista");
?>
					<th>
						<?php echo $nombreatributos[$atributo]; ?>
					</th>
<?php
					}
    
                              
 ?>
  
<?php
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 
	               
                             echo $fila[ $atributo ];
                              
                        
                       

?>
					</td>

    <?php
        }              
    ?>
   
                    
                    
                    
					   
				</tr>
<?php
				}
        
?>
        </table>
    </div>
  </section><!-- {
}#hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>