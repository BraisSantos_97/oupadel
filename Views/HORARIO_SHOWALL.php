<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class HORARIO_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$datos) {
        $this->lista=$lista;
        $this->datos=$datos;
		$this->render($this->datos,$lista);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($datos,$lista) {
		include_once '../Views/header.php';//incluimos la cabecera
        $this->datos =$datos;
        $this->lista=$lista;
?>

	<!--==========================
    Hero Section
    ============================-->

<section   class="wow fadeIn">
    <div class="container">
        <h2>Tabla de horarios</h2>    
        <table class="table table-bordred table-striped">
            <?php
            if($_SESSION['login'] == 'admin'){
            ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/HORARIO_CONTROLLER.php'>
                        <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir horario  <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            <?php
            }
            ?>
        	<tr>
                <?php
                $nombresatributos = array("FECHA"=>"Fecha", "HORA"=>"Hora", "DIA"=>"Dia", "Opciones"=>"Opciones");
			    foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
                ?>
				<th>
					<?= $nombresatributos[$atributo]; ?>
				</th>
                <?php
				}              
                ?>
                <?php
                if($_SESSION['login'] =='admin'){ 
                ?>
                <th>Opciones</th>
                <?php
                }
                ?>
			</tr>
            <?php
			while ( $fila = mysqli_fetch_array( $this->datos ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos
            ?>
			<tr>
                <?php
				foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
                ?>
				<td>
                <?= $fila[$atributo];
                }
                ?>
                </td>
                <?php
                if($_SESSION['login'] =='admin'){     
                ?>
                    <td>
                        <form action="../Controllers/HORARIO_CONTROLLER.php" method="get" style="display:inline" >
                            <input type="hidden" name="fecha" value="<?= $fila['FECHA']; ?>">
                            <input type="hidden" name="hora" value="<?= $fila['HORA']; ?>">
                            <button class="btn btn-default" type="submit" name="action" value="DELETE"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                <?php
                }                          
            }
            ?>
			</tr>
        </table>
    </div>
</section><!-- #hero -->

<?php
	include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

} //fin Login

?>