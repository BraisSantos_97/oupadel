/*Funciones individuales*/

/*Funcion esVacio*/
var solucion=false;
/*Funcion que nos sirve para comprobar si un campo es vacio*/
function esVacio(campo){
    /*if que nos irve para ver si el campo es nulo, está vacío o sólo tiene espacios en blanco*/
      if ((campo.value == null) || (campo.value.length == 0) || (/^\s+$/.test(campo.value))){
       	/*Estructura if para controlar que no se lancen alerts infinitos controlado por la variable solucion que se pone a true luego de que se lance el alert,se coloca el foco en el campo y se retorna false*/
        if (!solucion) {
          alert('El atributo ' + campo.name + ' no puede ser vacio');
          campo.focus();  
          solucion=true;
          setTimeout("solucion=false",10);
          
      }
      	  campo.style.borderColor='red';//Si el campo está vacío, se pone el borde del campo de color rojo
          return false;
      }
      /*Si no se cumple ninguna de las condiciones anteriores se devuelve el valor true*/
      else{
      	campo.style.borderColor='black';//Si el campo no está vacío, se pone el borde del campo de color blanco
        return true;
      }
} 


/*Variable que sirve para controlar que no se genere un bucle de alerts debido al uso del evento onblur*/
var solucion9=false;
/*Función que sirve para comprobar que el valor de un campo cumple con el formato marcado por una expresion regular y que no supera el tamaño máximo permitido*/
function comprobarExpresionRegular(campo,expresion){
  
    if(!(expresion.test(campo.value))){
      if(!solucion9){//Comprobamos el valor de la variable que permite romper el bucle de alerts generado por onblur
            alert('El atributo ' + campo.name + ' no cumple con el formato correcto');
            campo.focus();
              solucion9=true;
              setTimeout("solucion9=false",10);
              
          }
          campo.style.borderColor='red';
            return false;
    }
    else{//En caso de no entrar en ninguno de los casos anteriores se devuelve el valor true

      campo.style.borderColor='black';
      return true;
    }
  }
