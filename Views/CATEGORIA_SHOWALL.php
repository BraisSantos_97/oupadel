<?php

//Es la clase Login que nos permite mostrar la vista para logearse
class CATEGORIA_SHOWALL {
	//es el constructor de la clase Login
	function __construct($lista,$valores,$campeonato,$fecha_limite) {
        $this->lista=$lista;
        $this->valores =$valores;
        $this->campeonato=$campeonato;
        $this->fecha_limite = $fecha_limite;
		$this->render($this->valores,$this->lista,$this->campeonato,$this->fecha_limite);//Llamada a la función dónde se encuentra el formulario de logeo
	}
	//función render donde se mostrará el formulario login con los campos correspondientes
	function render($valores,$lista,$campeonato,$fecha_limite) {
        
		include_once '../Views/header.php';//incluimos la cabecera
        $this->valores =$valores;
        $this->lista=$lista;
        $this->campeonato=$campeonato;
        $this->fecha_limite = $fecha_limite;
		?>

<section class="section">
    <div class="container">
      <h2>Categorias del Campeonato: <?php echo $this->campeonato; ?> </h2>
        
        
        <table class="table table-bordred table-striped">
            <?php
                if($_SESSION['login'] == 'admin'){
             ?>
            <tr>
                <th colspan="100%">
                    <form action='../Controllers/CATEGORIA_CONTROLLER.php'>
                        <input type="hidden" name="nombre_campeonato" value="<?php echo $this->campeonato; ?>">
                        <button class="btn btn-default" type="submit" name="action" value="ADD">Añadir categoría <i class="fas fa-plus-circle"></i></button>
                    </form>
                </th>
            </tr>
            
            
            <?php
                }
            ?>
       
            
            
        			<tr>
<?php
$nombreatributos=array("NOMBRE_CATEGORIA"=>"Nombre categoría","DESCRIPCION_CATEGORIA"=>"Descripción","NOMBRE_CAMPEONATO"=>"Campeonato","NIVEL"=>"Nivel","Opciones"=>"Opciones","Grupos"=>"Grupos");
				foreach ( $lista as $atributo ) { //este bucle devolverá cada uno de los campos de la tabla USUARIO
?>
					<th>
						<?php echo $nombreatributos[$atributo]; ?>
					</th>
<?php
					}
                    
 ?>
                    <?php
                if($_SESSION['login'] =='admin'){ 
            ?>
                    <th>Opciones</th>
                        
            <?php
                }
            ?>
                    <th>Grupos</th>
            <?php
                
                    if($_SESSION['login']!="admin"){
                        
                   
            ?>
                    <th>Participar</th>
                        
            <?php
                 }
                 if($_SESSION['login']=="admin"){
            ?>
                <th>Finalizar fase y crear cruces</th>
            <?php        
                 }
            ?>
                        
				</tr>
<?php
				while ( $fila = mysqli_fetch_array( $this->valores ) ) { //este bucle va a devolver todas las tuplas de la tabla USUARIO de la base de datos

?>
				<tr>
<?php
					foreach ( $lista as $atributo ) { //este bucle va a ir devolviendo el valor de cada campo de la tabal usuario
?>
					<td>
<?php 
	               if($atributo =="DESCRIPCION_CATEGORIA"){
                       ?>
                        
                       <textarea class="form-control" rows="3" cols="50" style = "resize:none" readonly><?php echo $fila[ $atributo ]; ?></textarea>

                        <?php
                   }
                        else{
                            
                             echo $fila[ $atributo ];
                        }
                        
                       

?>
					</td>
<?php
					}
                    if($_SESSION['login'] =='admin'){
                        
                  
?>
                    <td>
						<form action="../Controllers/CATEGORIA_CONTROLLER.php" method="get" style="display:inline" >
                            
                            <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
						    <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
						    
                       
					
						  <button class="btn btn-default" type="submit" name="action" value="DELETE" ><i class="fas fa-trash-alt"></i></button>
                            </form>
                        
					</td>
                    
            <?php
                 }       
                        
                ?>
                            <td>
						<form action="../Controllers/GRUPOS_CONTROLLER.php" method="get" style="display:inline" >
                            
                            <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
							<input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
							<input type="hidden" name="descripcion_categoria" value="<?php echo $fila['DESCRIPCION_CATEGORIA']; ?>">
                            <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
						  
					
						  <button class="btn btn-default" type="submit" name="action" ><i class="fas fa-users"></i></button>
                            </form>
                        
					</td>
                    
                         
                    
    <?php
                 
                if($_SESSION['login']!="admin"){
                        
                    $fecha_actual=date("d/m/Y");
                    $porciones = explode("/", $fecha_actual);
                    $dia_actual= intval($porciones[0]);
                    $mes_actual= intval($porciones[1]);
                    $ano_actual= intval($porciones[2]);
                    
                    $partes=explode("/", $this->fecha_limite);
                    $dia_limite= intval($partes[0]);
                    $mes_limite= intval($partes[1]);
                    $ano_limite= intval($partes[2]);
                    
                   
                    
                    if($dia_actual <= $dia_limite && $mes_actual <= $mes_limite && $ano_actual <= $ano_limite){
                        
                
    ?>
                    
            <td>
                <form action="../Controllers/PAREJA_CONTROLLER.php" method="get" style="display:inline">
                    <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
                    <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                    <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
                    
                    
                    <button class="btn btn-default" type="submit" name="action"><i class="fas fa-handshake"></i></button>
                </form>
            
            </td>
         <?php
                }
            }
        ?>
        <?php
        if($_SESSION['login']=="admin"){
        ?>
            <td>
                <form action="../Controllers/CRUCES_CONTROLLER.php" method="get">
                    <input type="hidden" name="nombre_campeonato" value="<?php echo $fila['NOMBRE_CAMPEONATO']; ?>">
                    <input type="hidden" name="nombre_categoria" value="<?php echo $fila['NOMBRE_CATEGORIA']; ?>">
                    <input type="hidden" name="nivel" value="<?php echo $fila['NIVEL']; ?>">
                    <button class="btn btn-default" type="submit" name="action" value="GENERAR_CRUCES"><i class="fas fa-plus"></i></button>
                </form>
            </td>
        <?php
        }
        ?>
					   
				</tr>
<?php
				
    }
        
?>
        </table>
    </div>
  </section><!-- #hero -->

 

<?php
		include '../Views/footer.php';//incluimos el footer
	} //fin metodo render

	} //fin Login

?>