<?php

//es la clase DELETE de USUARIO que nos permite borrar un usuario
class CAMPEONATO_DELETE {
//es el constructor de la clase USUARIO_DELETE
	function __construct($valores,$campeonato) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        
		$this->render( $this->valores,$campeonato);//llamamos a la función render donde se mostrará el formulario DELETE con los campos correspondientes
	}
//funcion que mostrará el formulario DELETE con los campos correspondientes
	function render( $valores,$campeonato) { 
		$this->valores = $valores;//pasamos los valores de cada uno de los campos
        $this->campeonato=$campeonato;
		include_once '../Views/header.php';//incluimos la cabecera
?>
<section class="wow fadeIn">
    <div class="container">
      	<h2>Borrar Campeonato: <?php echo $this->campeonato ?></h2>
		<form name="ADD" action="../Controllers/CAMPEONATO_CONTROLLER.php" method="post" enctype="multipart/form-data" onsubmit="return esVacio(nombre_campeonato) && comprobarExpresionRegular(nombre_campeonato,/^([A-Za-zá-úÁ-Ú]+\s*)+$/) && esVacio(fecha_inicial) && esVacio(fecha_final) && esVacio(descripcion_campeonato) && esVacio(fecha_limite_inscripcion)">
			<div class="form-group">
				<label>Nombre Campeonato</label>
				<input class="form-control" type="text" id="nombre_campeonato" name="nombre_campeonato" value="<?php echo $this->valores['NOMBRE_CAMPEONATO'];?>" maxlength="50" readonly />
			</div>
			<div class="form-group">
				<label>Fecha inicio</label>
				<input class="form-control tcal" type="text" id="fecha_inicial" name="fecha_inicial" value="<?php echo $this->valores['FECHA_INICIO'];?>" maxlength="20" readonly />
			</div>
			<div class="form-group">
				<label>Fecha final</label>
				<input class="form-control tcal" type="text" id="fecha_final" name="fecha_final" value="<?php echo $this->valores['FECHA_FINAL'];   ?>" maxlength="20" readonly/>
			</div>
			<div class="form-group">
				<label>Descripcion campeonato</label>
				<textarea class="form-control" type="text" id="descripcion_campeonato" name="descripcion_campeonato" maxlength="100" size="100" readonly><?php echo $this->valores['DESCRIPCION_CAMPEONATO'];?></textarea>
			</div>
			<div class="form-group">
				<label>Fecha limite inscripcion</label>   
				<input class="form-control tcal" type="text" id="fecha_limite_inscripcion" name="fecha_limite_inscripcion"  value="<?php echo $this->valores['FECHA_LIMITE_INSCRIPCION'];?>"  maxlength="20" readonly />
			</div>
			<button class="btn btn-default" type="submit" name="action" value="DELETE">Delete <i class="fas fa-trash-alt"></i></button>
		</form>		
	</div>
</section>
<?php
        include '../Views/footer.php';//incluimos el footer
            }
		
                
     }
        
	


?>