-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-01-2019 a las 14:51:24
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

DROP DATABASE IF EXISTS OUPADEL;
CREATE DATABASE OUPADEL;
USE OUPADEL;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `oupadel`
--

CREATE DATABASE IF NOT EXISTS `oupadel` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `oupadel`;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campeonato`
--

CREATE TABLE `campeonato` (
  `NOMBRE_CAMPEONATO` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FECHA_INICIO` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FECHA_FINAL` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DESCRIPCION_CAMPEONATO` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FECHA_LIMITE_INSCRIPCION` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `campeonato`
--

INSERT INTO `campeonato` (`NOMBRE_CAMPEONATO`, `FECHA_INICIO`, `FECHA_FINAL`, `DESCRIPCION_CAMPEONATO`, `FECHA_LIMITE_INSCRIPCION`) VALUES
('campeonato oupadel', '15/01/2019', '06/02/2019', 'campeonato realizado desde enero a febrero', '15/01/2019'),
('especial padel', '15/01/2019', '31/01/2019', 'campeonato padel que se realiza sólo en enero', '15/01/2019');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `NOMBRE_CATEGORIA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DESCRIPCION_CATEGORIA` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOMBRE_CAMPEONATO` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NIVEL` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`NOMBRE_CATEGORIA`, `DESCRIPCION_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) VALUES
('Femenino', 'solo mujeres', 'campeonato oupadel', '2'),
('Masculino', 'solo hombres', 'campeonato oupadel', '1'),
('Masculino', 'solo hombres', 'especial padel', '3'),
('Mixto', 'tanto hombres como mujeres', 'campeonato oupadel', '3'),
('Mixto', 'tanto hombres como mujeres\r\n', 'especial padel', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase`
--

CREATE TABLE `clase` (
  `ID_CLASE` int(10) NOT NULL,
  `FECHA_CLASE` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HORA_CLASE` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOMBRE_ESCUELA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_PISTA` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clase`
--

INSERT INTO `clase` (`ID_CLASE`, `FECHA_CLASE`, `HORA_CLASE`, `NOMBRE_ESCUELA`, `ID_PISTA`) VALUES
(33, '21/01/2019', '16:00', 'Entrenamiento físico', NULL),
(34, '23/01/2019', '20:30', 'Entrenamiento físico', NULL),
(35, '30/01/2019', '19:00', 'Entrenamiento básico de pádel', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE `contenido` (
  `ID_CONTENIDO` int(10) NOT NULL,
  `TEXTO` text CHARACTER SET latin1 NOT NULL,
  `TITULO` varchar(100) CHARACTER SET latin1 NOT NULL,
  `FECHA_PUBLICACION` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`ID_CONTENIDO`, `TEXTO`, `TITULO`, `FECHA_PUBLICACION`) VALUES
(1, 'buenos dias ashifwer', 'quedada', '13/12/2018 17:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruces`
--

CREATE TABLE `cruces` (
  `ID_GRUPO` int(10) NOT NULL,
  `NOMBRE_CAMPEONATO` varchar(50) CHARACTER SET latin1 NOT NULL,
  `NOMBRE_CATEGORIA` varchar(50) CHARACTER SET latin1 NOT NULL,
  `NIVEL` varchar(20) CHARACTER SET latin1 NOT NULL,
  `ID_PARTIDO` int(10) NOT NULL DEFAULT '0',
  `ID_PAREJA_1` int(11) DEFAULT NULL,
  `ID_PAREJA_2` int(10) DEFAULT NULL,
  `RESULTADO` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GANADOR` int(10) DEFAULT NULL,
  `FASE` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `FECHA` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HORA` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `PISTA` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cruces`
--

INSERT INTO `cruces` (`ID_GRUPO`, `NOMBRE_CAMPEONATO`, `NOMBRE_CATEGORIA`, `NIVEL`, `ID_PARTIDO`, `ID_PAREJA_1`, `ID_PAREJA_2`, `RESULTADO`, `GANADOR`, `FASE`, `FECHA`, `HORA`, `PISTA`) VALUES
(1, 'Campeonato especial OuPadel', 'Masculino', '1', 0, 3, 51, '6-1/3-6/6-3', 3, 'CUARTOS', NULL, NULL, NULL),
(1, 'Campeonato especial OuPadel', 'Masculino', '1', 1, 2, 7, '6-1/2-6/6-4', 2, 'CUARTOS', NULL, NULL, NULL),
(1, 'Campeonato especial OuPadel', 'Masculino', '1', 2, 10, 6, NULL, NULL, 'CUARTOS', NULL, NULL, NULL),
(1, 'Campeonato especial OuPadel', 'Masculino', '1', 3, 14, 9, NULL, NULL, 'CUARTOS', NULL, NULL, NULL),
(1, 'Campeonato especial OuPadel', 'Masculino', '1', 4, 3, 2, NULL, NULL, 'SEMIFINALES', NULL, NULL, NULL),
(1, 'Campeonato especial OuPadel', 'Masculino', '1', 5, NULL, NULL, NULL, NULL, 'SEMIFINALES', NULL, NULL, NULL),
(1, 'Campeonato especial OuPadel', 'Masculino', '1', 6, NULL, NULL, NULL, NULL, 'FINAL', NULL, NULL, NULL),
(2, 'Campeonato especial OuPadel', 'Masculino', '1', 0, 16, 49, NULL, NULL, 'CUARTOS', NULL, NULL, NULL),
(2, 'Campeonato especial OuPadel', 'Masculino', '1', 1, 53, 48, NULL, NULL, 'CUARTOS', NULL, NULL, NULL),
(2, 'Campeonato especial OuPadel', 'Masculino', '1', 2, 44, 47, NULL, NULL, 'CUARTOS', NULL, NULL, NULL),
(2, 'Campeonato especial OuPadel', 'Masculino', '1', 3, 45, 46, NULL, NULL, 'CUARTOS', NULL, NULL, NULL),
(2, 'Campeonato especial OuPadel', 'Masculino', '1', 4, NULL, NULL, NULL, NULL, 'SEMIFINALES', NULL, NULL, NULL),
(2, 'Campeonato especial OuPadel', 'Masculino', '1', 5, NULL, NULL, NULL, NULL, 'SEMIFINALES', NULL, NULL, NULL),
(2, 'Campeonato especial OuPadel', 'Masculino', '1', 6, NULL, NULL, NULL, NULL, 'FINAL', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deportista`
--

CREATE TABLE `deportista` (
  `NOMBRE` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `APELLIDO` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TELEFONO` int(9) NOT NULL,
  `LOGIN` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EMAIL` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PASSWORD` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SEXO` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `deportista`
--

INSERT INTO `deportista` (`NOMBRE`, `APELLIDO`, `TELEFONO`, `LOGIN`, `EMAIL`, `PASSWORD`, `SEXO`) VALUES
('a', 'a', 458998859, 'a', '', 'a', 'Masculino'),
('', '', 0, 'admin', '', 'admin', ''),
('aldara', 'jksdjkf', 872327837, 'aldara', 'aldara@gmail.com', 'aldara', 'Femenino'),
('Alexia', 'Segura Carmona', 654321987, 'Alexia77', '', 'lK9PoiN6', 'Femenino'),
('alfre', 'alfre', 782327347, 'alfre', 'alfre@gmail.com', 'alfre', 'Masculino'),
('alfred', 'alfred', 878734873, 'alfred', 'alfred@gmail.com', 'alfred', 'Masculino'),
('am', 'am', 834890598, 'am', '', 'am', 'Masculino'),
('amm', 'amm', 874387578, 'amm', '', 'ammm', 'Masculino'),
('Amparo', 'Trueba Mejia', 665555124, 'Ampharos', '', 'As2gAg3J', 'Femenino'),
('Ana', 'Valles Galvan', 665874122, 'Anita', '', '3256874', 'Femenino'),
('Beatriz', 'Varas Manas', 665547778, 'Bea78', '', '12cad3Z', 'Femenino'),
('Beatriz', 'Pons Munoz', 665654685, 'Bellatrix', '', '5Vm8Vz1C3', 'Femenino'),
('bm', 'bm', 566666666, 'bm', '', 'b,', 'Masculino'),
('bmm', 'bmm', 834889533, 'bmm', '', 'bmm', 'Masculino'),
('Brais', 'Santos Negreira', 983863869, 'brais', 'santosbrais@gmail.com', 'brais', 'Masculino'),
('Candela', 'Domenech Prieto', 665985321, 'CandyCrush', '', 'crush12387', 'Femenino'),
('carlos', 'carlos', 783782787, 'carlos', 'carlos@gmail.com', 'carlos', 'Masculino'),
('cm', 'cm', 329889382, 'cm', '', 'cm', 'Masculino'),
('cmm', 'cmm', 834787834, 'cmm', '', 'cmm', 'Masculino'),
('Miguel', 'Cubero Perello', 665644824, 'Cubero', '', 'lk123dDa', 'Masculino'),
('dm', 'dm', 545355335, 'dm', '', 'dm', 'Masculino'),
('dmm', 'dmm', 342222222, 'dmm', '', 'dmm', 'Masculino'),
('Elena', 'Granell Encinas', 654789123, 'Elen', '', 'El87HdZ', 'Femenino'),
('Luis Miguel', 'Balaguer Benavent', 698573214, 'ElLuismi', '', 'dasCo120', 'Masculino'),
('Elsa', 'Fernandez Ramirez', 654231885, 'Elsa', '', 'ijuhygtf65', 'Femenino'),
('em', 'em', 327832787, 'em', '', 'em', 'Masculino'),
('emm', 'emm', 344543535, 'emm', '', 'emm', 'Masculino'),
('fm', 'fm', 892834989, 'fm', '', 'fm', 'Masculino'),
('fmm', 'fmm', 325325332, 'fmm', '', 'fmm', 'Masculino'),
('Gabriel', 'Perez Garrido', 661142335, 'Gabo', '', '1a2s3d4f', 'Masculino'),
('Gerardo', 'Grau Medina', 665664123, 'Gery', '', '6hZxc3Mha9', 'Masculino'),
('gm', 'gm', 344535335, 'gm', '', 'gm', 'Masculino'),
('gmm', 'gmm', 444444444, 'gmm', '', 'gmm', 'Masculino'),
('Elena', 'Hernandez Garrido', 632112458, 'Helen', '', 'pokl876Fg', 'Femenino'),
('hm', 'hm', 978987978, 'hm', '', 'hm', 'Masculino'),
('hmm', 'hmm', 454565456, 'hmm', '', 'hmm', 'Masculino'),
('im', 'im', 898778787, 'im', '', 'im', 'Masculino'),
('Inmaculada', 'Almazan Mestres', 66112312, 'InmaAlma', '', '1hGf87Q', 'Femenino'),
('Isabel', 'Diaz Lorenzo', 654147369, 'IsaasI', '', 'lK896GGha', 'Femenino'),
('Ismael', 'Toral Oliva', 618453129, 'Isma89', '', 'Is89ToV', 'Masculino'),
('Aitana', 'Castillo Vega', 654982328, 'Itsaitana', '', 'asd123gt65', 'Femenino'),
('Ivan', 'Hervella Tunas', 792874295, 'ivan', '', 'ivan', 'Masculino'),
('Javier', 'Melgar Pereira', 685471147, 'JaviPadel', '', 'JMP1252', 'Masculino'),
('jm', 'jm', 879788787, 'jm', '', 'jm', 'Masculino'),
('jmm', 'jmm', 999888888, 'jmm', '', 'jmm', 'Masculino'),
('Joel', 'Ferrer Diaz', 654332597, 'Jo', '', 'Hf92lZb', 'Masculino'),
('Joan', 'Iglesias Moya', 657884235, 'JoanI', '', 'Hd93mCa02Ha', 'Masculino'),
('Jorge', 'Casado Olle', 654985324, 'Jorge94', '', 'Gorje49', 'Masculino'),
('Juan Carlos', 'Revuelta Crespo', 654987321, 'JuanKA', '', 'JK098mnZ', 'Masculino'),
('Arturo', 'Sanz Gimenez', 665325994, 'KingArthur', '', 'Kl87Gzc1', 'Masculino'),
('km', 'km', 849389894, 'km', '', 'km', 'Masculino'),
('kmm', 'kmm', 788888888, 'kmm', '', 'kmm', 'Masculino'),
('Leo', 'Ruiz Soler', 698852331, 'Leo', '', 'KhJfG5564', 'Masculino'),
('Lidia', 'Hidalgo Fernandez', 665445885, 'Lidiiia', '', 'JhGfDs', 'Femenino'),
('Mario', 'Pina Nieves', 658642631, 'Luigi86', '', '145Zb7Aw', 'Masculino'),
('Luna', 'Serrano Pastor', 674852634, 'LuSePa', '', 'l1u2n3a4', 'Femenino'),
('marcos', 'marcos', 783277378, 'marcos', 'marcos@gmail.com', 'marcos', 'Masculino'),
('Magarita', 'Sarrion Cantero', 675482112, 'Marga90', '', 'a4sF9wH', 'Femenino'),
('maria', 'maria', 384774857, 'maria', 'maria@gmail.com', 'maria', 'Femenino'),
('Maria Carmen', 'Espinoza Gandara', 667778951, 'MariCarmen', '', '1rG57Sc3', 'Femenino'),
('Marina', 'Castro Herrera', 659645632, 'Marinera', '', 'l09jGh6A', 'Femenino'),
('Jose Ramon', 'Funes Holguin', 658794123, 'Monchino', '', 'asDq1341Q', 'Masculino'),
('Martin', 'Varela Garcia', 953890859, 'mvgarcia4', '', 'martin', 'Masculino'),
('Nerea', 'Gallego Castillo', 661123145, 'NereGC', '', 'Ne1234re', 'Femenino'),
('Nuria', 'Moyano Valdes', 698741235, 'NuriMV', '', 'q13wert6', 'Femenino'),
('Raquel', 'Roig Dominguez', 665411234, 'Raqueta', '', 'K8hBv5a6', 'Femenino'),
('Richi', 'Gilabert Tena', 695321457, 'Richy', '', '1a2b3c4', 'Masculino'),
('Manuel', 'Romero Delgado', 664889531, 'Romano', '', '96846HHGs7A', 'Masculino'),
('Rosa', 'Tirado Armenteros', 689665689, 'RosiTA', '', 'Asd24aQwE', 'Femenino'),
('Santiago', 'Iglesias Hidalgo', 661233578, 'Santih', '', 'NNmh87g4A1', 'Masculino'),
('Sergio', 'Martinez Viso', 903904204, 'sergio', '', 'sergio', 'Masculino'),
('sofi', 'sofi', 882387328, 'sofi', 'sofi@gmail.com', 'sofi', 'Femenino'),
('Sofia', 'Tendero Hernan', 666333999, 'Sofy92', '', 'Ag521Zoq', 'Femenino'),
('tsw', 'tse', 783287472, 'tsw', 'tsw@gmail.com', 'tsw', 'Masculino'),
('uxia', 'uxia', 362478344, 'uxia', 'uxia@gmail.com', 'uxia', 'Femenino'),
('Guillermo', 'Rial Calabuig', 664125458, 'WillyRex', '', '2Re4v6lQ', 'Masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deportista_clase`
--

CREATE TABLE `deportista_clase` (
  `LOGIN` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_CLASE` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `deportista_clase`
--

INSERT INTO `deportista_clase` (`LOGIN`, `ID_CLASE`) VALUES
('brais', 35),
('ivan', 35),
('mvgarcia4', 35),
('tsw', 35);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deportista_pareja`
--

CREATE TABLE `deportista_pareja` (
  `LOGIN` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_PAREJA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `deportista_pareja`
--

INSERT INTO `deportista_pareja` (`LOGIN`, `ID_PAREJA`) VALUES
('aldara', 113),
('aldara', 114),
('Alexia77', 57),
('Alexia77', 61),
('Alexia77', 114),
('alfre', 100),
('alfre', 101),
('alfre', 123),
('alfred', 100),
('alfred', 123),
('am', 106),
('am', 107),
('amm', 106),
('Ampharos', 115),
('Bea78', 63),
('Bea78', 65),
('Bea78', 116),
('Bellatrix', 116),
('bm', 110),
('bmm', 110),
('bmm', 111),
('brais', 57),
('brais', 58),
('brais', 60),
('brais', 61),
('CandyCrush', 107),
('carlos', 98),
('carlos', 99),
('cm', 108),
('cm', 109),
('cmm', 108),
('Cubero', 66),
('Cubero', 67),
('Cubero', 68),
('Cubero', 69),
('dm', 112),
('dm', 113),
('dmm', 112),
('Elen', 109),
('ElLuismi', 66),
('ElLuismi', 68),
('Elsa', 67),
('Elsa', 69),
('Elsa', 117),
('Gabo', 70),
('Gabo', 72),
('Gery', 90),
('Gery', 91),
('Helen', 71),
('Helen', 73),
('Helen', 117),
('hm', 102),
('hm', 103),
('hmm', 102),
('im', 104),
('im', 105),
('InmaAlma', 93),
('IsaasI', 87),
('IsaasI', 89),
('Isma89', 92),
('ivan', 62),
('ivan', 63),
('ivan', 64),
('ivan', 65),
('JaviPadel', 82),
('JaviPadel', 83),
('JaviPadel', 84),
('JaviPadel', 85),
('jm', 96),
('jm', 104),
('Jo', 82),
('Jo', 84),
('JoanI', 90),
('JuanKA', 86),
('JuanKA', 88),
('KingArthur', 94),
('KingArthur', 95),
('km', 94),
('kmm', 96),
('kmm', 97),
('Leo', 86),
('Leo', 87),
('Leo', 88),
('Leo', 89),
('Lidiiia', 83),
('Lidiiia', 85),
('Lidiiia', 118),
('Luigi86', 78),
('Luigi86', 81),
('LuSePa', 103),
('LuSePa', 118),
('marcos', 98),
('Marga90', 91),
('maria', 122),
('MariCarmen', 95),
('MariCarmen', 119),
('Marinera', 79),
('Marinera', 80),
('Marinera', 119),
('Monchino', 74),
('Monchino', 75),
('Monchino', 76),
('Monchino', 77),
('mvgarcia4', 62),
('mvgarcia4', 64),
('NereGC', 97),
('NuriMV', 105),
('NuriMV', 120),
('Raqueta', 99),
('Raqueta', 120),
('Richy', 74),
('Richy', 76),
('Romano', 78),
('Romano', 79),
('Romano', 80),
('Romano', 81),
('RosiTA', 75),
('RosiTA', 77),
('RosiTA', 121),
('sergio', 58),
('sergio', 60),
('sofi', 111),
('sofi', 115),
('Sofy92', 101),
('Sofy92', 121),
('tsw', 70),
('tsw', 71),
('tsw', 72),
('tsw', 73),
('uxia', 122),
('WillyRex', 92),
('WillyRex', 93);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deportista_partido`
--

CREATE TABLE `deportista_partido` (
  `LOGIN` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_PARTIDO` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `deportista_partido`
--

INSERT INTO `deportista_partido` (`LOGIN`, `ID_PARTIDO`) VALUES
('brais', 1),
('ivan', 1),
('sergio', 1),
('tsw', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfrentamiento`
--

CREATE TABLE `enfrentamiento` (
  `ID_ENFRENTAMIENTO` int(11) NOT NULL,
  `NOMBRE_CAMPEONATO` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOMBRE_CATEGORIA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_GRUPO` int(11) NOT NULL,
  `RESULTADO` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FECHA_ENFRENTAMIENTO` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HORA_ENFRENTAMIENTO` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ID_PISTA` int(11) DEFAULT NULL,
  `NIVEL` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `enfrentamiento`
--

INSERT INTO `enfrentamiento` (`ID_ENFRENTAMIENTO`, `NOMBRE_CAMPEONATO`, `NOMBRE_CATEGORIA`, `ID_GRUPO`, `RESULTADO`, `FECHA_ENFRENTAMIENTO`, `HORA_ENFRENTAMIENTO`, `ID_PISTA`, `NIVEL`) VALUES
(421, 'campeonato oupadel', 'Masculino', 1, '6-2/6-4', '15/01/2019', '10:00', 1, '1'),
(422, 'campeonato oupadel', 'Masculino', 1, '6-2/6-4', '17/01/2019', '10:00', 1, '1'),
(423, 'campeonato oupadel', 'Masculino', 1, '6-2/6-4', '18/01/2019', '10:00', 1, '1'),
(424, 'campeonato oupadel', 'Masculino', 1, '0-6/6-3/6-2', '16/01/2019', '16:00', 1, '1'),
(425, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '17/01/2019', '10:00', 2, '1'),
(426, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '24/01/2019', '20:30', 1, '1'),
(427, 'campeonato oupadel', 'Masculino', 1, '0-6/6-3/6-2', '23/01/2019', '10:00', 1, '1'),
(428, 'campeonato oupadel', 'Masculino', 1, '0-6/6-3/6-2', '23/01/2019', '13:00', 1, '1'),
(429, 'campeonato oupadel', 'Masculino', 1, '0-6/6-3/6-2', '16/01/2019', '20:30', 1, '1'),
(430, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '22/01/2019', '10:00', 1, '1'),
(431, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '18/01/2019', '16:00', 1, '1'),
(432, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(433, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '16/01/2019', '10:00', 1, '1'),
(434, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '21/01/2019', '10:00', 1, '1'),
(435, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(436, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(437, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(438, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(439, 'campeonato oupadel', 'Masculino', 1, '6-2/6-4', '16/01/2019', '10:00', 2, '1'),
(440, 'campeonato oupadel', 'Masculino', 1, '6-2/6-4', '17/01/2019', '16:00', 1, '1'),
(441, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '16/01/2019', '10:00', 3, '1'),
(442, 'campeonato oupadel', 'Masculino', 1, '2-6/4-6', '18/01/2019', '20:30', 1, '1'),
(443, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(444, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(445, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(446, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(447, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(448, 'campeonato oupadel', 'Masculino', 1, NULL, NULL, NULL, NULL, '1'),
(449, 'campeonato oupadel', 'Mixto', 1, NULL, '17/01/2019', '14:30', 1, '3'),
(450, 'campeonato oupadel', 'Mixto', 1, NULL, '22/01/2019', '19:00', 1, '3'),
(451, 'campeonato oupadel', 'Mixto', 1, NULL, '17/01/2019', '20:30', 1, '3'),
(452, 'campeonato oupadel', 'Mixto', 1, NULL, '21/01/2019', '11:30', 1, '3'),
(453, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(454, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(455, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(456, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(457, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(458, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(459, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(460, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(461, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(462, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(463, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(464, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(465, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(466, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(467, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(468, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(469, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(470, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(471, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(472, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(473, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(474, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(475, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(476, 'campeonato oupadel', 'Mixto', 1, NULL, NULL, NULL, NULL, '3'),
(543, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(544, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(545, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(546, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(547, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(548, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(549, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(550, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(551, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(552, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(553, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(554, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(555, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(556, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(557, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(558, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(559, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(560, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(561, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(562, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(563, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(564, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(565, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(566, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(567, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(568, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(569, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(570, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(571, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(572, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(573, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(574, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(575, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(576, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(577, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(578, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(579, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(580, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(581, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(582, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(583, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(584, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(585, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(586, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(587, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(588, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(589, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(590, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(591, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(592, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(593, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(594, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(595, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(596, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(597, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(598, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(599, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(600, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(601, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(602, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(603, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(604, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(605, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(606, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(607, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3'),
(608, 'especial padel', 'Masculino', 1, NULL, NULL, NULL, NULL, '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuela_deportiva`
--

CREATE TABLE `escuela_deportiva` (
  `NOMBRE_ESCUELA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DESCRIPCION` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TIPO_CLASE` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `escuela_deportiva`
--

INSERT INTO `escuela_deportiva` (`NOMBRE_ESCUELA`, `DESCRIPCION`, `TIPO_CLASE`) VALUES
('Entrenamiento básico de pádel', 'Aquí­ os enseñaremos las bases del pádel. Este curso está pensado para los recién iniciados en este deporte.', 'Pádel'),
('Entrenamiento físico', 'Esta escuela está pensada para aquellos que además del pádel queréis realizar un entrenamiento fí­sico para manteneros en forma.', 'Físico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fecha_propuesta`
--

CREATE TABLE `fecha_propuesta` (
  `ID_FECHA_PROPUESTA` int(11) NOT NULL,
  `FECHA_PROPUESTA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HORA_PROPUESTA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_ENFRENTAMIENTO` int(11) DEFAULT NULL,
  `NOMBRE_CAMPEONATO` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NOMBRE_CATEGORIA` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ID_GRUPO` int(11) DEFAULT NULL,
  `NIVEL` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ID_PAREJA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `fecha_propuesta`
--

INSERT INTO `fecha_propuesta` (`ID_FECHA_PROPUESTA`, `FECHA_PROPUESTA`, `HORA_PROPUESTA`, `ID_ENFRENTAMIENTO`, `NOMBRE_CAMPEONATO`, `NOMBRE_CATEGORIA`, `ID_GRUPO`, `NIVEL`, `ID_PAREJA`) VALUES
(7, '19/01/2019', '10:00', 421, 'campeonato oupadel', 'Masculino', 1, '1', 58),
(8, '15/01/2019', '10:00', 421, 'campeonato oupadel', 'Masculino', 1, '1', 62),
(9, '16/01/2019', '10:00', 428, 'campeonato oupadel', 'Masculino', 1, '1', 62),
(10, '16/01/2019', '20:30', 429, 'campeonato oupadel', 'Masculino', 1, '1', 62),
(11, '17/01/2019', '10:00', 422, 'campeonato oupadel', 'Masculino', 1, '1', 58),
(12, '18/01/2019', '10:00', 423, 'campeonato oupadel', 'Masculino', 1, '1', 58),
(13, '21/01/2019', '10:00', 434, 'campeonato oupadel', 'Masculino', 1, '1', 66),
(14, '16/01/2019', '16:00', 424, 'campeonato oupadel', 'Masculino', 1, '1', 58),
(15, '17/01/2019', '10:00', 425, 'campeonato oupadel', 'Masculino', 1, '1', 58),
(16, '24/01/2019', '20:30', 426, 'campeonato oupadel', 'Masculino', 1, '1', 58),
(17, '23/01/2019', '10:00', 427, 'campeonato oupadel', 'Masculino', 1, '1', 58),
(18, '23/01/2019', '13:00', 428, 'campeonato oupadel', 'Masculino', 1, '1', 62),
(19, '22/01/2019', '10:00', 430, 'campeonato oupadel', 'Masculino', 1, '1', 62),
(20, '18/01/2019', '16:00', 431, 'campeonato oupadel', 'Masculino', 1, '1', 62),
(21, '16/01/2019', '10:00', 433, 'campeonato oupadel', 'Masculino', 1, '1', 62),
(22, '16/01/2019', '10:00', 439, 'campeonato oupadel', 'Masculino', 1, '1', 70),
(23, '17/01/2019', '16:00', 440, 'campeonato oupadel', 'Masculino', 1, '1', 70),
(24, '16/01/2019', '10:00', 441, 'campeonato oupadel', 'Masculino', 1, '1', 70),
(25, '18/01/2019', '20:30', 442, 'campeonato oupadel', 'Masculino', 1, '1', 70),
(26, '17/01/2019', '14:30', 449, 'campeonato oupadel', 'Mixto', 1, '3', 57),
(27, '22/01/2019', '19:00', 450, 'campeonato oupadel', 'Mixto', 1, '3', 57),
(28, '17/01/2019', '20:30', 451, 'campeonato oupadel', 'Mixto', 1, '3', 57),
(29, '21/01/2019', '11:30', 452, 'campeonato oupadel', 'Mixto', 1, '3', 57);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `ID_GRUPO` int(11) NOT NULL,
  `NOMBRE_CATEGORIA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOMBRE_CAMPEONATO` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NIVEL` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`ID_GRUPO`, `NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) VALUES
(1, 'Femenino', 'campeonato oupadel', '2'),
(1, 'Masculino', 'campeonato oupadel', '1'),
(1, 'Masculino', 'especial padel', '3'),
(1, 'Mixto', 'campeonato oupadel', '3'),
(1, 'Mixto', 'especial padel', '2'),
(2, 'Masculino', 'campeonato oupadel', '1'),
(2, 'Mixto', 'campeonato oupadel', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `FECHA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HORA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DIA` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`FECHA`, `HORA`, `DIA`) VALUES
('2019-01-15', '10:00', 'Martes'),
('2019-01-15', '11:30', 'Martes'),
('2019-01-15', '13:00', 'Martes'),
('2019-01-15', '14:30', 'Martes'),
('2019-01-15', '16:00', 'Martes'),
('2019-01-15', '17:30', 'Martes'),
('2019-01-15', '19:00', 'Martes'),
('2019-01-15', '20:30', 'Martes'),
('2019-01-16', '10:00', 'Miercoles'),
('2019-01-16', '11:30', 'Miercoles'),
('2019-01-16', '13:00', 'Miercoles'),
('2019-01-16', '14:30', 'Miercoles'),
('2019-01-16', '16:00', 'Miercoles'),
('2019-01-16', '17:30', 'Miercoles'),
('2019-01-16', '19:00', 'Miercoles'),
('2019-01-16', '20:30', 'Miercoles'),
('2019-01-17', '10:00', 'Jueves'),
('2019-01-17', '11:30', 'Jueves'),
('2019-01-17', '13:00', 'Jueves'),
('2019-01-17', '14:30', 'Jueves'),
('2019-01-17', '16:00', 'Jueves'),
('2019-01-17', '17:30', 'Jueves'),
('2019-01-17', '19:00', 'Jueves'),
('2019-01-17', '20:30', 'Jueves'),
('2019-01-18', '10:00', 'Viernes'),
('2019-01-18', '11:30', 'Viernes'),
('2019-01-18', '13:00', 'Viernes'),
('2019-01-18', '14:30', 'Viernes'),
('2019-01-18', '16:00', 'Viernes'),
('2019-01-18', '17:30', 'Viernes'),
('2019-01-18', '19:00', 'Viernes'),
('2019-01-18', '20:30', 'Viernes'),
('2019-01-21', '10:00', 'Lunes'),
('2019-01-21', '11:30', 'Lunes'),
('2019-01-21', '13:00', 'Lunes'),
('2019-01-21', '14:30', 'Lunes'),
('2019-01-21', '16:00', 'Lunes'),
('2019-01-21', '17:30', 'Lunes'),
('2019-01-21', '19:00', 'Lunes'),
('2019-01-21', '20:30', 'Lunes'),
('2019-01-22', '10:00', 'Martes'),
('2019-01-22', '11:30', 'Martes'),
('2019-01-22', '13:00', 'Martes'),
('2019-01-22', '14:30', 'Martes'),
('2019-01-22', '16:00', 'Martes'),
('2019-01-22', '17:30', 'Martes'),
('2019-01-22', '19:00', 'Martes'),
('2019-01-22', '20:30', 'Martes'),
('2019-01-23', '10:00', 'Miercoles'),
('2019-01-23', '11:30', 'Miercoles'),
('2019-01-23', '13:00', 'Miercoles'),
('2019-01-23', '14:30', 'Miercoles'),
('2019-01-23', '16:00', 'Miercoles'),
('2019-01-23', '17:30', 'Miercoles'),
('2019-01-23', '19:00', 'Miercoles'),
('2019-01-23', '20:30', 'Miercoles'),
('2019-01-24', '10:00', 'Jueves'),
('2019-01-24', '11:30', 'Jueves'),
('2019-01-24', '13:00', 'Jueves'),
('2019-01-24', '14:30', 'Jueves'),
('2019-01-24', '16:00', 'Jueves'),
('2019-01-24', '17:30', 'Jueves'),
('2019-01-24', '19:00', 'Jueves'),
('2019-01-24', '20:30', 'Jueves'),
('2019-01-25', '10:00', 'Viernes'),
('2019-01-25', '11:30', 'Viernes'),
('2019-01-25', '13:00', 'Viernes'),
('2019-01-25', '14:30', 'Viernes'),
('2019-01-25', '16:00', 'Viernes'),
('2019-01-25', '17:30', 'Viernes'),
('2019-01-25', '19:00', 'Viernes'),
('2019-01-25', '20:30', 'Viernes'),
('2019-01-28', '10:00', 'Lunes'),
('2019-01-28', '11:30', 'Lunes'),
('2019-01-28', '13:00', 'Lunes'),
('2019-01-28', '14:30', 'Lunes'),
('2019-01-28', '16:00', 'Lunes'),
('2019-01-28', '17:30', 'Lunes'),
('2019-01-28', '19:00', 'Lunes'),
('2019-01-28', '20:30', 'Lunes'),
('2019-01-29', '10:00', 'Martes'),
('2019-01-29', '11:30', 'Martes'),
('2019-01-29', '13:00', 'Martes'),
('2019-01-29', '14:30', 'Martes'),
('2019-01-29', '16:00', 'Martes'),
('2019-01-29', '17:30', 'Martes'),
('2019-01-29', '19:00', 'Martes'),
('2019-01-29', '20:30', 'Martes'),
('2019-01-30', '10:00', 'Miercoles'),
('2019-01-30', '11:30', 'Miercoles'),
('2019-01-30', '13:00', 'Miercoles'),
('2019-01-30', '14:30', 'Miercoles'),
('2019-01-30', '16:00', 'Miercoles'),
('2019-01-30', '17:30', 'Miercoles'),
('2019-01-30', '19:00', 'Miercoles'),
('2019-01-30', '20:30', 'Miercoles'),
('2019-01-31', '10:00', 'Jueves'),
('2019-01-31', '11:30', 'Jueves'),
('2019-01-31', '13:00', 'Jueves'),
('2019-01-31', '14:30', 'Jueves'),
('2019-01-31', '16:00', 'Jueves'),
('2019-01-31', '17:30', 'Jueves'),
('2019-01-31', '19:00', 'Jueves'),
('2019-01-31', '20:30', 'Jueves'),
('2019-02-01', '10:00', 'Viernes'),
('2019-02-01', '11:30', 'Viernes'),
('2019-02-01', '13:00', 'Viernes'),
('2019-02-01', '14:30', 'Viernes'),
('2019-02-01', '16:00', 'Viernes'),
('2019-02-01', '17:30', 'Viernes'),
('2019-02-01', '19:00', 'Viernes'),
('2019-02-01', '20:30', 'Viernes'),
('2019-02-04', '10:00', 'Lunes'),
('2019-02-04', '11:30', 'Lunes'),
('2019-02-04', '13:00', 'Lunes'),
('2019-02-04', '14:30', 'Lunes'),
('2019-02-04', '16:00', 'Lunes'),
('2019-02-04', '17:30', 'Lunes'),
('2019-02-04', '19:00', 'Lunes'),
('2019-02-04', '20:30', 'Lunes'),
('2019-02-05', '10:00', 'Martes'),
('2019-02-05', '11:30', 'Martes'),
('2019-02-05', '13:00', 'Martes'),
('2019-02-05', '14:30', 'Martes'),
('2019-02-05', '16:00', 'Martes'),
('2019-02-05', '17:30', 'Martes'),
('2019-02-05', '19:00', 'Martes'),
('2019-02-05', '20:30', 'Martes'),
('2019-02-06', '10:00', 'Miercoles'),
('2019-02-06', '11:30', 'Miercoles'),
('2019-02-06', '13:00', 'Miercoles'),
('2019-02-06', '14:30', 'Miercoles'),
('2019-02-06', '16:00', 'Miercoles'),
('2019-02-06', '17:30', 'Miercoles'),
('2019-02-06', '19:00', 'Miercoles'),
('2019-02-06', '20:30', 'Miercoles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario_pista`
--

CREATE TABLE `horario_pista` (
  `ID_PISTA` int(10) NOT NULL,
  `FECHA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HORA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LOGIN` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `horario_pista`
--

INSERT INTO `horario_pista` (`ID_PISTA`, `FECHA`, `HORA`, `LOGIN`) VALUES
(1, '2019-01-16', '16:00', 'admin'),
(1, '2019-01-16', '20:30', 'admin'),
(1, '2019-01-17', '10:00', 'admin'),
(1, '2019-01-17', '14:30', 'admin'),
(1, '2019-01-17', '16:00', 'admin'),
(1, '2019-01-17', '20:30', 'admin'),
(1, '2019-01-18', '10:00', 'admin'),
(1, '2019-01-18', '16:00', 'admin'),
(1, '2019-01-18', '20:30', 'admin'),
(1, '2019-01-21', '10:00', 'admin'),
(1, '2019-01-21', '11:30', 'admin'),
(1, '2019-01-22', '10:00', 'admin'),
(1, '2019-01-22', '19:00', 'admin'),
(1, '2019-01-23', '10:00', 'admin'),
(1, '2019-01-23', '13:00', 'admin'),
(1, '2019-01-24', '14:30', 'admin'),
(1, '2019-01-24', '20:30', 'admin'),
(1, '2019-01-30', '19:00', 'admin'),
(2, '2019-01-16', '10:00', 'admin'),
(2, '2019-01-17', '10:00', 'admin'),
(3, '2019-01-16', '10:00', 'admin'),
(2, '2019-01-18', '10:00', 'brais'),
(3, '2019-01-18', '10:00', 'brais'),
(4, '2019-01-18', '10:00', 'brais'),
(5, '2019-01-18', '10:00', 'sergio'),
(9, '2019-01-18', '10:00', 'sergio'),
(10, '2019-01-18', '10:00', 'sergio'),
(11, '2019-01-18', '10:00', 'sergio'),
(6, '2019-01-18', '10:00', 'tsw'),
(7, '2019-01-18', '10:00', 'tsw'),
(8, '2019-01-18', '10:00', 'tsw');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `mis_clases`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `mis_clases` (
`LOGIN` varchar(20)
,`ID_CLASE` int(10)
,`FECHA_CLASE` varchar(20)
,`HORA_CLASE` varchar(20)
,`NOMBRE_ESCUELA` varchar(50)
,`ID_PISTA` int(10)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `mis_partidos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `mis_partidos` (
`Login` varchar(20)
,`ID_Partido` int(10)
,`Fecha_Partido` varchar(20)
,`Hora_Partido` varchar(20)
,`ID_Pista` int(10)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pareja`
--

CREATE TABLE `pareja` (
  `ID_PAREJA` int(11) NOT NULL,
  `CAPITAN` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LOGIN_PAREJA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PUNTOS` int(11) NOT NULL,
  `NOMBRE_CATEGORIA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOMBRE_CAMPEONATO` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NIVEL` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_GRUPO` int(11) DEFAULT NULL,
  `NOMBRE_CATEGORIA_GRUPO` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NOMBRE_CAMPEONATO_GRUPO` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NIVEL_CATEGORIA_GRUPO` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pareja`
--

INSERT INTO `pareja` (`ID_PAREJA`, `CAPITAN`, `LOGIN_PAREJA`, `PUNTOS`, `NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`, `ID_GRUPO`, `NOMBRE_CATEGORIA_GRUPO`, `NOMBRE_CAMPEONATO_GRUPO`, `NIVEL_CATEGORIA_GRUPO`) VALUES
(57, 'brais', 'Alexia77', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(58, 'brais', 'sergio', 17, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(60, 'brais', 'sergio', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(61, 'brais', 'Alexia77', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(62, 'ivan', 'mvgarcia4', 10, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(63, 'ivan', 'Bea78', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(64, 'ivan', 'mvgarcia4', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(65, 'ivan', 'Bea78', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(66, 'Cubero', 'ElLuismi', 3, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(67, 'Cubero', 'Elsa', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(68, 'Cubero', 'ElLuismi', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(69, 'Cubero', 'Elsa', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(70, 'tsw', 'Gabo', 13, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(71, 'tsw', 'Helen', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(72, 'tsw', 'Gabo', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(73, 'tsw', 'Helen', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(74, 'Monchino', 'Richy', 5, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(75, 'Monchino', 'RosiTA', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(76, 'Monchino', 'Richy', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(77, 'Monchino', 'RosiTA', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(78, 'Romano', 'Luigi86', 7, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(79, 'Romano', 'Marinera', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(80, 'Romano', 'Marinera', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(81, 'Romano', 'Luigi86', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(82, 'JaviPadel', 'Jo', 6, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(83, 'JaviPadel', 'Lidiiia', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(84, 'JaviPadel', 'Jo', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(85, 'JaviPadel', 'Lidiiia', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(86, 'Leo', 'JuanKA', 7, 'Masculino', 'campeonato oupadel', '1', 1, 'Masculino', 'campeonato oupadel', '1'),
(87, 'Leo', 'IsaasI', 0, 'Mixto', 'campeonato oupadel', '3', 1, 'Mixto', 'campeonato oupadel', '3'),
(88, 'Leo', 'JuanKA', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(89, 'Leo', 'IsaasI', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(90, 'Gery', 'JoanI', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(91, 'Gery', 'Marga90', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(92, 'WillyRex', 'Isma89', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(93, 'WillyRex', 'InmaAlma', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(94, 'KingArthur', 'km', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(95, 'KingArthur', 'MariCarmen', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(96, 'kmm', 'jm', 0, 'Masculino', 'especial padel', '3', 1, 'Masculino', 'especial padel', '3'),
(97, 'kmm', 'NereGC', 0, 'Mixto', 'especial padel', '2', NULL, NULL, NULL, NULL),
(98, 'carlos', 'marcos', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(99, 'carlos', 'Raqueta', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(100, 'alfre', 'alfred', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(101, 'alfre', 'Sofy92', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(102, 'hm', 'hmm', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(103, 'hm', 'LuSePa', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(104, 'im', 'jm', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(105, 'im', 'NuriMV', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(106, 'am', 'amm', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(107, 'am', 'CandyCrush', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(108, 'cm', 'cmm', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(109, 'cm', 'Elen', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(110, 'bmm', 'bm', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(111, 'bmm', 'sofi', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(112, 'dm', 'dmm', 0, 'Masculino', 'campeonato oupadel', '1', NULL, NULL, NULL, NULL),
(113, 'dm', 'aldara', 0, 'Mixto', 'campeonato oupadel', '3', 2, 'Mixto', 'campeonato oupadel', '3'),
(114, 'Alexia77', 'aldara', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(115, 'sofi', 'Ampharos', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(116, 'Bea78', 'Bellatrix', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(117, 'Elsa', 'Helen', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(118, 'Lidiiia', 'LuSePa', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(119, 'MariCarmen', 'Marinera', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(120, 'NuriMV', 'Raqueta', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(121, 'RosiTA', 'Sofy92', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(122, 'maria', 'uxia', 0, 'Femenino', 'campeonato oupadel', '2', NULL, NULL, NULL, NULL),
(123, 'alfred', 'alfre', 0, 'Masculino', 'especial padel', '3', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pareja_enfrentamiento`
--

CREATE TABLE `pareja_enfrentamiento` (
  `ID_PAREJA` int(11) NOT NULL,
  `ID_ENFRENTAMIENTO` int(11) NOT NULL,
  `ID_GRUPO` int(11) NOT NULL,
  `NOMBRE_CATEGORIA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOMBRE_CAMPEONATO` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NIVEL` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pareja_enfrentamiento`
--

INSERT INTO `pareja_enfrentamiento` (`ID_PAREJA`, `ID_ENFRENTAMIENTO`, `ID_GRUPO`, `NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) VALUES
(57, 449, 1, 'Mixto', 'campeonato oupadel', '3'),
(57, 450, 1, 'Mixto', 'campeonato oupadel', '3'),
(57, 451, 1, 'Mixto', 'campeonato oupadel', '3'),
(57, 452, 1, 'Mixto', 'campeonato oupadel', '3'),
(57, 453, 1, 'Mixto', 'campeonato oupadel', '3'),
(57, 454, 1, 'Mixto', 'campeonato oupadel', '3'),
(57, 455, 1, 'Mixto', 'campeonato oupadel', '3'),
(58, 421, 1, 'Masculino', 'campeonato oupadel', '1'),
(58, 422, 1, 'Masculino', 'campeonato oupadel', '1'),
(58, 423, 1, 'Masculino', 'campeonato oupadel', '1'),
(58, 424, 1, 'Masculino', 'campeonato oupadel', '1'),
(58, 425, 1, 'Masculino', 'campeonato oupadel', '1'),
(58, 426, 1, 'Masculino', 'campeonato oupadel', '1'),
(58, 427, 1, 'Masculino', 'campeonato oupadel', '1'),
(60, 543, 1, 'Masculino', 'especial padel', '3'),
(60, 544, 1, 'Masculino', 'especial padel', '3'),
(60, 545, 1, 'Masculino', 'especial padel', '3'),
(60, 546, 1, 'Masculino', 'especial padel', '3'),
(60, 547, 1, 'Masculino', 'especial padel', '3'),
(60, 548, 1, 'Masculino', 'especial padel', '3'),
(60, 549, 1, 'Masculino', 'especial padel', '3'),
(60, 550, 1, 'Masculino', 'especial padel', '3'),
(60, 551, 1, 'Masculino', 'especial padel', '3'),
(60, 552, 1, 'Masculino', 'especial padel', '3'),
(60, 553, 1, 'Masculino', 'especial padel', '3'),
(62, 421, 1, 'Masculino', 'campeonato oupadel', '1'),
(62, 428, 1, 'Masculino', 'campeonato oupadel', '1'),
(62, 429, 1, 'Masculino', 'campeonato oupadel', '1'),
(62, 430, 1, 'Masculino', 'campeonato oupadel', '1'),
(62, 431, 1, 'Masculino', 'campeonato oupadel', '1'),
(62, 432, 1, 'Masculino', 'campeonato oupadel', '1'),
(62, 433, 1, 'Masculino', 'campeonato oupadel', '1'),
(63, 449, 1, 'Mixto', 'campeonato oupadel', '3'),
(63, 456, 1, 'Mixto', 'campeonato oupadel', '3'),
(63, 457, 1, 'Mixto', 'campeonato oupadel', '3'),
(63, 458, 1, 'Mixto', 'campeonato oupadel', '3'),
(63, 459, 1, 'Mixto', 'campeonato oupadel', '3'),
(63, 460, 1, 'Mixto', 'campeonato oupadel', '3'),
(63, 461, 1, 'Mixto', 'campeonato oupadel', '3'),
(64, 543, 1, 'Masculino', 'especial padel', '3'),
(64, 554, 1, 'Masculino', 'especial padel', '3'),
(64, 555, 1, 'Masculino', 'especial padel', '3'),
(64, 556, 1, 'Masculino', 'especial padel', '3'),
(64, 557, 1, 'Masculino', 'especial padel', '3'),
(64, 558, 1, 'Masculino', 'especial padel', '3'),
(64, 559, 1, 'Masculino', 'especial padel', '3'),
(64, 560, 1, 'Masculino', 'especial padel', '3'),
(64, 561, 1, 'Masculino', 'especial padel', '3'),
(64, 562, 1, 'Masculino', 'especial padel', '3'),
(64, 563, 1, 'Masculino', 'especial padel', '3'),
(66, 422, 1, 'Masculino', 'campeonato oupadel', '1'),
(66, 428, 1, 'Masculino', 'campeonato oupadel', '1'),
(66, 434, 1, 'Masculino', 'campeonato oupadel', '1'),
(66, 435, 1, 'Masculino', 'campeonato oupadel', '1'),
(66, 436, 1, 'Masculino', 'campeonato oupadel', '1'),
(66, 437, 1, 'Masculino', 'campeonato oupadel', '1'),
(66, 438, 1, 'Masculino', 'campeonato oupadel', '1'),
(67, 450, 1, 'Mixto', 'campeonato oupadel', '3'),
(67, 456, 1, 'Mixto', 'campeonato oupadel', '3'),
(67, 462, 1, 'Mixto', 'campeonato oupadel', '3'),
(67, 463, 1, 'Mixto', 'campeonato oupadel', '3'),
(67, 464, 1, 'Mixto', 'campeonato oupadel', '3'),
(67, 465, 1, 'Mixto', 'campeonato oupadel', '3'),
(67, 466, 1, 'Mixto', 'campeonato oupadel', '3'),
(68, 544, 1, 'Masculino', 'especial padel', '3'),
(68, 554, 1, 'Masculino', 'especial padel', '3'),
(68, 564, 1, 'Masculino', 'especial padel', '3'),
(68, 565, 1, 'Masculino', 'especial padel', '3'),
(68, 566, 1, 'Masculino', 'especial padel', '3'),
(68, 567, 1, 'Masculino', 'especial padel', '3'),
(68, 568, 1, 'Masculino', 'especial padel', '3'),
(68, 569, 1, 'Masculino', 'especial padel', '3'),
(68, 570, 1, 'Masculino', 'especial padel', '3'),
(68, 571, 1, 'Masculino', 'especial padel', '3'),
(68, 572, 1, 'Masculino', 'especial padel', '3'),
(70, 423, 1, 'Masculino', 'campeonato oupadel', '1'),
(70, 429, 1, 'Masculino', 'campeonato oupadel', '1'),
(70, 434, 1, 'Masculino', 'campeonato oupadel', '1'),
(70, 439, 1, 'Masculino', 'campeonato oupadel', '1'),
(70, 440, 1, 'Masculino', 'campeonato oupadel', '1'),
(70, 441, 1, 'Masculino', 'campeonato oupadel', '1'),
(70, 442, 1, 'Masculino', 'campeonato oupadel', '1'),
(71, 451, 1, 'Mixto', 'campeonato oupadel', '3'),
(71, 457, 1, 'Mixto', 'campeonato oupadel', '3'),
(71, 462, 1, 'Mixto', 'campeonato oupadel', '3'),
(71, 467, 1, 'Mixto', 'campeonato oupadel', '3'),
(71, 468, 1, 'Mixto', 'campeonato oupadel', '3'),
(71, 469, 1, 'Mixto', 'campeonato oupadel', '3'),
(71, 470, 1, 'Mixto', 'campeonato oupadel', '3'),
(72, 545, 1, 'Masculino', 'especial padel', '3'),
(72, 555, 1, 'Masculino', 'especial padel', '3'),
(72, 564, 1, 'Masculino', 'especial padel', '3'),
(72, 573, 1, 'Masculino', 'especial padel', '3'),
(72, 574, 1, 'Masculino', 'especial padel', '3'),
(72, 575, 1, 'Masculino', 'especial padel', '3'),
(72, 576, 1, 'Masculino', 'especial padel', '3'),
(72, 577, 1, 'Masculino', 'especial padel', '3'),
(72, 578, 1, 'Masculino', 'especial padel', '3'),
(72, 579, 1, 'Masculino', 'especial padel', '3'),
(72, 580, 1, 'Masculino', 'especial padel', '3'),
(74, 424, 1, 'Masculino', 'campeonato oupadel', '1'),
(74, 430, 1, 'Masculino', 'campeonato oupadel', '1'),
(74, 435, 1, 'Masculino', 'campeonato oupadel', '1'),
(74, 439, 1, 'Masculino', 'campeonato oupadel', '1'),
(74, 443, 1, 'Masculino', 'campeonato oupadel', '1'),
(74, 444, 1, 'Masculino', 'campeonato oupadel', '1'),
(74, 445, 1, 'Masculino', 'campeonato oupadel', '1'),
(75, 452, 1, 'Mixto', 'campeonato oupadel', '3'),
(75, 458, 1, 'Mixto', 'campeonato oupadel', '3'),
(75, 463, 1, 'Mixto', 'campeonato oupadel', '3'),
(75, 467, 1, 'Mixto', 'campeonato oupadel', '3'),
(75, 471, 1, 'Mixto', 'campeonato oupadel', '3'),
(75, 472, 1, 'Mixto', 'campeonato oupadel', '3'),
(75, 473, 1, 'Mixto', 'campeonato oupadel', '3'),
(76, 546, 1, 'Masculino', 'especial padel', '3'),
(76, 556, 1, 'Masculino', 'especial padel', '3'),
(76, 565, 1, 'Masculino', 'especial padel', '3'),
(76, 573, 1, 'Masculino', 'especial padel', '3'),
(76, 581, 1, 'Masculino', 'especial padel', '3'),
(76, 582, 1, 'Masculino', 'especial padel', '3'),
(76, 583, 1, 'Masculino', 'especial padel', '3'),
(76, 584, 1, 'Masculino', 'especial padel', '3'),
(76, 585, 1, 'Masculino', 'especial padel', '3'),
(76, 586, 1, 'Masculino', 'especial padel', '3'),
(76, 587, 1, 'Masculino', 'especial padel', '3'),
(78, 425, 1, 'Masculino', 'campeonato oupadel', '1'),
(78, 431, 1, 'Masculino', 'campeonato oupadel', '1'),
(78, 436, 1, 'Masculino', 'campeonato oupadel', '1'),
(78, 440, 1, 'Masculino', 'campeonato oupadel', '1'),
(78, 443, 1, 'Masculino', 'campeonato oupadel', '1'),
(78, 446, 1, 'Masculino', 'campeonato oupadel', '1'),
(78, 447, 1, 'Masculino', 'campeonato oupadel', '1'),
(79, 453, 1, 'Mixto', 'campeonato oupadel', '3'),
(79, 459, 1, 'Mixto', 'campeonato oupadel', '3'),
(79, 464, 1, 'Mixto', 'campeonato oupadel', '3'),
(79, 468, 1, 'Mixto', 'campeonato oupadel', '3'),
(79, 471, 1, 'Mixto', 'campeonato oupadel', '3'),
(79, 474, 1, 'Mixto', 'campeonato oupadel', '3'),
(79, 475, 1, 'Mixto', 'campeonato oupadel', '3'),
(81, 547, 1, 'Masculino', 'especial padel', '3'),
(81, 557, 1, 'Masculino', 'especial padel', '3'),
(81, 566, 1, 'Masculino', 'especial padel', '3'),
(81, 574, 1, 'Masculino', 'especial padel', '3'),
(81, 581, 1, 'Masculino', 'especial padel', '3'),
(81, 588, 1, 'Masculino', 'especial padel', '3'),
(81, 589, 1, 'Masculino', 'especial padel', '3'),
(81, 590, 1, 'Masculino', 'especial padel', '3'),
(81, 591, 1, 'Masculino', 'especial padel', '3'),
(81, 592, 1, 'Masculino', 'especial padel', '3'),
(81, 593, 1, 'Masculino', 'especial padel', '3'),
(82, 426, 1, 'Masculino', 'campeonato oupadel', '1'),
(82, 432, 1, 'Masculino', 'campeonato oupadel', '1'),
(82, 437, 1, 'Masculino', 'campeonato oupadel', '1'),
(82, 441, 1, 'Masculino', 'campeonato oupadel', '1'),
(82, 444, 1, 'Masculino', 'campeonato oupadel', '1'),
(82, 446, 1, 'Masculino', 'campeonato oupadel', '1'),
(82, 448, 1, 'Masculino', 'campeonato oupadel', '1'),
(83, 454, 1, 'Mixto', 'campeonato oupadel', '3'),
(83, 460, 1, 'Mixto', 'campeonato oupadel', '3'),
(83, 465, 1, 'Mixto', 'campeonato oupadel', '3'),
(83, 469, 1, 'Mixto', 'campeonato oupadel', '3'),
(83, 472, 1, 'Mixto', 'campeonato oupadel', '3'),
(83, 474, 1, 'Mixto', 'campeonato oupadel', '3'),
(83, 476, 1, 'Mixto', 'campeonato oupadel', '3'),
(84, 548, 1, 'Masculino', 'especial padel', '3'),
(84, 558, 1, 'Masculino', 'especial padel', '3'),
(84, 567, 1, 'Masculino', 'especial padel', '3'),
(84, 575, 1, 'Masculino', 'especial padel', '3'),
(84, 582, 1, 'Masculino', 'especial padel', '3'),
(84, 588, 1, 'Masculino', 'especial padel', '3'),
(84, 594, 1, 'Masculino', 'especial padel', '3'),
(84, 595, 1, 'Masculino', 'especial padel', '3'),
(84, 596, 1, 'Masculino', 'especial padel', '3'),
(84, 597, 1, 'Masculino', 'especial padel', '3'),
(84, 598, 1, 'Masculino', 'especial padel', '3'),
(86, 427, 1, 'Masculino', 'campeonato oupadel', '1'),
(86, 433, 1, 'Masculino', 'campeonato oupadel', '1'),
(86, 438, 1, 'Masculino', 'campeonato oupadel', '1'),
(86, 442, 1, 'Masculino', 'campeonato oupadel', '1'),
(86, 445, 1, 'Masculino', 'campeonato oupadel', '1'),
(86, 447, 1, 'Masculino', 'campeonato oupadel', '1'),
(86, 448, 1, 'Masculino', 'campeonato oupadel', '1'),
(87, 455, 1, 'Mixto', 'campeonato oupadel', '3'),
(87, 461, 1, 'Mixto', 'campeonato oupadel', '3'),
(87, 466, 1, 'Mixto', 'campeonato oupadel', '3'),
(87, 470, 1, 'Mixto', 'campeonato oupadel', '3'),
(87, 473, 1, 'Mixto', 'campeonato oupadel', '3'),
(87, 475, 1, 'Mixto', 'campeonato oupadel', '3'),
(87, 476, 1, 'Mixto', 'campeonato oupadel', '3'),
(88, 549, 1, 'Masculino', 'especial padel', '3'),
(88, 559, 1, 'Masculino', 'especial padel', '3'),
(88, 568, 1, 'Masculino', 'especial padel', '3'),
(88, 576, 1, 'Masculino', 'especial padel', '3'),
(88, 583, 1, 'Masculino', 'especial padel', '3'),
(88, 589, 1, 'Masculino', 'especial padel', '3'),
(88, 594, 1, 'Masculino', 'especial padel', '3'),
(88, 599, 1, 'Masculino', 'especial padel', '3'),
(88, 600, 1, 'Masculino', 'especial padel', '3'),
(88, 601, 1, 'Masculino', 'especial padel', '3'),
(88, 602, 1, 'Masculino', 'especial padel', '3'),
(90, 550, 1, 'Masculino', 'especial padel', '3'),
(90, 560, 1, 'Masculino', 'especial padel', '3'),
(90, 569, 1, 'Masculino', 'especial padel', '3'),
(90, 577, 1, 'Masculino', 'especial padel', '3'),
(90, 584, 1, 'Masculino', 'especial padel', '3'),
(90, 590, 1, 'Masculino', 'especial padel', '3'),
(90, 595, 1, 'Masculino', 'especial padel', '3'),
(90, 599, 1, 'Masculino', 'especial padel', '3'),
(90, 603, 1, 'Masculino', 'especial padel', '3'),
(90, 604, 1, 'Masculino', 'especial padel', '3'),
(90, 605, 1, 'Masculino', 'especial padel', '3'),
(92, 551, 1, 'Masculino', 'especial padel', '3'),
(92, 561, 1, 'Masculino', 'especial padel', '3'),
(92, 570, 1, 'Masculino', 'especial padel', '3'),
(92, 578, 1, 'Masculino', 'especial padel', '3'),
(92, 585, 1, 'Masculino', 'especial padel', '3'),
(92, 591, 1, 'Masculino', 'especial padel', '3'),
(92, 596, 1, 'Masculino', 'especial padel', '3'),
(92, 600, 1, 'Masculino', 'especial padel', '3'),
(92, 603, 1, 'Masculino', 'especial padel', '3'),
(92, 606, 1, 'Masculino', 'especial padel', '3'),
(92, 607, 1, 'Masculino', 'especial padel', '3'),
(94, 552, 1, 'Masculino', 'especial padel', '3'),
(94, 562, 1, 'Masculino', 'especial padel', '3'),
(94, 571, 1, 'Masculino', 'especial padel', '3'),
(94, 579, 1, 'Masculino', 'especial padel', '3'),
(94, 586, 1, 'Masculino', 'especial padel', '3'),
(94, 592, 1, 'Masculino', 'especial padel', '3'),
(94, 597, 1, 'Masculino', 'especial padel', '3'),
(94, 601, 1, 'Masculino', 'especial padel', '3'),
(94, 604, 1, 'Masculino', 'especial padel', '3'),
(94, 606, 1, 'Masculino', 'especial padel', '3'),
(94, 608, 1, 'Masculino', 'especial padel', '3'),
(96, 553, 1, 'Masculino', 'especial padel', '3'),
(96, 563, 1, 'Masculino', 'especial padel', '3'),
(96, 572, 1, 'Masculino', 'especial padel', '3'),
(96, 580, 1, 'Masculino', 'especial padel', '3'),
(96, 587, 1, 'Masculino', 'especial padel', '3'),
(96, 593, 1, 'Masculino', 'especial padel', '3'),
(96, 598, 1, 'Masculino', 'especial padel', '3'),
(96, 602, 1, 'Masculino', 'especial padel', '3'),
(96, 605, 1, 'Masculino', 'especial padel', '3'),
(96, 607, 1, 'Masculino', 'especial padel', '3'),
(96, 608, 1, 'Masculino', 'especial padel', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

CREATE TABLE `partido` (
  `ID_PARTIDO` int(10) NOT NULL,
  `FECHA_PARTIDO` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HORA_PARTIDO` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_PISTA` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `partido`
--

INSERT INTO `partido` (`ID_PARTIDO`, `FECHA_PARTIDO`, `HORA_PARTIDO`, `ID_PISTA`) VALUES
(1, '24/01/2019', '14:30', 1),
(2, '29/01/2019', '17:30', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pista`
--

CREATE TABLE `pista` (
  `ID_PISTA` int(10) NOT NULL,
  `TIPO_PISTA` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DESCRIPCION_PISTA` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pista`
--

INSERT INTO `pista` (`ID_PISTA`, `TIPO_PISTA`, `DESCRIPCION_PISTA`) VALUES
(1, 'CEMENTO', 'PISTA Nº1, SITUADA AL FINAL DE LA CALLE 1 A LA IZQUIERDA'),
(2, 'HORMIGÓN POROSO', 'PISTA Nº2, SITUADA AL FINAL DE LA CALLE 1 A LA DERECHA'),
(3, 'RESINA SINTÉTICA', 'PISTA Nº3, SITUADA A LA MITAD DE LA CALLE 1 A LA IZQUIERDA'),
(4, 'CESPED ARTIFICIAL', 'PISTA Nº4, SITUADA A LA MITAD DE LA CALLE 1 A LA DERECHA'),
(5, 'CEMENTO', 'PISTA Nº5, SITUADA AL PRINCIPIO DE LA CALLE 1 A LA IZQUIERDA'),
(6, 'CEMENTO', 'PISTA Nº6, SITUADA AL PRINCIPIO DE LA CALLE 1 A LA DERECHA'),
(7, 'CEMENTO', 'PISTA Nº7, SITUADA AL FINAL DE LA CALLE 2 A LA IZQUIERDA'),
(8, 'HORMIGÓN POROSO', 'PISTA Nº8, SITUADA AL FINAL DE LA CALLE 2 A LA DERECHA'),
(9, 'RESINA SINTÉTICA', 'PISTA Nº9, SITUADA A LA MITAD DE LA CALLE 2 A LA IZQUIERDA'),
(10, 'CESPED ARTIFICIAL', 'PISTA Nº10, SITUADA A LA MITAD DE LA CALLE 2 A LA DERECHA'),
(11, 'CEMENTO', 'PISTA Nº11, SITUADA AL PRINCIPIO DE LA CALLE 2 A LA IZQUIERDA');

-- --------------------------------------------------------

--
-- Estructura para la vista `mis_clases`
--
DROP TABLE IF EXISTS `mis_clases`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mis_clases`  AS  select `deportista_clase`.`LOGIN` AS `LOGIN`,`deportista_clase`.`ID_CLASE` AS `ID_CLASE`,`clase`.`FECHA_CLASE` AS `FECHA_CLASE`,`clase`.`HORA_CLASE` AS `HORA_CLASE`,`clase`.`NOMBRE_ESCUELA` AS `NOMBRE_ESCUELA`,`clase`.`ID_PISTA` AS `ID_PISTA` from (`deportista_clase` join `clase` on((`deportista_clase`.`ID_CLASE` = `clase`.`ID_CLASE`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `mis_partidos`
--
DROP TABLE IF EXISTS `mis_partidos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mis_partidos`  AS  select `deportista_partido`.`LOGIN` AS `Login`,`deportista_partido`.`ID_PARTIDO` AS `ID_Partido`,`partido`.`FECHA_PARTIDO` AS `Fecha_Partido`,`partido`.`HORA_PARTIDO` AS `Hora_Partido`,`partido`.`ID_PISTA` AS `ID_Pista` from (`deportista_partido` join `partido` on((`deportista_partido`.`ID_PARTIDO` = `partido`.`ID_PARTIDO`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `campeonato`
--
ALTER TABLE `campeonato`
  ADD PRIMARY KEY (`NOMBRE_CAMPEONATO`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`),
  ADD KEY `FK_CAMPEONATO` (`NOMBRE_CAMPEONATO`);

--
-- Indices de la tabla `clase`
--
ALTER TABLE `clase`
  ADD PRIMARY KEY (`ID_CLASE`),
  ADD KEY `FK_IDPISTA` (`ID_PISTA`),
  ADD KEY `FK_ESCUELA` (`NOMBRE_ESCUELA`);

--
-- Indices de la tabla `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`ID_CONTENIDO`);

--
-- Indices de la tabla `cruces`
--
ALTER TABLE `cruces`
  ADD PRIMARY KEY (`ID_GRUPO`,`NOMBRE_CAMPEONATO`,`NOMBRE_CATEGORIA`,`NIVEL`,`ID_PARTIDO`);

--
-- Indices de la tabla `deportista`
--
ALTER TABLE `deportista`
  ADD PRIMARY KEY (`LOGIN`),
  ADD UNIQUE KEY `TELEFONO` (`TELEFONO`),
  ADD UNIQUE KEY `LOGIN` (`LOGIN`);

--
-- Indices de la tabla `deportista_clase`
--
ALTER TABLE `deportista_clase`
  ADD PRIMARY KEY (`LOGIN`,`ID_CLASE`),
  ADD KEY `FK_CLASE` (`ID_CLASE`),
  ADD KEY `FK_DEPORTISTAA` (`LOGIN`);

--
-- Indices de la tabla `deportista_pareja`
--
ALTER TABLE `deportista_pareja`
  ADD PRIMARY KEY (`LOGIN`,`ID_PAREJA`),
  ADD KEY `FK_PAREJA` (`ID_PAREJA`),
  ADD KEY `FK_DEPORTISTA2` (`LOGIN`);

--
-- Indices de la tabla `deportista_partido`
--
ALTER TABLE `deportista_partido`
  ADD PRIMARY KEY (`LOGIN`,`ID_PARTIDO`),
  ADD KEY `FK_PARTIDO` (`ID_PARTIDO`),
  ADD KEY `FK_DEPORTISTA_2` (`LOGIN`);

--
-- Indices de la tabla `enfrentamiento`
--
ALTER TABLE `enfrentamiento`
  ADD PRIMARY KEY (`ID_ENFRENTAMIENTO`,`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`),
  ADD KEY `FK_GRUPO2` (`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`),
  ADD KEY `FK_PISTA2` (`ID_PISTA`);

--
-- Indices de la tabla `escuela_deportiva`
--
ALTER TABLE `escuela_deportiva`
  ADD PRIMARY KEY (`NOMBRE_ESCUELA`);

--
-- Indices de la tabla `fecha_propuesta`
--
ALTER TABLE `fecha_propuesta`
  ADD PRIMARY KEY (`ID_FECHA_PROPUESTA`),
  ADD KEY `FK_PAREJA2` (`ID_PAREJA`),
  ADD KEY `FK_ENFRENTAMIENTO_OUPADEL` (`ID_ENFRENTAMIENTO`,`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`),
  ADD KEY `FK_CATEGORIA` (`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`FECHA`,`HORA`);

--
-- Indices de la tabla `horario_pista`
--
ALTER TABLE `horario_pista`
  ADD PRIMARY KEY (`ID_PISTA`,`FECHA`,`HORA`) USING BTREE,
  ADD KEY `FK_DEPORTISTA` (`LOGIN`),
  ADD KEY `FK_HORARIO_OUPADEL` (`FECHA`,`HORA`),
  ADD KEY `FK_PISTA_OUPADEL` (`ID_PISTA`);

--
-- Indices de la tabla `pareja`
--
ALTER TABLE `pareja`
  ADD PRIMARY KEY (`ID_PAREJA`),
  ADD KEY `FK_CATEGORIA2` (`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`),
  ADD KEY `FK_GRUPO` (`ID_GRUPO`,`NOMBRE_CATEGORIA_GRUPO`,`NOMBRE_CAMPEONATO_GRUPO`,`NIVEL_CATEGORIA_GRUPO`);

--
-- Indices de la tabla `pareja_enfrentamiento`
--
ALTER TABLE `pareja_enfrentamiento`
  ADD PRIMARY KEY (`ID_PAREJA`,`ID_ENFRENTAMIENTO`,`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`),
  ADD KEY `FK_ENFRENTAMIENTO2` (`ID_ENFRENTAMIENTO`,`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`),
  ADD KEY `FK_PAREJA3` (`ID_PAREJA`);

--
-- Indices de la tabla `partido`
--
ALTER TABLE `partido`
  ADD PRIMARY KEY (`ID_PARTIDO`),
  ADD UNIQUE KEY `ID_PARTIDO` (`ID_PARTIDO`),
  ADD KEY `FK_PISTA` (`ID_PISTA`);

--
-- Indices de la tabla `pista`
--
ALTER TABLE `pista`
  ADD PRIMARY KEY (`ID_PISTA`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clase`
--
ALTER TABLE `clase`
  MODIFY `ID_CLASE` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `contenido`
--
ALTER TABLE `contenido`
  MODIFY `ID_CONTENIDO` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `enfrentamiento`
--
ALTER TABLE `enfrentamiento`
  MODIFY `ID_ENFRENTAMIENTO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=609;

--
-- AUTO_INCREMENT de la tabla `fecha_propuesta`
--
ALTER TABLE `fecha_propuesta`
  MODIFY `ID_FECHA_PROPUESTA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `horario_pista`
--
ALTER TABLE `horario_pista`
  MODIFY `ID_PISTA` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `pareja`
--
ALTER TABLE `pareja`
  MODIFY `ID_PAREJA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT de la tabla `partido`
--
ALTER TABLE `partido`
  MODIFY `ID_PARTIDO` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `pista`
--
ALTER TABLE `pista`
  MODIFY `ID_PISTA` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `FK_CAMPEONATO` FOREIGN KEY (`NOMBRE_CAMPEONATO`) REFERENCES `campeonato` (`NOMBRE_CAMPEONATO`) ON DELETE CASCADE;

--
-- Filtros para la tabla `clase`
--
ALTER TABLE `clase`
  ADD CONSTRAINT `FK_ESCUELA` FOREIGN KEY (`NOMBRE_ESCUELA`) REFERENCES `escuela_deportiva` (`NOMBRE_ESCUELA`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IDPISTA` FOREIGN KEY (`ID_PISTA`) REFERENCES `pista` (`ID_PISTA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `deportista_clase`
--
ALTER TABLE `deportista_clase`
  ADD CONSTRAINT `FK_CLASE` FOREIGN KEY (`ID_CLASE`) REFERENCES `clase` (`ID_CLASE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_DEPORTISTAA` FOREIGN KEY (`LOGIN`) REFERENCES `deportista` (`LOGIN`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `deportista_pareja`
--
ALTER TABLE `deportista_pareja`
  ADD CONSTRAINT `FK_DEPORTISTA2` FOREIGN KEY (`LOGIN`) REFERENCES `deportista` (`LOGIN`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PAREJA` FOREIGN KEY (`ID_PAREJA`) REFERENCES `pareja` (`ID_PAREJA`) ON DELETE CASCADE;

--
-- Filtros para la tabla `deportista_partido`
--
ALTER TABLE `deportista_partido`
  ADD CONSTRAINT `FK_DEPORTISTA_2` FOREIGN KEY (`LOGIN`) REFERENCES `deportista` (`LOGIN`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PARTIDO` FOREIGN KEY (`ID_PARTIDO`) REFERENCES `partido` (`ID_PARTIDO`) ON DELETE CASCADE;

--
-- Filtros para la tabla `enfrentamiento`
--
ALTER TABLE `enfrentamiento`
  ADD CONSTRAINT `FK_GRUPO2` FOREIGN KEY (`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`) REFERENCES `grupo` (`ID_GRUPO`, `NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PISTA2` FOREIGN KEY (`ID_PISTA`) REFERENCES `pista` (`ID_PISTA`) ON DELETE SET NULL;

--
-- Filtros para la tabla `fecha_propuesta`
--
ALTER TABLE `fecha_propuesta`
  ADD CONSTRAINT `FK_ENFRENTAMIENTO_OUPADEL` FOREIGN KEY (`ID_ENFRENTAMIENTO`,`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`) REFERENCES `enfrentamiento` (`ID_ENFRENTAMIENTO`, `ID_GRUPO`, `NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_PAREJA2` FOREIGN KEY (`ID_PAREJA`) REFERENCES `pareja` (`ID_PAREJA`) ON DELETE CASCADE;

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `FK_CATEGORIA` FOREIGN KEY (`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`) REFERENCES `categoria` (`NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) ON DELETE CASCADE;

--
-- Filtros para la tabla `horario_pista`
--
ALTER TABLE `horario_pista`
  ADD CONSTRAINT `FK_DEPORTISTA` FOREIGN KEY (`LOGIN`) REFERENCES `deportista` (`LOGIN`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_HORARIO_OUPADEL` FOREIGN KEY (`FECHA`,`HORA`) REFERENCES `horario` (`FECHA`, `HORA`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PISTA_OUPADEL` FOREIGN KEY (`ID_PISTA`) REFERENCES `pista` (`ID_PISTA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pareja`
--
ALTER TABLE `pareja`
  ADD CONSTRAINT `FK_CATEGORIA2` FOREIGN KEY (`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`) REFERENCES `categoria` (`NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_GRUPO` FOREIGN KEY (`ID_GRUPO`,`NOMBRE_CATEGORIA_GRUPO`,`NOMBRE_CAMPEONATO_GRUPO`,`NIVEL_CATEGORIA_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`, `NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) ON DELETE SET NULL;

--
-- Filtros para la tabla `pareja_enfrentamiento`
--
ALTER TABLE `pareja_enfrentamiento`
  ADD CONSTRAINT `FK_ENFRENTAMIENTO2` FOREIGN KEY (`ID_ENFRENTAMIENTO`,`ID_GRUPO`,`NOMBRE_CATEGORIA`,`NOMBRE_CAMPEONATO`,`NIVEL`) REFERENCES `enfrentamiento` (`ID_ENFRENTAMIENTO`, `ID_GRUPO`, `NOMBRE_CATEGORIA`, `NOMBRE_CAMPEONATO`, `NIVEL`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PAREJA3` FOREIGN KEY (`ID_PAREJA`) REFERENCES `pareja` (`ID_PAREJA`) ON DELETE CASCADE;

--
-- Filtros para la tabla `partido`
--
ALTER TABLE `partido`
  ADD CONSTRAINT `FK_PISTA` FOREIGN KEY (`ID_PISTA`) REFERENCES `pista` (`ID_PISTA`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
